import 'package:bloc_test/bloc_test.dart';
import 'package:didayz/core/presentation/blocs/navigation/navigation_cubit.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  late NavigationCubit navigationCubit;

  setUp(() {
    navigationCubit = NavigationCubit();
  });

  blocTest<NavigationCubit, NavigationState>(
    'Navigate to "my_didayz"',
    build: () => navigationCubit,
    act: (cubit) => cubit.getNavBarItem(NavbarItem.didayz),
    expect: () => [
      const NavigationState(NavbarItem.didayz),
    ],
  );

  blocTest<NavigationCubit, NavigationState>(
    'Navigate to "movies"',
    build: () => navigationCubit,
    act: (cubit) => cubit.getNavBarItem(NavbarItem.didayz),
    expect: () => [
      const NavigationState(NavbarItem.didayz),
    ],
  );

  blocTest<NavigationCubit, NavigationState>(
    '"Home" is the first item',
    build: () => navigationCubit,
    act: (cubit) => cubit.getNavBarItemFromIndex(0),
    expect: () => [
      const NavigationState(NavbarItem.didayz),
    ],
  );

  blocTest<NavigationCubit, NavigationState>(
    '"Movies" is the second item',
    build: () => navigationCubit,
    act: (cubit) => cubit.getNavBarItemFromIndex(1),
    expect: () => [
      const NavigationState(NavbarItem.didayz),
    ],
  );
}
