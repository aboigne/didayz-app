import 'dart:convert';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:didayz/core/presentation/service/notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'features/didayz/presentation/blocs/didayz/didayz_cubit.dart';
import 'features/didayz_day_detail/presentation/blocs/didayz_day_detail_cubit.dart';
import 'features/didayz_detail/presentation/blocs/didayz_detail_cubit.dart';
import 'firebase_options.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';

import '../../../../core/resources/vectors.dart';
import 'core/presentation/blocs/navigation/navigation_cubit.dart';
import 'core/presentation/widgets/banner_wrapper.dart';
import 'core/router/app_router.dart';
import 'core/theme/theme.dart';
import 'di/injection_container.dart' as di;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  AwesomeNotifications().initialize(
    // set the icon to null if you want to use the default app icon
      null,
      [
        NotificationChannel(
            channelGroupKey: 'basic_channel_group',
            channelKey: 'basic_channel',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel',
            defaultColor: Color(0xFF9D50DD),
            ledColor: Colors.white,
        ),
      ],
      // Channel groups are only visual and are not required
      channelGroups: [
        NotificationChannelGroup(
            channelGroupKey: 'basic_channel_group',
            channelGroupName: 'Basic group',
        ),
      ],
      debug: true,
  );

  // Initialize Dependency Injection
  await di.init();

  await GlobalConfiguration().loadFromAsset('app_settings');
  const loader = SvgAssetLoader(Vectors.didayz);
  await svg.cache.putIfAbsent(loader.cacheKey(null), () => loader.loadBytes(null));

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const DidayzApp());
}

class DidayzApp extends StatefulWidget {
  const DidayzApp({super.key});

  static final appRouter = AppRouter();

  @override
  State<DidayzApp> createState() => _DidayzAppState();
}

class _DidayzAppState extends State<DidayzApp> {

  late NavigationCubit _navigationCubit;
  late DidayzCubit _didayzCubit;
  late DidayzCreatorCubit _didayzCreatorCubit;
  late DidayzCreatorPublishedCubit _didayzCreatorPublishedCubit;
  late DidayzDetailCubit _didayzDetailCubit;
  late DidayzDayDetailCubit _didayzDayDetailCubit;

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    _navigationCubit = di.sl<NavigationCubit>();
    _didayzCubit = di.sl<DidayzCubit>();
    _didayzCreatorCubit = di.sl<DidayzCreatorCubit>();
    _didayzCreatorPublishedCubit = di.sl<DidayzCreatorPublishedCubit>();
    _didayzDetailCubit = di.sl<DidayzDetailCubit>();
    _didayzDayDetailCubit = di.sl<DidayzDayDetailCubit>();
    encryption_key();

    // Only after at least the action method is set, the notification events are delivered
    AwesomeNotifications().setListeners(
        onActionReceivedMethod:         NotificationController.onActionReceivedMethod,
        onNotificationCreatedMethod:    NotificationController.onNotificationCreatedMethod,
        onNotificationDisplayedMethod:  NotificationController.onNotificationDisplayedMethod,
        onDismissActionReceivedMethod:  NotificationController.onDismissActionReceivedMethod,
    );
  }

  Future<void> encryption_key() async {
    final containsEncryptionKey =
        await secureStorage.containsKey(key: 'encryptionKey');
    if (!containsEncryptionKey) {
      final key = Hive.generateSecureKey();
      await secureStorage.write(
          key: 'encryptionKey', value: base64UrlEncode(key),);
    }
  }

  @override
  void dispose() {
    _navigationCubit.close();
    _didayzCubit.close();
    _didayzCreatorCubit.close();
    _didayzCreatorPublishedCubit.close();
    _didayzDetailCubit.close();
    _didayzDayDetailCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalConfiguration cfg = GlobalConfiguration();
    return BannerWrapper(
      env: cfg.getValue('env'),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<NavigationCubit>.value(value: _navigationCubit),
          BlocProvider<DidayzCubit>.value(value: _didayzCubit),
          BlocProvider<DidayzCreatorCubit>.value(value: _didayzCreatorCubit),
          BlocProvider<DidayzCreatorPublishedCubit>.value(value: _didayzCreatorPublishedCubit),
          BlocProvider<DidayzDetailCubit>.value(value: _didayzDetailCubit),
          BlocProvider<DidayzDayDetailCubit>.value(value: _didayzDayDetailCubit),
        ],
        child: MaterialApp(
          title: 'Didayz',
          theme: DidayzTheme(),
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          initialRoute: DidayzApp.appRouter.initialRoute,
          onGenerateInitialRoutes: DidayzApp.appRouter.onGenerateInitialRoutes,
          onGenerateRoute: DidayzApp.appRouter.onGenerateRoute,
          navigatorKey: DidayzApp.appRouter.navigatorKey,
        ),
      ),
    );
  }
}
