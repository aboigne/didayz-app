import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:didayz/features/auth/service/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:page_transition/page_transition.dart';

import '../../../../core/presentation/pages/main_page.dart';
import '../../../../core/resources/images.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../auth/presentation/pages/login_page.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({super.key});

  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();
  bool token_valid = false, token_initialized = false;

  @override
  void initState() {
    super.initState();
    goLogin();
  }

  void goLogin() async {
    final result_token_valid = await AuthService().verify_crt_token();
    switch (result_token_valid) {
      case 200:
        setState(() {
          token_valid = true;
          token_initialized = true;
        });
        break;
      default:
        setState(() {
          token_valid = false;
          token_initialized = true;
        });
    }
  }

  @override
  Widget build(BuildContext context) {
    /*if(token_initialized)
    {*/
      return AnimatedSplashScreen(
        backgroundColor: AppColors.primaryColor,
        duration: 3000,
        splashIconSize: double.infinity,
        splash: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              Images.didayz,
              width: 200,
            ),
          ],
        ),
        nextScreen: token_valid ? MainPage() : LoginPage(),
        splashTransition: SplashTransition.fadeTransition,
        pageTransitionType: PageTransitionType.fade,
      );
    /*}
    else
    {
      return Scaffold(
        backgroundColor: AppColors.primaryColor,
        body: SizedBox(),
      );
    }*/

  }
}
