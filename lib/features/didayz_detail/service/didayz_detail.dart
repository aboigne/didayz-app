import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

import '../../../core/constants/api_constants.dart';

class DidayzDetailService {
  final String domain = GlobalConfiguration().getValue('api_base_url');

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Future<int> change_order_days(String old_date, String new_date, int cal_id, int day_id) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'oldDate': old_date, 'newDate': new_date, 'id': cal_id, 'day_id': day_id});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_order_days),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }
}
