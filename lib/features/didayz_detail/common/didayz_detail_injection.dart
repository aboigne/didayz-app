part of '../../../di/injection_container.dart';

void _featureDidayzDetail() {
  sl
  // DataSources
    ..injectDataSource<DidayzDetailRemoteDataSource>(() => DidayzDetailRemoteDataSourceImpl(dio: dio))

  // Repositories
    ..injectRepository<DidayzDetailRepository>(
          () => DidayzDetailRepositoryImpl(
                  remoteDataSource: sl(),
                  networkInfo: sl(),
                ),
    )

  // UseCases
    ..injectUseCase(() => GetDidayzDetailUseCase(sl()))

  // Blocs
    ..injectBloc(() => DidayzDetailCubit(didayzDetailById: sl(),));

  // Interfaces
}
