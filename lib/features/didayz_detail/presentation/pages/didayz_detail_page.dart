import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/didayz_states.dart';
import 'package:didayz/core/presentation/widgets/loading_widget.dart';
import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/didayz_detail/presentation/blocs/didayz_detail_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderable_grid_view/reorderable_grid_view.dart';
import 'package:intl/intl.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/card_day.dart';
import '../../../../core/presentation/widgets/card_didayz.dart';
import '../../../didayz/domain/entities/didayz_day.dart';
import '../../service/didayz_detail.dart';

class DidayzDetailPage extends StatefulWidget {
  const DidayzDetailPage(this.calId, this.state, {super.key});

  final int calId;
  final States_Didayz state;

  @override
  State<DidayzDetailPage> createState() => _DidayzDetailPageState();
}

class _DidayzDetailPageState extends State<DidayzDetailPage> {
  final _gridViewKey = GlobalKey();
  final _scrollController = ScrollController();
  bool? is_connected = null;
  GlobalKey<RefreshIndicatorState> refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    clearCache();
    getDidayz();
    super.initState();
  }

  void copy(textToCopy) async {
    await FlutterClipboard.copy(textToCopy);

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      backgroundColor: AppColors.primaryColor,
      content: Text(
        textToCopy + context.translate().copy_key_success,
        style: AppStyles.defaultTextStyle.merge(
          TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    ),
    );
  }

  void getDidayz() async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      this.setState(() {
        is_connected = true;
      });
      context.read<DidayzDetailCubit>().getDidayzDetail(widget.calId);
    }
  }

  void clearCache() {
    imageCache.clear();
    imageCache.clearLiveImages();
  }

  @override
  Widget build(BuildContext context) {
    if(is_connected != null && is_connected!)
    {
      return BlocBuilder<DidayzDetailCubit, DidayzDetailState>
        (builder: (context, state)
      {
        return state.when(
          loading: () =>
              Scaffold(
                appBar: TopBar(),
                body: Center(
                  child: LoadingWidget(),
                ),
              ),
          error: () =>
              Scaffold(
                appBar: TopBar(),
                body: Center(
                  child: Center(
                    child: Text(
                      context.translate().didayz_desc_error,
                      style: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
          loaded: (didayz, days) {
            List<DidayzDayEntity> days_to_display = [];
            days_to_display..addAll(days)
            ..sort((a, b) => DateTime.parse((a.Year.toString() + a.Month.toString().padLeft(2, '0') + a.Number.toString().padLeft(2, '0'))).compareTo(DateTime.parse(b.Year.toString() + b.Month.toString().padLeft(2, '0') + b.Number.toString().padLeft(2, '0'))));

            return Scaffold(
              appBar: TopBar(),
              body: RefreshIndicator(
                key: refreshKey,
                onRefresh: () {
                  Navigator.pop(context);
                  return context.read<DidayzDetailCubit>().getDidayzDetail(widget.calId);
                },
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        if(widget.state == States_Didayz.published) Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(24),),
                              color: Colors.white,
                              boxShadow: [
                                new BoxShadow(
                                  offset: Offset(0.0, 4.0),
                                  color: AppColors.primaryColor.withOpacity(0.1),
                                  blurRadius: 20.0,
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0,),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        context.translate().copy_key_to_share,
                                        softWrap: true,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: AppStyles.defaultDescStyle.merge(
                                          TextStyle(
                                            color: AppColors.grey,
                                          ),
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.content_copy,
                                          color: Colors.grey,
                                          size: 20,
                                        ),
                                        onPressed: () => copy(didayz[0].Key),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(25.0),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      didayz[0].Key,
                                      softWrap: true,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.black,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        CardDidayz(didayz[0], widget.state, didayz_desc: true, key: UniqueKey(),),
                        widget.state == States_Didayz.draft ? ReorderableGridView.builder(
                          key: Key(_gridViewKey.toString()),
                          shrinkWrap: true,
                          controller: _scrollController,
                          itemCount: days_to_display.length + 1,
                          itemBuilder: (context, index) => buildReorderedCard(index, index == 0 ? null : days_to_display[index-1]),
                          onReorder: (oldIndex, newIndex) async {
                            if(newIndex == 0 || oldIndex == 0)
                            {
                              return;
                            }
                            final DateTime old_date = DateTime.parse(days_to_display[oldIndex - 1].Year.toString() + days_to_display[oldIndex - 1].Month.toString().padLeft(2, '0') + days_to_display[oldIndex - 1].Number.toString().padLeft(2, '0'));
                            final DateTime new_date = DateTime.parse(days_to_display[newIndex - 1].Year.toString() + days_to_display[newIndex - 1].Month.toString().padLeft(2, '0') + days_to_display[newIndex - 1].Number.toString().padLeft(2, '0'));

                            final String formatted_old_date = DateFormat('dd/MM/yyyy').format(old_date);
                            final String formatted_new_date = DateFormat('dd/MM/yyyy').format(new_date);

                            final String old_date_to_send = formatted_old_date.split('/')[2] + '-' + formatted_old_date.split('/')[1] + '-' + formatted_old_date.split('/')[0];
                            final String new_date_to_send = formatted_new_date.split('/')[2] + '-' + formatted_new_date.split('/')[1] + '-' + formatted_new_date.split('/')[0];

                            final result_updt_order = await DidayzDetailService().change_order_days(old_date_to_send, new_date_to_send, widget.calId, days_to_display[oldIndex - 1].id);
                            switch (result_updt_order) {
                              case 200:
                                context.read<DidayzDetailCubit>().getDidayzDetail(widget.calId);
                                break;
                              default:
                                final SnackBar snackBar = SnackBar(
                                  content: Text(
                                    context.translate().error_updating_day,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                            }
                          },
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                            mainAxisSpacing: 5.0,
                            crossAxisSpacing: 5.0,
                            childAspectRatio: 0.8,
                            crossAxisCount: 2,
                          ),
                          dragWidgetBuilder: (index, child) => buildReorderedCardSelected(index, index == 0 ? null : days_to_display[index-1]),
                        ) :
                        GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: days.length,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            mainAxisSpacing: 5.0,
                            crossAxisSpacing: 5.0,
                            childAspectRatio: 0.8,
                            crossAxisCount: 2,
                          ),
                          itemBuilder: (context, index) {
                            return CardDay(widget.calId, didayz_day: days_to_display[index], state: widget.state, key: UniqueKey(),);
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
      );
    }
    else if(is_connected != null && !is_connected!)
    {
      return Scaffold(
        appBar: TopBar(),
        body: Align(
          alignment: Alignment.center,
          child: Text(
            context.translate().no_internet_connection,
            style: AppStyles.defaultTextStyle.merge(
              TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ),
      );
    }
    else
    {
      return Scaffold(
        appBar: TopBar(),
        body: Center(
          child: LoadingWidget(),
        ),
      );
    }
  }

  Widget buildReorderedCard(int index, DidayzDayEntity? day) {
    if(index == 0) {
      return CardDay(widget.calId, add_day: true, key: UniqueKey());
    }
    return CardDay(widget.calId, didayz_day: day, state: widget.state, key: UniqueKey(), refreshKey: refreshKey,);
  }

  Widget buildReorderedCardSelected(int index, DidayzDayEntity? day) {
    if(index == 0) {
      return CardDay(widget.calId, add_day: true, key: UniqueKey());
    }
    return Opacity(
      opacity: 0.8,
      child: CardDay(widget.calId, didayz_day: day, state: widget.state, key: UniqueKey(), refreshKey: refreshKey,),
    );
  }
}

// A Widget that extracts the necessary arguments from
// the ModalRoute.
class ExtractArgumentsScreen extends StatelessWidget {
  const ExtractArgumentsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    // Extract the arguments from the current ModalRoute
    final int id = int.parse(ModalRoute.of(context)!.settings.arguments.toString());

    return DidayzDetailPage(id, States_Didayz.received);
  }
}
