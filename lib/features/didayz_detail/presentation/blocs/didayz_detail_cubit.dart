import 'package:bloc/bloc.dart';
import 'package:didayz/features/didayz/domain/entities/didayz.dart';
import 'package:didayz/features/didayz/domain/entities/didayz_day.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/usecases/get_didayz_detail_usecase.dart';

part 'didayz_detail_state.dart';
part 'didayz_detail_cubit.freezed.dart';

class DidayzDetailCubit extends Cubit<DidayzDetailState> {
  DidayzDetailCubit({required this.didayzDetailById}) : super(DidayzDetailState.loading());

  final GetDidayzDetailUseCase didayzDetailById;

  Future<void> getDidayzDetail(int id) async {
    emit(DidayzDetailState.loading());
    final result = await didayzDetailById(GetDidayzDetailUseCaseParams(id: id));

    result.when(
      success: (didayzDetail) {
        emit(DidayzDetailState.loaded(didayz: didayzDetail.didayz, days: didayzDetail.days));
      },
      failure: (failure) {
        emit(DidayzDetailState.error());
      },
    );
  }
}
