part of 'didayz_detail_cubit.dart';

@freezed
class DidayzDetailState with _$DidayzDetailState {
  const DidayzDetailState._();

  const factory DidayzDetailState.loading() = _Loading;

  const factory DidayzDetailState.error() = _Error;

  const factory DidayzDetailState.loaded({
    required List<DidayzEntity> didayz,
    required List<DidayzDayEntity> days,
  }) = _Loaded;
}
