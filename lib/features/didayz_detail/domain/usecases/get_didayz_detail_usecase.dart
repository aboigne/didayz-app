import '../../../../../core/domain/entities/result.dart';
import '../../../../../core/domain/usecases/usecase.dart';
import '../entities/didayz_detail.dart';
import '../repositories/didayz_detail_repository.dart';

class GetDidayzDetailUseCase extends UseCase<DidayzDetailEntity, GetDidayzDetailUseCaseParams> {
  GetDidayzDetailUseCase(this.repository);

  final DidayzDetailRepository repository;

  @override
  Future<Result<DidayzDetailEntity>> call(GetDidayzDetailUseCaseParams params) async {
    return repository.getDidayzDetail(params.id);
  }
}

class GetDidayzDetailUseCaseParams {
  const GetDidayzDetailUseCaseParams({
    required this.id,
  });

  final int id;
}
