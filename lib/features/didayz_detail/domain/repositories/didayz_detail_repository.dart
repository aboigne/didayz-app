import '../../../../core/domain/entities/result.dart';
import '../entities/didayz_detail.dart';

abstract class DidayzDetailRepository {
  Future<Result<DidayzDetailEntity>> getDidayzDetail(int id);
}
