import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../didayz/domain/entities/didayz.dart';
import '../../../didayz/domain/entities/didayz_day.dart';

part 'didayz_detail.freezed.dart';

@freezed
class DidayzDetailEntity with _$DidayzDetailEntity {
  const factory DidayzDetailEntity({
    required List<DidayzEntity> didayz,
    required List<DidayzDayEntity> days,
  }) = _DidayzDetailEntity;
}

