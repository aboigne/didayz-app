// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'didayz_detail.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DidayzDetailEntity {
  List<DidayzEntity> get didayz => throw _privateConstructorUsedError;
  List<DidayzDayEntity> get days => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DidayzDetailEntityCopyWith<DidayzDetailEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DidayzDetailEntityCopyWith<$Res> {
  factory $DidayzDetailEntityCopyWith(
          DidayzDetailEntity value, $Res Function(DidayzDetailEntity) then) =
      _$DidayzDetailEntityCopyWithImpl<$Res, DidayzDetailEntity>;
  @useResult
  $Res call({List<DidayzEntity> didayz, List<DidayzDayEntity> days});
}

/// @nodoc
class _$DidayzDetailEntityCopyWithImpl<$Res, $Val extends DidayzDetailEntity>
    implements $DidayzDetailEntityCopyWith<$Res> {
  _$DidayzDetailEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? didayz = null,
    Object? days = null,
  }) {
    return _then(_value.copyWith(
      didayz: null == didayz
          ? _value.didayz
          : didayz // ignore: cast_nullable_to_non_nullable
              as List<DidayzEntity>,
      days: null == days
          ? _value.days
          : days // ignore: cast_nullable_to_non_nullable
              as List<DidayzDayEntity>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DidayzDetailEntityCopyWith<$Res>
    implements $DidayzDetailEntityCopyWith<$Res> {
  factory _$$_DidayzDetailEntityCopyWith(_$_DidayzDetailEntity value,
          $Res Function(_$_DidayzDetailEntity) then) =
      __$$_DidayzDetailEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<DidayzEntity> didayz, List<DidayzDayEntity> days});
}

/// @nodoc
class __$$_DidayzDetailEntityCopyWithImpl<$Res>
    extends _$DidayzDetailEntityCopyWithImpl<$Res, _$_DidayzDetailEntity>
    implements _$$_DidayzDetailEntityCopyWith<$Res> {
  __$$_DidayzDetailEntityCopyWithImpl(
      _$_DidayzDetailEntity _value, $Res Function(_$_DidayzDetailEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? didayz = null,
    Object? days = null,
  }) {
    return _then(_$_DidayzDetailEntity(
      didayz: null == didayz
          ? _value._didayz
          : didayz // ignore: cast_nullable_to_non_nullable
              as List<DidayzEntity>,
      days: null == days
          ? _value._days
          : days // ignore: cast_nullable_to_non_nullable
              as List<DidayzDayEntity>,
    ));
  }
}

/// @nodoc

class _$_DidayzDetailEntity implements _DidayzDetailEntity {
  const _$_DidayzDetailEntity(
      {required final List<DidayzEntity> didayz,
      required final List<DidayzDayEntity> days})
      : _didayz = didayz,
        _days = days;

  final List<DidayzEntity> _didayz;
  @override
  List<DidayzEntity> get didayz {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_didayz);
  }

  final List<DidayzDayEntity> _days;
  @override
  List<DidayzDayEntity> get days {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_days);
  }

  @override
  String toString() {
    return 'DidayzDetailEntity(didayz: $didayz, days: $days)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DidayzDetailEntity &&
            const DeepCollectionEquality().equals(other._didayz, _didayz) &&
            const DeepCollectionEquality().equals(other._days, _days));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_didayz),
      const DeepCollectionEquality().hash(_days));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DidayzDetailEntityCopyWith<_$_DidayzDetailEntity> get copyWith =>
      __$$_DidayzDetailEntityCopyWithImpl<_$_DidayzDetailEntity>(
          this, _$identity);
}

abstract class _DidayzDetailEntity implements DidayzDetailEntity {
  const factory _DidayzDetailEntity(
      {required final List<DidayzEntity> didayz,
      required final List<DidayzDayEntity> days}) = _$_DidayzDetailEntity;

  @override
  List<DidayzEntity> get didayz;
  @override
  List<DidayzDayEntity> get days;
  @override
  @JsonKey(ignore: true)
  _$$_DidayzDetailEntityCopyWith<_$_DidayzDetailEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
