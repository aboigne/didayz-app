import 'dart:async';
import '../../../../core/domain/entities/result.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/didayz_detail.dart';
import '../../domain/repositories/didayz_detail_repository.dart';
import '../datasources/didayz_detail_remote_data_source.dart';
import '../models/didayz_detail_model.dart';

class DidayzDetailRepositoryImpl implements DidayzDetailRepository {
  DidayzDetailRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  final DidayzDetailRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  @override
  Future<Result<DidayzDetailEntity>> getDidayzDetail(int id) async {
    if (!(await networkInfo.isConnected)) {
      return const Result.failure(Failure.offline());
    }

    try {
      final result = await remoteDataSource.getDidayzDetail(id);
      final didayzDetail = DidayzDetailModel.fromJson(result);
      return Result.success(didayzDetail.toDomain());
    } on ServerException {
      return const Result.failure(Failure.server());
    }
  }
}
