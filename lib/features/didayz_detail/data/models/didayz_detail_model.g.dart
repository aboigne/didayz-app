// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'didayz_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DidayzDetailModel _$$_DidayzDetailModelFromJson(Map<String, dynamic> json) =>
    _$_DidayzDetailModel(
      didayz: (json['calendar'] as List<dynamic>)
          .map((e) => DidayzModel.fromJson(e))
          .toList(),
      days: (json['days'] as List<dynamic>)
          .map((e) => DidayzDayModel.fromJson(e))
          .toList(),
    );

Map<String, dynamic> _$$_DidayzDetailModelToJson(
        _$_DidayzDetailModel instance) =>
    <String, dynamic>{
      'calendar': instance.didayz,
      'days': instance.days,
    };
