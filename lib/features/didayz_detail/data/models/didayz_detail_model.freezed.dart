// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'didayz_detail_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DidayzDetailModel _$DidayzDetailModelFromJson(Map<String, dynamic> json) {
  return _DidayzDetailModel.fromJson(json);
}

/// @nodoc
mixin _$DidayzDetailModel {
  @JsonKey(name: 'calendar')
  List<DidayzModel> get didayz => throw _privateConstructorUsedError;
  @JsonKey(name: 'days')
  List<DidayzDayModel> get days => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DidayzDetailModelCopyWith<DidayzDetailModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DidayzDetailModelCopyWith<$Res> {
  factory $DidayzDetailModelCopyWith(
          DidayzDetailModel value, $Res Function(DidayzDetailModel) then) =
      _$DidayzDetailModelCopyWithImpl<$Res, DidayzDetailModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'calendar') List<DidayzModel> didayz,
      @JsonKey(name: 'days') List<DidayzDayModel> days});
}

/// @nodoc
class _$DidayzDetailModelCopyWithImpl<$Res, $Val extends DidayzDetailModel>
    implements $DidayzDetailModelCopyWith<$Res> {
  _$DidayzDetailModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? didayz = null,
    Object? days = null,
  }) {
    return _then(_value.copyWith(
      didayz: null == didayz
          ? _value.didayz
          : didayz // ignore: cast_nullable_to_non_nullable
              as List<DidayzModel>,
      days: null == days
          ? _value.days
          : days // ignore: cast_nullable_to_non_nullable
              as List<DidayzDayModel>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DidayzDetailModelCopyWith<$Res>
    implements $DidayzDetailModelCopyWith<$Res> {
  factory _$$_DidayzDetailModelCopyWith(_$_DidayzDetailModel value,
          $Res Function(_$_DidayzDetailModel) then) =
      __$$_DidayzDetailModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'calendar') List<DidayzModel> didayz,
      @JsonKey(name: 'days') List<DidayzDayModel> days});
}

/// @nodoc
class __$$_DidayzDetailModelCopyWithImpl<$Res>
    extends _$DidayzDetailModelCopyWithImpl<$Res, _$_DidayzDetailModel>
    implements _$$_DidayzDetailModelCopyWith<$Res> {
  __$$_DidayzDetailModelCopyWithImpl(
      _$_DidayzDetailModel _value, $Res Function(_$_DidayzDetailModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? didayz = null,
    Object? days = null,
  }) {
    return _then(_$_DidayzDetailModel(
      didayz: null == didayz
          ? _value._didayz
          : didayz // ignore: cast_nullable_to_non_nullable
              as List<DidayzModel>,
      days: null == days
          ? _value._days
          : days // ignore: cast_nullable_to_non_nullable
              as List<DidayzDayModel>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DidayzDetailModel implements _DidayzDetailModel {
  const _$_DidayzDetailModel(
      {@JsonKey(name: 'calendar') required final List<DidayzModel> didayz,
      @JsonKey(name: 'days') required final List<DidayzDayModel> days})
      : _didayz = didayz,
        _days = days;

  factory _$_DidayzDetailModel.fromJson(Map<String, dynamic> json) =>
      _$$_DidayzDetailModelFromJson(json);

  final List<DidayzModel> _didayz;
  @override
  @JsonKey(name: 'calendar')
  List<DidayzModel> get didayz {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_didayz);
  }

  final List<DidayzDayModel> _days;
  @override
  @JsonKey(name: 'days')
  List<DidayzDayModel> get days {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_days);
  }

  @override
  String toString() {
    return 'DidayzDetailModel(didayz: $didayz, days: $days)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DidayzDetailModel &&
            const DeepCollectionEquality().equals(other._didayz, _didayz) &&
            const DeepCollectionEquality().equals(other._days, _days));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_didayz),
      const DeepCollectionEquality().hash(_days));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DidayzDetailModelCopyWith<_$_DidayzDetailModel> get copyWith =>
      __$$_DidayzDetailModelCopyWithImpl<_$_DidayzDetailModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DidayzDetailModelToJson(
      this,
    );
  }
}

abstract class _DidayzDetailModel implements DidayzDetailModel {
  const factory _DidayzDetailModel(
          {@JsonKey(name: 'calendar') required final List<DidayzModel> didayz,
          @JsonKey(name: 'days') required final List<DidayzDayModel> days}) =
      _$_DidayzDetailModel;

  factory _DidayzDetailModel.fromJson(Map<String, dynamic> json) =
      _$_DidayzDetailModel.fromJson;

  @override
  @JsonKey(name: 'calendar')
  List<DidayzModel> get didayz;
  @override
  @JsonKey(name: 'days')
  List<DidayzDayModel> get days;
  @override
  @JsonKey(ignore: true)
  _$$_DidayzDetailModelCopyWith<_$_DidayzDetailModel> get copyWith =>
      throw _privateConstructorUsedError;
}
