import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

import '../../../didayz/data/models/didayz_day_model.dart';
import '../../../didayz/data/models/didayz_model.dart';
import '../../../didayz/data/models/user_model.dart';
import '../../domain/entities/didayz_detail.dart';


part 'didayz_detail_model.freezed.dart';
part 'didayz_detail_model.g.dart';

@freezed
class DidayzDetailModel with _$DidayzDetailModel {
  const factory DidayzDetailModel({
    @JsonKey(name: 'calendar') required List<DidayzModel> didayz,
    @JsonKey(name: 'days') required List<DidayzDayModel> days,
  }) = _DidayzDetailModel;

  factory DidayzDetailModel.fromJson(dynamic json) => _$DidayzDetailModelFromJson(json);

  factory DidayzDetailModel.fromDomain(DidayzDetailEntity entity) {
    return DidayzDetailModel(
        didayz: entity.didayz.map(DidayzModel.fromDomain).toList(),
        days: entity.days.map(DidayzDayModel.fromDomain).toList(),
    );
  }
}

extension DidayzDetailModelX on DidayzDetailModel {
  DidayzDetailEntity toDomain() {
    return DidayzDetailEntity(
        didayz: didayz.map((e) => e.toDomain()).toList(),
        days: days.map((e) => e.toDomain()).toList(),
    );
  }
}
