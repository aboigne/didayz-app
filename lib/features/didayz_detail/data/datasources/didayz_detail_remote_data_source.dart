import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';

import '../../../../../core/constants/api_constants.dart';
import '../../../../../core/data/datasources/remote_data_source.dart';

abstract class DidayzDetailRemoteDataSource {
  Future<dynamic> getDidayzDetail(int id);
}

class DidayzDetailRemoteDataSourceImpl extends RemoteDataSource implements DidayzDetailRemoteDataSource {
  DidayzDetailRemoteDataSourceImpl({required super.dio});

  final String domain = GlobalConfiguration().getValue('api_base_url');
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  @override
  Future getDidayzDetail(int id) async {
    final key_encrypted = await secureStorage.read(
      key: 'encryptionKey',
    );
    final encryptionKey = base64Url.decode(
      key_encrypted!,
    );

    final boxUser = await Hive.openBox(
      'user',
      encryptionCipher: HiveAesCipher(encryptionKey),
    );

    return performGetRequestApi(
      apiEndpoint: ApiConstants.getDays(id.toString()),
      token: boxUser.get('token'),
    );
  }
}
