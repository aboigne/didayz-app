import 'dart:convert';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/didayz.dart';
import 'package:didayz/core/presentation/service/didayz_states.dart';
import 'package:didayz/core/presentation/widgets/loading_widget.dart';
import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:didayz/core/resources/images.dart';
import 'package:didayz/core/resources/vectors.dart';
import 'package:didayz/features/account/presentation/pages/account_page.dart';
import 'package:didayz/features/didayz/domain/entities/didayz.dart';
import 'package:didayz/features/home/service/information_messages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/card_didayz.dart';
import '../../../../core/theme/spacing.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/theme/styles.dart';
import '../../../didayz/domain/entities/user.dart';

class Didayz_User {
  String? name;
  String? first_name;
  String? email;

  Didayz_User({this.name, this.first_name, this.email});
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool is_connected = true, is_loading = true, is_loading_msg = true;
  Didayz_User crt_user = Didayz_User();
  List<dynamic>? didayz_pub = null, info_msg = null;
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();
  int currentIndexCarousel = 0;

  @override
  void initState() {
    super.initState();
    getUser();
    getPublicDidayz();
    getInformationMessages();
  }

  void getUser() async {
    final key_encrypted = await secureStorage.read(
      key: 'encryptionKey',
    );
    final encryptionKey = base64Url.decode(
      key_encrypted!,
    );

    final boxUser = await Hive.openBox(
      'user',
      encryptionCipher: HiveAesCipher(encryptionKey),
    );

    setState(() {
      crt_user = Didayz_User(
          name: boxUser.get('user_name', defaultValue: ''),
          first_name: boxUser.get('user_firstname', defaultValue: ''),
          email: boxUser.get('user_email', defaultValue: ''),
      );
    });
  }

  void getPublicDidayz() async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      DidayzService().getPublicDidayz().then(
            (value)
        {
          if(value.error_code == 200)
          {
            if(value.didayz != null && value.didayz!.length > 0 && (didayz_pub == null || didayz_pub!.length == 0))
            {
              setState(() {
                didayz_pub = value.didayz!.toList();
                is_loading = false;
              });
            }
            else if(value.didayz != null && value.didayz!.length > 0)
            {
              List<dynamic>? new_didayz = didayz_pub;
              new_didayz!.add(value.didayz!.where((element) => element['IsPublished'] == 1).toList());

              setState(() {
                didayz_pub = new_didayz;
                is_loading = false;
              });
            }
          }
        },
      );
    }
  }

  void getInformationMessages() async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      InformationMessagesService().getInformations().then((value)
        {
          if(value.error_code == 200)
          {
            if(value.msg != null && value.msg!.length > 0 && (info_msg == null || info_msg!.length == 0))
            {
              setState(() {
                info_msg = value.msg!.toList();
                is_loading_msg = false;
              });
            }
            else if(value.msg != null && value.msg!.length > 0)
            {
              List<dynamic>? new_msgs = info_msg;
              new_msgs!.add(value.msg!.toList());

              setState(() {
                info_msg = new_msgs;
                is_loading_msg = false;
              });
            }
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Locale myLocale = Localizations.localeOf(context);
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).viewPadding.top,
            color: AppColors.primaryColor,
          ),
          Stack(
            children: [
              Container(
                height: 140,
                decoration: BoxDecoration(
                  color: AppColors.primaryColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(24),
                    bottomRight: Radius.circular(24),
                  ),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    Images.didayz,
                    height: 86.0,
                  ),
                ),
              ),
              Positioned(
                left: 0.038 * MediaQuery.of(context).size.width,
                top: 36,
                child: Transform.rotate(
                  angle: 0.33,
                  child: Container(
                    height: 27,
                    width: 18,
                    decoration: BoxDecoration(
                      color: AppColors.superNova,
                      borderRadius: BorderRadius.all(Radius.circular(3.75),),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0.051 * MediaQuery.of(context).size.width,
                top: 102,
                child: Transform.rotate(
                  angle: 0.33,
                  child: Container(
                    height: 5.9,
                    width: 21,
                    decoration: BoxDecoration(
                      color: AppColors.chartreuse,
                      borderRadius: BorderRadius.all(Radius.circular(3.75),),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0.204 * MediaQuery.of(context).size.width,
                top: 60,
                child: Transform.rotate(
                  angle: 0.33,
                  child: Container(
                    height: 22.6,
                    width: 14.8,
                    decoration: BoxDecoration(
                      color: AppColors.carnationPink,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(2),
                        bottomLeft: Radius.circular(2),
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(2),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0.318 * MediaQuery.of(context).size.width,
                top: 45,
                child: Transform.rotate(
                  angle: -0.26,
                  child: Container(
                    height: 8,
                    width: 9.7,
                    decoration: BoxDecoration(
                      color: AppColors.superNova,
                      borderRadius: BorderRadius.all(Radius.circular(2),),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0.733 * MediaQuery.of(context).size.width,
                top: 105,
                child: Transform.rotate(
                  angle: -0.26,
                  child: Container(
                    height: 11.34,
                    width: 7.43,
                    decoration: BoxDecoration(
                      color: AppColors.mauve,
                      borderRadius: BorderRadius.all(Radius.circular(4),),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0.764 * MediaQuery.of(context).size.width,
                top: 45,
                child: Transform.rotate(
                  angle: 2.62,
                  child: Container(
                    height: 12.8,
                    width: 8.4,
                    decoration: BoxDecoration(
                      color: AppColors.malibu,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(2),
                        bottomLeft: Radius.circular(2),
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(2),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(createRoute(AccountPage()));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SvgPicture.asset(
                        Vectors.user_icon,
                        height: 40,
                        width: 40,
                        color: AppColors.primaryColor,
                        fit: BoxFit.scaleDown,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(),
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          AppGap.semiLarge(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: AppColors.mauve,
                borderRadius: BorderRadius.all(Radius.circular(24),),
              ),
              child: is_connected ?
                      !is_loading_msg ?
                        info_msg != null && info_msg!.length == 1 ?
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.2),
                                          borderRadius: BorderRadius.all(Radius.circular(100),),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(12.0),
                                          child: Text(
                                            myLocale.toString() == 'en' ? info_msg![0]['TitleEN'] : info_msg![0]['Title'],
                                            overflow: TextOverflow.ellipsis,
                                            style: AppStyles.defaultTextStyle.merge(
                                              TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      if(info_msg![0]['ExternalLink'] != null && info_msg![0]['ExternalLink'] != '') GestureDetector(
                                        onTap: () async {
                                          final Uri url = Uri.parse(info_msg![0]['ExternalLink']);
                                          if(await canLaunchUrl(url)) {
                                            launchUrl(url);
                                          }
                                        },
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(50.0),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                            child: Container(
                                              padding: const EdgeInsets.all(10.0),
                                              decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.4),
                                                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                              ),
                                              child: Icon(
                                                Icons.link_outlined,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                                    child: Text(
                                      myLocale.toString() == 'en' ? info_msg![0]['DescriptionEN'] : info_msg![0]['Description'],
                                      maxLines: 4,
                                      overflow: TextOverflow.ellipsis,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                          ) :
                        Column(
                          children: [
                            CarouselSlider(
                              items: buildMsgCarousel(info_msg!, myLocale),
                              options: CarouselOptions(
                                autoPlay: false,
                                initialPage: 0,
                                enableInfiniteScroll: false,
                                enlargeCenterPage: true,
                                viewportFraction: 1,
                                onPageChanged: (val, _) {
                                  setState(() {
                                    currentIndexCarousel = val;
                                  });
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: buildIndicatorsCarousel(info_msg!.length),
                              ),
                            )
                          ],
                        ) :
                      LoadingWidget() :
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        context.translate().no_internet_connection,
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
            ),
          ),
          AppGap.semiLarge(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                context.translate().discover_ideas,
                style: AppStyles.defaultTitleStyle.merge(
                  TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          AppGap.semiLarge(),
          is_connected ?
            !is_loading ?
              didayz_pub != null && didayz_pub!.length > 0 ?
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: didayz_pub!.length,
                    padding: EdgeInsets.zero,
                    itemBuilder: (context, index) {
                      DidayzEntity ddayz = new DidayzEntity(
                        id: didayz_pub![index]['id'],
                        Name: didayz_pub![index]['Name'],
                        IntroMsg: didayz_pub![index]['IntroMessage'],
                        Background: didayz_pub![index]['BackgroundPicture'],
                        Key: '',
                        DateEnd: DateTime.parse(didayz_pub![index]['DateEnd']),
                        NBDays: didayz_pub![index]['NBDays'],
                        IsPublished: true,
                        Creator: UserEntity(
                          id: 0,
                          Name: 'Didayz',
                          FirstName: 'Admin',
                          Email: 'contact@didayz.com'
                        ),
                      );
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: CardDidayz(ddayz, States_Didayz.demo),
                      );
                    },
                  ) :
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0,),
                    child: Container(
                      padding: EdgeInsets.only(bottom: 15.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(24),),
                        color: Colors.white,
                        boxShadow: [
                          new BoxShadow(
                            offset: Offset(0.0, 4.0),
                            color: AppColors.primaryColor.withOpacity(0.1),
                            blurRadius: 20.0,
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Container(
                              height: 215,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(18.0),),
                                color: AppColors.mauve,
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: Image.asset(Images.no_didayz),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 25.0,),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    context.translate().no_demo_didayz,
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ) :
              LoadingWidget() :
            Align(
              alignment: Alignment.center,
              child: Text(
                context.translate().no_internet_connection,
                style: AppStyles.defaultTextStyle.merge(
                  TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
    );
  }

  List<Widget> buildMsgCarousel(List<dynamic> msgs, Locale mylocale) {
    final List<Widget> listMsg = <Widget>[];

    msgs.forEach((msg) {
      listMsg.add(
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.2),
                        borderRadius: BorderRadius.all(Radius.circular(100),),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          mylocale.toString() == 'en' ? msg['TitleEN'] : msg['Title'],
                          overflow: TextOverflow.ellipsis,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    if(msg['ExternalLink'] != null && msg['ExternalLink'] != '') GestureDetector(
                      onTap: () async {
                        final Uri url = Uri.parse(msg['ExternalLink']);
                        if(await canLaunchUrl(url)) {
                          launchUrl(url);
                        }
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: BackdropFilter(
                          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                          child: Container(
                            padding: const EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.4),
                              borderRadius: BorderRadius.all(Radius.circular(50.0),),
                            ),
                            child: Icon(
                              Icons.link_outlined,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        mylocale.toString() == 'en' ? msg['DescriptionEN'] : msg['Description'],
                        maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
      );
    });
    return listMsg;
  }

  List<Widget> buildIndicatorsCarousel(int length_msg) {
    final List<Widget> listMsg = <Widget>[];

    for(int idx=0; idx < length_msg; idx++)
    {
      if(idx == currentIndexCarousel)
      {
        listMsg.add(
          Container(
            height: 5.0,
            width: 10.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: Colors.white,
            ),
          ),
        );
      }
      else
      {
        listMsg.add(
          Container(
            height: 5.0,
            width: 5.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
          ),
        );
      }

      if(idx < length_msg - 1)
      {
        listMsg.add(
          SizedBox(
            width: 5.0,
          ),
        );
      }
    }

    return listMsg;
  }
}
