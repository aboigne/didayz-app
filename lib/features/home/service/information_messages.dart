import 'dart:convert';

import 'package:didayz/core/constants/api_constants.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

class ResponseMsg{
  int error_code;
  List<dynamic>? msg;

  ResponseMsg(this.error_code, {this.msg});
}

class InformationMessagesService {
  final String domain = GlobalConfiguration().getValue('api_base_url');

  Future<ResponseMsg> getInformations() async {
    try {
      final result = await http.get(
        Uri.parse(domain + ApiConstants.information_msg),
        headers: {'Content-Type': 'application/json',},
      );

      ResponseMsg res_msg = new ResponseMsg(
        result.statusCode,
        msg: json.decode(result.body),
      );

      return res_msg;
    } on Exception catch (e) {
      ResponseMsg res_msg = new ResponseMsg(
        500,
      );
      return res_msg;
    }
  }
}
