import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

import '../../../core/constants/api_constants.dart';

class AuthService {
  final String domain = GlobalConfiguration().getValue('api_base_url');

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  Future<int> signin(String email, String pwd) async {
    var error_code = 0;
    try {
      final body = jsonEncode({'email': email, 'password': pwd});

      final result = await http.post(
        Uri.parse(domain + ApiConstants.signin),
        headers: {'Content-Type': 'application/json'},
        body: body,
      );

      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );

      switch (result.statusCode) {
        case 200:
          final boxUser = await Hive.openBox(
            'user',
            encryptionCipher: HiveAesCipher(encryptionKey),
          );
          boxUser.put('token', jsonDecode(result.body)['accessToken']);
          boxUser.put('user_name', jsonDecode(result.body)['name']);
          boxUser.put('user_firstname', jsonDecode(result.body)['firstname']);
          boxUser.put('user_email', jsonDecode(result.body)['email']);
          error_code = 200;
          break;
        case 401:
          error_code = 401; // Login/Mdp incorrects
          break;
        default:
          error_code = 500; // Erreur serveur
      }
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> signin_with_google() async {
    var error_code = 0;
    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn(
          scopes: [
            'openid',
            'email',
            ],
          clientId : '332156745546-800mn2jc4pkh77jhukif6n4oeccslgk4.apps.googleusercontent.com',
      ).signIn();

      final GoogleSignInAuthentication googleAuth = await googleUser!.authentication;

      final OAuthCredential googleAuthCredential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      //Authenticate against Firebase with Google credentials
      await firebaseAuth.signInWithCredential(googleAuthCredential);

      final body = jsonEncode({ 'token': googleAuth.idToken, });

      final result = await http.post(
        Uri.parse(domain + ApiConstants.verifytoken),
        headers: {'Content-Type': 'application/json'},
        body: body,
      );

      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );

      switch (result.statusCode) {
        case 200:
          final boxUser = await Hive.openBox(
            'user',
            encryptionCipher: HiveAesCipher(encryptionKey),
          );
          boxUser.put('token', jsonDecode(result.body)['accessToken']);
          boxUser.put('user_name', jsonDecode(result.body)['name']);
          boxUser.put('user_firstname', jsonDecode(result.body)['firstname']);
          boxUser.put('user_email', jsonDecode(result.body)['email']);
          error_code = 200;
          break;
        default:
          error_code = 500; // Erreur serveur
      }
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> register(String email, String pwd, String name, String firstname) async {
    var error_code = 0;
    try {
      final body = jsonEncode({ 'name': name, 'firstname': firstname, 'email': email, 'password': pwd });
      final result = await http.post(
        Uri.parse(domain + ApiConstants.signup),
        headers: {'Content-Type': 'application/json'},
        body: body,
      );

      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );

      switch (result.statusCode) {
        case 200:
          final boxUser = await Hive.openBox(
            'user',
            encryptionCipher: HiveAesCipher(encryptionKey),
          );
          boxUser.put('token', jsonDecode(result.body)['accessToken']);
          boxUser.put('user_name', jsonDecode(result.body)['name']);
          boxUser.put('user_firstname', jsonDecode(result.body)['firstname']);
          boxUser.put('user_email', jsonDecode(result.body)['email']);
          error_code = 200;
          break;
        case 401:
          error_code = 400; // Email déjà utilisé
          break;
        default:
          error_code = 500; // Erreur serveur
      }
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> forgot_pwd(String email) async {
    try {
      final body = jsonEncode({'email': email});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.rstPassword),
        headers: {'Content-Type': 'application/json'},
        body: body,
      );
      return result.statusCode;
    } on Exception catch (_) {
      return 500;
    }
  }

  Future<int> verify_crt_token() async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final result = await http.get(
          Uri.parse(domain + ApiConstants.verify_crt_tkn),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        );

        boxUser..put('user_name', jsonDecode(result.body)['name'])
        ..put('user_firstname', jsonDecode(result.body)['firstname'])
        ..put('user_email', jsonDecode(result.body)['email']);

        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }
}
