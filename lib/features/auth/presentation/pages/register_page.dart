import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/widgets/form_data.dart';
import 'package:didayz/core/presentation/widgets/input_form.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';

import '../../../../core/constants/route_list.dart';
import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/logo_header.dart';
import '../../../../core/theme/app_colors.dart';
import '../../service/auth.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  FormData formData = FormData();
  final _formKey = GlobalKey<FormState>();

  void goDidayz() async {
    Navigator.of(context).pushReplacementNamed(RouteList.didayz);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SingleChildScrollView(
          child: new Padding(
            padding: new EdgeInsets.all(25.0),
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  DefaultLogoHeader(),
                  Form(
                    key: _formKey,
                    child: Scrollbar(
                      child: SingleChildScrollView(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          children: [
                            ...[
                              DefaultInputForm(inputForm: TypeFormInput('name', context.translate().name), formData: this.formData,),
                              DefaultInputForm(inputForm: TypeFormInput('firstname', context.translate().firstname), formData: this.formData,),
                              DefaultInputForm(inputForm: TypeFormInput('email', context.translate().email), formData: this.formData,),
                              DefaultInputForm(inputForm: TypeFormInput('password', context.translate().password), formData: this.formData,),
                              DefaultInputForm(inputForm: TypeFormInput('confirmpassword', context.translate().confirm_pwd), formData: this.formData,),
                              GestureDetector(
                                onTap: () async {
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }

                                  if(_formKey.currentState!.validate() && formData.email != null && formData.password != null && formData.confirmpassword != null && formData.name != null && formData.firstname != null)
                                  {
                                    final network_inf = new NetworkInfoImpl();
                                    if(!(await network_inf.isConnected))
                                    {
                                      final snackBar = SnackBar(
                                        content: Text(
                                          context.translate().no_internet_connection,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.blazeOrange,
                                      );
                                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                    }
                                    else
                                    {
                                      final result_register = await AuthService().register(formData.email!, formData.password!, formData.name!, formData.firstname!);

                                      switch(result_register){
                                        case 200:
                                          goDidayz();
                                          break;
                                        case 400:
                                          final snackBar = SnackBar(
                                            content: Text(
                                              context.translate().email_use,
                                              style: AppStyles.defaultTextStyle.merge(
                                                TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            backgroundColor: AppColors.blazeOrange,
                                          );
                                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                          break;
                                        default:
                                          final snackBar = SnackBar(
                                            content: Text(
                                              context.translate().account_error,
                                              style: AppStyles.defaultTextStyle.merge(
                                                TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            backgroundColor: AppColors.blazeOrange,
                                          );
                                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                          break;
                                      }
                                    }
                                  }
                                },
                                child: Container(
                                  margin: const EdgeInsets.only(top: 20.0),
                                  height: 60,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                      color: AppColors.primaryColor,
                                    ),
                                    color: Colors.white,
                                  ),
                                  child: Center(
                                    child: Text(
                                      context.translate().registration.toUpperCase(),
                                      style: AppStyles.defaultTitleStyle.merge(
                                        TextStyle(
                                          color: AppColors.primaryColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  new InkWell(
                                    child: new Text(
                                      context.translate().registered,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  new InkWell(
                                    child: new Text(
                                      context.translate().log_in,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                          decoration: TextDecoration.underline,
                                        ),
                                      ),
                                    ),
                                    onTap: () => Navigator.of(context).pushReplacementNamed(RouteList.login),
                                  ),
                                ],
                              )
                            ].expand(
                                  (widget) => [
                                widget,
                                const SizedBox(
                                  height: 24,
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
            ),
          ),
      ),
    );
  }
}
