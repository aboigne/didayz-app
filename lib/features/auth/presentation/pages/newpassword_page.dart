import 'package:didayz/core/constants/route_list.dart';
import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/widgets/form_data.dart';
import 'package:didayz/core/presentation/widgets/input_form.dart';
import 'package:didayz/core/presentation/widgets/logo_header.dart';
import 'package:didayz/features/auth/service/auth.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/theme/app_colors.dart';
import 'package:flutter/material.dart';

import '../../../../core/theme/styles.dart';

class newPasswordPage extends StatefulWidget {
  const newPasswordPage({super.key});

  @override
  State<newPasswordPage> createState() => _newPasswordPageState();
}

class _newPasswordPageState extends State<newPasswordPage> {
  FormData formData = FormData();
  final _formKey = GlobalKey<FormState>();
  String? errorText = null;

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SingleChildScrollView(
        child: new Padding(
          padding: new EdgeInsets.all(25.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DefaultLogoHeader(),
              Form(
                key: _formKey,
                child: Scrollbar(
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      children: [
                        ...[
                          Text(
                            context.translate().forgot_pwd,
                            style: AppStyles.defaultTitleStyle.merge(
                              TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            context.translate().forgot_pwd_text,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            textAlign: TextAlign.center,
                          ),
                          DefaultInputForm(inputForm: TypeFormInput('email', context.translate().email), formData: this.formData,),
                          GestureDetector(
                            onTap: () async {
                              FocusScopeNode currentFocus = FocusScope.of(context);

                              if (!currentFocus.hasPrimaryFocus) {
                                currentFocus.unfocus();
                              }

                              if(_formKey.currentState!.validate() && formData.email != null)
                              {
                                late SnackBar snackBar;
                                final network_inf = new NetworkInfoImpl();
                                if(!(await network_inf.isConnected))
                                {
                                  snackBar = SnackBar(
                                    content: Text(
                                      context.translate().no_internet_connection,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    backgroundColor: AppColors.blazeOrange,
                                  );
                                }
                                else
                                {
                                  final result_fgt_pwd = await AuthService().forgot_pwd(formData.email.toString(),);
                                  switch(result_fgt_pwd){
                                    case 200:
                                      snackBar = SnackBar(
                                        content: Text(
                                          context.translate().rst_pwd_success,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.primaryColor,
                                      );
                                      break;
                                    case 404:
                                      snackBar = SnackBar(
                                        content: Text(
                                          context.translate().rst_pwd_error,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.blazeOrange,
                                      );
                                      break;
                                    default:
                                      snackBar = SnackBar(
                                        content: Text(
                                          context.translate().rst_pwd_error_bis,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.blazeOrange,
                                      );
                                  }
                                }
                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.only(top: 20.0),
                              height: 60,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: AppColors.primaryColor,
                                ),
                                color: Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  context.translate().rst_pwd,
                                  style: AppStyles.defaultTitleStyle.merge(
                                    TextStyle(
                                      color: AppColors.primaryColor,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              new InkWell(
                                child: new Text(
                                  context.translate().connect,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).pushReplacementNamed(RouteList.login),
                              ),
                            ],
                          )
                        ].expand(
                              (widget) => [
                            widget,
                            const SizedBox(
                              height: 24,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
