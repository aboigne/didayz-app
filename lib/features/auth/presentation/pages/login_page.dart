import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/widgets/form_data.dart';
import 'package:didayz/core/presentation/widgets/input_form.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:hive/hive.dart';

import '../../../../core/constants/route_list.dart';
import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/logo_header.dart';
import '../../../../core/resources/vectors.dart';
import '../../../../core/theme/app_colors.dart';
import '../../service/auth.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FormData formData = FormData();
  final _formKey = GlobalKey<FormState>();

  void goHome() async {
    final box = await Hive.openBox('connectionBox');
    final alreadyConnected = box.get('alreadyConnected', defaultValue: false);
    if (alreadyConnected) {
      Navigator.of(context).pushReplacementNamed(RouteList.didayz);
    } else {
      box.put('alreadyConnected', true);
      Navigator.of(context).pushReplacementNamed(RouteList.tutorial);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SingleChildScrollView(
        child: new Padding(
          padding: new EdgeInsets.all(25.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DefaultLogoHeader(),
              Form(
                key: _formKey,
                child: Scrollbar(
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      children: [
                        ...[
                          DefaultInputForm(inputForm: TypeFormInput('email', context.translate().email), formData: this.formData,),
                          DefaultInputForm(inputForm: TypeFormInput('password', context.translate().password), formData: this.formData, fromLogin: true,),
                          GestureDetector(
                            onTap: () async {
                              FocusScopeNode currentFocus = FocusScope.of(context);

                              if (!currentFocus.hasPrimaryFocus) {
                                currentFocus.unfocus();
                              }

                              if (_formKey.currentState!.validate() && formData.email != null && formData.password != null) {
                                final network_inf = new NetworkInfoImpl();
                                if(!(await network_inf.isConnected))
                                {
                                  final snackBar = SnackBar(
                                    content: Text(
                                      context.translate().no_internet_connection,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    backgroundColor: AppColors.blazeOrange,
                                  );
                                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                }
                                else
                                {
                                  final result_signin = await AuthService().signin(formData.email.toString(), formData.password.toString());
                                  switch (result_signin) {
                                    case 200:
                                      goHome();
                                      break;
                                    default:
                                      final snackBar = SnackBar(
                                        content: Text(
                                          context.translate().unknown_user,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.blazeOrange,
                                      );
                                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                  }
                                }
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.only(top: 20.0),
                              height: 60,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: AppColors.primaryColor,
                                ),
                                color: Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  context.translate().connexion.toUpperCase(),
                                  style: AppStyles.defaultTitleStyle.merge(
                                    TextStyle(
                                      color: AppColors.primaryColor,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              final network_inf = new NetworkInfoImpl();
                              if(!(await network_inf.isConnected))
                              {
                                final snackBar = SnackBar(
                                  content: Text(
                                    context.translate().no_internet_connection,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                              }
                              else
                              {
                                final result_signin_google = await AuthService().signin_with_google();
                                switch (result_signin_google) {
                                  case 200:
                                    goHome();
                                    break;
                                  default:
                                    final snackBar = SnackBar(
                                      content: Text(
                                        context.translate().unknown_user,
                                        style: AppStyles.defaultTextStyle.merge(
                                          TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      backgroundColor: AppColors.blazeOrange,
                                    );
                                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                }
                              }
                            },
                            child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: AppColors.primaryColor,
                                ),
                                color: Colors.white,
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    width: 25,
                                    height: 25,
                                    margin: const EdgeInsets.only(left: 10.00, right: 10.00),
                                    padding: const EdgeInsets.only(left: 20.00),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      image: DecorationImage(
                                        image: Svg(
                                          Vectors.google_icon,
                                          size: Size(12, 12),
                                        ),
                                        colorFilter: ColorFilter.mode(
                                          AppColors.primaryColor,
                                          BlendMode.srcIn,
                                        ),
                                        fit: BoxFit.scaleDown,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    context.translate().connect_with_google,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new InkWell(
                                child: new Text(
                                  context.translate().not_logged,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).pushReplacementNamed(RouteList.register),
                              ),
                              new InkWell(
                                child: new Text(
                                  context.translate().forgot_pwd,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).pushReplacementNamed(RouteList.forgotpwd),
                              ),
                            ],
                          )
                        ].expand(
                              (widget) =>
                          [
                            widget,
                            const SizedBox(
                              height: 24,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
