import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../core/extension/context.dart';
import '../../../../core/presentation/pages/main_page.dart';
import '../../../../core/resources/vectors.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/theme/spacing.dart';
import '../../../../core/theme/styles.dart';

class StartTutorialPage extends StatefulWidget {
  StartTutorialPage({super.key});

  @override
  State<StartTutorialPage> createState() => _StartTutorialPageState();
}

class _StartTutorialPageState extends State<StartTutorialPage> {
  late List<List<String>> contentText;
  int crtPosition = 0;
  late CarouselSliderController buttonCarouselController;

  @override
  void initState() {
    super.initState();
    buttonCarouselController = CarouselSliderController();
  }

  @override
  void didChangeDependencies() {
    contentText = [
      [
        context.translate().welcome_message,
        context.translate().brief_explanation_1,
        Vectors.didayz,
      ],
      [
        context.translate().tagline,
        context.translate().brief_explanation_2,
      ],
      [
        context.translate().surprise,
        context.translate().brief_explanation_3,
      ],
      [
        context.translate().striking_concepts,
        context.translate().brief_explanation_4,
      ],
    ];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        color: AppColors.primaryColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              flex: 1,
              child: CarouselSlider(
                items: buildPageFromItems(),
                carouselController: buttonCarouselController,
                options: CarouselOptions(
                  height: MediaQuery.of(context).size.height,
                  autoPlay: false,
                  initialPage: 0,
                  enableInfiniteScroll: false,
                  enlargeCenterPage: true,
                  viewportFraction: 1,
                  onPageChanged: (index, page) {
                    setState(() {
                      crtPosition = index;
                    });
                  },
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                for (int i = 0; i < contentText.length; i++)
                  InkWell(
                    child: Container(
                      height: 13,
                      width: 13,
                      margin: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: crtPosition == i ? Colors.white : Color(0xFF999999),
                        shape: BoxShape.circle,
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        crtPosition = i;
                      });
                      buttonCarouselController.jumpToPage(i);
                    },
                  ),
              ],
            ),
            AppGap.large(),
            crtPosition == contentText.length - 1
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const MainPage(),
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().lets_go,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          foregroundColor: WidgetStateProperty.all<Color>(Colors.black),
                          backgroundColor: WidgetStateProperty.all<Color>(Colors.white),
                          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : SizedBox(),
            AppGap.large(),
          ],
        ),
      ),
    );
  }

  List<Widget> buildPageFromItems() {
    final List<Widget> tutorialPages = <Widget>[];

    contentText.forEach((page) {
      tutorialPages.add(
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppGap.veryLarge(),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        page[0],
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                    page.length > 2
                        ? Expanded(
                            child: SvgPicture.asset(
                              page[2],
                              width: 70,
                              height: 70,
                              colorFilter: ColorFilter.mode(
                                Colors.white,
                                BlendMode.srcIn,
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              AppGap.large(),
              Expanded(
                child: Text(
                  page[1],
                  style: AppStyles.defaultTextStyle.merge(
                    TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
    return tutorialPages;
  }
}
