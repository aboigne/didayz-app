import 'dart:convert';

import 'package:didayz/core/constants/api_constants.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

class MessageService {
  final String domain = GlobalConfiguration().getValue('api_base_url');

  Future<int> send_msg(String email, String msg) async {
    var error_code = 0;
    try {
      final body = jsonEncode({'email': email, 'msg': msg});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.send_msg),
        headers: {'Content-Type': 'application/json'},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }
}
