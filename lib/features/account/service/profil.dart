import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

import '../../../core/constants/api_constants.dart';

class ProfilService {
  final String domain = GlobalConfiguration().getValue('api_base_url');

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Future<int> change_info_user(String email, String name, String firstname) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({ 'name': name, 'firstname': firstname, 'email': email, });
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_user),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> change_pwd_user(String old_pwd, String new_pwd) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({ 'password': old_pwd, 'new_password': new_pwd, });
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_user_pwd),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> delete_account_user() async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final result = await http.post(
        Uri.parse(domain + ApiConstants.delete_user),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
      );

      error_code = result.statusCode;
    } on Exception catch (_) {
      error_code = 500;
    }

    return error_code;
  }
}
