import 'dart:convert';

import 'package:didayz/core/constants/api_constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

class ResponseStatistics {
  int error_code;
  List<dynamic>? didayz;
  int? page;
  int? total_page;

  ResponseStatistics(this.error_code, {this.didayz, this.page, this.total_page});
}

class StatisticService {
  final String domain = GlobalConfiguration().getValue('api_base_url');
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Future<ResponseStatistics> getInfos(int page, int limit) async {
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final result = await http.get(
        Uri.parse(domain + ApiConstants.calendarsInfoByUser(page, limit)),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
      );

      ResponseStatistics res_stats = new ResponseStatistics(
        result.statusCode,
        didayz: json.decode(result.body)['calendar'],
        page: json.decode(result.body)['currentPage'],
        total_page: json.decode(result.body)['totalPages'],
      );

      return res_stats;
    } on Exception catch (e) {
      ResponseStatistics res_stats = new ResponseStatistics(
        500,
      );
      return res_stats;
    }
  }
}
