import 'dart:convert';

import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:didayz/core/presentation/widgets/sign_out_alert.dart';
import 'package:didayz/core/presentation/widgets/delete_account_alert.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/features/account/presentation/pages/contact_us.dart';
import 'package:didayz/features/account/presentation/pages/ideas_didayz.dart';
import 'package:didayz/features/account/presentation/pages/statistics.dart';
import 'package:didayz/features/account/presentation/pages/terms_and_conditions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/top_bar.dart';
import '../../../../core/resources/images.dart';
import '../../../../core/theme/styles.dart';
import 'faq.dart';
import 'legal_notice.dart';
import 'my_profil.dart';
import 'prices.dart';
import 'privacy.dart';

class Didayz_User {
  String? name;
  String? first_name;
  String? email;

  Didayz_User({this.name, this.first_name, this.email});
}

class Link_Format {
  IconData icon;
  String title;
  Widget? link;
  bool? is_alert = false;

  Link_Format(this.icon, this.title, {this.link, this.is_alert});
}

class AccountPage extends StatefulWidget {
  const AccountPage({super.key});

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Didayz_User crt_user = Didayz_User();

  List<Link_Format> all_items = [];

  @override
  void initState() {
    getUser();
    super.initState();
  }

  void getUser() async {
    final key_encrypted = await secureStorage.read(
      key: 'encryptionKey',
    );
    final encryptionKey = base64Url.decode(
      key_encrypted!,
    );

    final boxUser = await Hive.openBox(
      'user',
      encryptionCipher: HiveAesCipher(encryptionKey),
    );

    setState(() {
      crt_user = Didayz_User(
          name: boxUser.get('user_name', defaultValue: ''),
          first_name: boxUser.get('user_firstname', defaultValue: ''),
          email: boxUser.get('user_email', defaultValue: ''));
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Link_Format> my_account = [Link_Format(Icons.manage_accounts_outlined, context.translate().my_profile, link: MyProfilPage(crt_user)), Link_Format(Icons.bar_chart_outlined, context.translate().statistics, link: StatisticsPage(),), Link_Format(Icons.logout_outlined, context.translate().sign_out, link: SignOutAlert(), is_alert: true), Link_Format(Icons.delete_outlined, context.translate().delete_account, link: DeleteAccountAlert(), is_alert: true)];
    List<Link_Format> contact = [/*Link_Format(Icons.euro_outlined, context.translate().prices, link: PricePage()),*/ Link_Format(Icons.chat_bubble_outline_outlined, context.translate().contact_us, link: ContactUsPage(email: crt_user.email)), Link_Format(Icons.help_outline_outlined, context.translate().faq, link: FAQPage())];
    List<Link_Format> legal = [Link_Format(Icons.description_outlined, context.translate().terms_and_conditions, link: TermsAndConditionsPage()), Link_Format(Icons.balance_outlined, context.translate().legal_notice, link: LegalNoticePage()), Link_Format(Icons.lock_outlined, context.translate().privacy, link: PrivacyPage())];

    all_items = [];
    all_items.addAll(my_account);
    all_items.addAll(contact);
    all_items.addAll(legal);
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().my_account,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(15.0),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(12),),
                color: Colors.white,
                boxShadow: [
                  new BoxShadow(
                    offset: Offset(0.0, 4.0),
                    color: AppColors.primaryColor.withOpacity(0.1),
                    blurRadius: 20.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      context.translate().need_ideas,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        context.translate().ideas_text,
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            fontWeight: FontWeight.w200,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 4,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: InkWell(
                                child: Text(
                                  context.translate().see_ideas,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: AppColors.primaryColor,
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(createRoute(IdeasDidayzPage()));
                                },
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 5,
                            child: Image.asset(Images.ideas_draw, height: 100,),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: all_items.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: Icon(
                      all_items[index].icon,
                      color: Color(0xFF999999),
                    ),
                    title: Text(
                      all_items[index].title,
                      style: AppStyles.defaultTextStyle,
                    ),
                    trailing: Icon(
                      Icons.chevron_right_outlined,
                      color: AppColors.primaryColor,
                    ),
                    dense: true,
                    visualDensity: VisualDensity(vertical: -3), // to compact
                    onTap: () async {
                      if(all_items[index].link != null)
                      {
                        if(all_items[index].is_alert != null && all_items[index].is_alert!)
                        {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return all_items[index].link!;
                            },
                          );
                        }
                        else
                        {
                          Navigator.of(context).push(createRoute(all_items[index].link));
                        }
                      }
                    },
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    thickness: index + 1 == my_account.length || index + 1 == my_account.length + contact.length ? 5 : 0,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
