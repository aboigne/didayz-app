import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';

import '../../../../core/extension/context.dart';

class PricePage extends StatelessWidget {
  const PricePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().prices,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().price_txt,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontWeight: FontWeight.w200,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
                Table(
                  columnWidths: const <int, TableColumnWidth>{
                    0: FlexColumnWidth(),
                    1: FixedColumnWidth(64),
                  },
                  border: TableBorder.all(),
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  children: [
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0,),
                          child: Text(
                            context.translate().nb_users_didayz,
                            style: AppStyles.defaultTextStyle,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().price,
                            style: AppStyles.defaultTextStyle,
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(1),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '1 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(2),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '2 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(5),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '4 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(10),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '8 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(25),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '20 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(50),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '40 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(100),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '40 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children:[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            context.translate().users(500),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '40 €',
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
