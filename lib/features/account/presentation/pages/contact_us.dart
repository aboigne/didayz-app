import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/account/service/message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/network/network_info.dart';

class ContactUsPage extends StatefulWidget {
  const ContactUsPage({super.key, this.email});

  final String? email;

  @override
  State<ContactUsPage> createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {

  String? subject = null, message = null;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().contact_us,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Scrollbar(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            child: Padding(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: Column(
                children: [
                  ...[
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().subject,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                          ),
                        ),
                        hintText: context.translate().subject,
                        hintStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Color(0xFF999999),
                          ),
                        ),
                        errorStyle: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Color(0xffffaea6),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      cursorColor: Colors.black,
                      cursorHeight: 25.0,
                      obscureText: false,
                      onChanged: (value) {
                        this.setState(() {
                          subject = value;
                        });
                      },
                      validator: (value){
                        if(value == null || value.isEmpty || value == '')
                        {
                          return context.translate().field_required;
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      minLines: 10,
                      textInputAction: TextInputAction.newline,
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().message,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      cursorColor: Colors.black,
                      cursorHeight: 25.0,
                      obscureText: false,
                      onChanged: (value) {
                        this.setState(() {
                          message = value;
                        });
                      },
                      validator: (value){
                        if(value == null || value.isEmpty || value == '')
                        {
                          return context.translate().field_required;
                        }
                        return null;
                      },
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.primaryColor,
                        foregroundColor: Colors.white,
                        minimumSize: Size.fromHeight(60),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      onPressed: () async {
                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }

                        if(_formKey.currentState!.validate())
                        {
                          late SnackBar snackBar;
                          final network_inf = new NetworkInfoImpl();

                          if(!(await network_inf.isConnected))
                          {
                            snackBar = SnackBar(
                              content: Text(
                                context.translate().no_internet_connection,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                          }
                          else
                          {
                            final build_final_msg = context.translate().subject + ' : ' + subject! + '<br><br>' + context.translate().message + ' : <br>' + message!.replaceAll('\n', '<br>');
                            final result_msg_send = await MessageService().send_msg(widget.email!, build_final_msg);
                            switch (result_msg_send) {
                              case 200:
                                snackBar = SnackBar(
                                  content: Text(
                                    context.translate().message_send_success,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.primaryColor,
                                );
                                break;
                              default:
                                snackBar = SnackBar(
                                  content: Text(
                                    context.translate().message_send_error,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                            }
                          }
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      },
                      child: Text(
                        context.translate().send,
                        style: AppStyles.defaultTitleStyle.merge(
                          TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ].expand(
                        (widget) =>
                    [
                      widget,
                      const SizedBox(
                        height: 24,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
