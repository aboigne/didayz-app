import 'package:flutter/material.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/network/network_info.dart';
import '../../../../core/presentation/widgets/loading_widget.dart';
import '../../../../core/presentation/widgets/statistics_alert.dart';
import '../../../../core/presentation/widgets/top_bar.dart';
import '../../../../core/theme/styles.dart';
import '../../service/statistics.dart';

class StatisticsPage extends StatefulWidget {
  const StatisticsPage({Key? key}) : super(key: key);

  @override
  State<StatisticsPage> createState() => _StatisticsPageState();
}

class _StatisticsPageState extends State<StatisticsPage> {
  int page = 1, limit = 10, total_page = 0;
  List<dynamic>? my_didayz = null;
  ScrollController _scrollController = ScrollController();
  bool is_connected = true, is_loading = true;

  @override
  void initState() {
    super.initState();
    getStatistics();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.position.pixels && page < total_page) {
        setState(() {
          page = page + 1;
        });

        getStatistics();
      }
    });
  }

  void getStatistics() async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      StatisticService().getInfos(page, limit).then((value)
        {
          if(value.error_code == 200)
          {
            if(value.didayz != null && value.didayz!.length > 0 && (my_didayz == null || my_didayz!.length == 0))
            {
              setState(() {
                my_didayz = value.didayz!.where((element) => element['IsPublished'] == 1).toList();
                total_page = value.total_page != null ? value.total_page! : 0;
                is_loading = false;
              });
            }
            else if(value.didayz != null && value.didayz!.length > 0)
            {
              List<dynamic>? new_didayz = my_didayz;
              new_didayz!.add(value.didayz!.where((element) => element['IsPublished'] == 1).toList());

              setState(() {
                my_didayz = new_didayz;
                total_page = value.total_page != null ? value.total_page! : 0;
                is_loading = false;
              });
            }
            else
            {
              setState(() {
                is_loading = false;
              });
            }
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().publications_statistics,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                is_connected ?
                (!is_loading ?
                  (my_didayz != null && my_didayz!.length > 0 ?
                    ListView.separated(
                      controller: _scrollController,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: my_didayz!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          trailing: IconButton(
                            icon: Icon(Icons.remove_red_eye_rounded),
                            onPressed: (){
                              Map<String, DateTime> crt_user = {};
                              my_didayz![index]['CalForUser'].forEach((user){
                                crt_user[user['Email']] = DateTime.parse(user['Calendar_User']['createdAt']);
                              });
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return StatisticsAlert(didayz_name: my_didayz![index]['Name'], users: crt_user);
                                },
                              );
                            },
                          ),
                          title: Text(
                            my_didayz![index]['Name'],
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          ),
                          subtitle: Text(
                            context.translate().users(my_didayz![index]['CalForUser'].length),
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                fontSize: 12,
                                color: Color(0xFF999999),
                              ),
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(
                          thickness: 0,
                        );
                      },
                    ) :
                    Text(
                      context.translate().no_publication,
                      style: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    )
                  ) :
                    LoadingWidget()
                ) :
                Text(
                  context.translate().no_internet_connection,
                  style: AppStyles.defaultTextStyle.merge(
                    TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
