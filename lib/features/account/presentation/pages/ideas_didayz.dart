import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';

import '../../../../core/extension/context.dart';

class IdeasDidayzPage extends StatelessWidget {
  const IdeasDidayzPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().didayz_ideas,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().intro_ideas_didayz,
                      style: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    context.translate().ideas_didayz_title1,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().ideas_didayz_text1,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    context.translate().ideas_didayz_title2,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().ideas_didayz_text2,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    context.translate().ideas_didayz_title3,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().ideas_didayz_text3,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    context.translate().ideas_didayz_title4,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().ideas_didayz_text4,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    context.translate().ideas_didayz_title5,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    context.translate().ideas_didayz_text5,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
