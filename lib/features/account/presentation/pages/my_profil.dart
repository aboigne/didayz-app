import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/widgets/form_data.dart';
import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/change_password_alert.dart';
import '../../service/profil.dart';
import 'account_page.dart';

class MyProfilPage extends StatefulWidget {
  const MyProfilPage(this.user, {Key? key}) : super(key: key);

  final Didayz_User user;

  @override
  State<MyProfilPage> createState() => _MyProfilPageState();
}

class _MyProfilPageState extends State<MyProfilPage> {
  FormData formData = FormData();
  final _formKey = GlobalKey<FormState>();

  String? errorTextName = null, errorTextFirstName = null, errorTextEmail = null;

  @override
  void initState() {
    super.initState();

    this.formData.setEmail(widget.user.email!);
    this.formData.setName(widget.user.name!);
    this.formData.setFirstName(widget.user.first_name!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().my_informations,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                ...[
                  TextFormField(
                    autofocus: false,
                    initialValue: this.formData.name,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    style: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 14,
                      ),
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: context.translate().name,
                      hintStyle: AppStyles.inputText.merge(
                        TextStyle(
                          color: Color(0xFF999999),
                        ),
                      ),
                      errorText: errorTextName,
                      errorStyle: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Color(0xffffaea6),
                        ),
                      ),
                      errorMaxLines: 10,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    cursorColor: Colors.black,
                    cursorHeight: 25.0,
                    obscureText: false,
                    onChanged: (value) {
                      this.formData.setName(value);
                    },
                    validator: (val){
                      if(val == null || val.isEmpty)
                      {
                        errorTextName = context.translate().field_required;
                      }
                      else if(val.length > 50)
                      {
                        errorTextName = context.translate().name_format;
                      }
                      else
                      {
                        errorTextName = null;
                      }
                      return errorTextName;
                    },
                  ),
                  TextFormField(
                    autofocus: false,
                    initialValue: this.formData.firstname,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    style: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 14,
                      ),
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: context.translate().firstname,
                      hintStyle: AppStyles.inputText.merge(
                        TextStyle(
                          color: Color(0xFF999999),
                        ),
                      ),
                      errorText: errorTextFirstName,
                      errorStyle: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Color(0xffffaea6),
                        ),
                      ),
                      errorMaxLines: 10,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    cursorColor: Colors.black,
                    cursorHeight: 25.0,
                    obscureText: false,
                    onChanged: (value) {
                      this.formData.setFirstName(value);
                    },
                    validator: (val){
                      if(val == null || val.isEmpty)
                      {
                        errorTextFirstName = context.translate().field_required;
                      }
                      else if(val.length > 50)
                      {
                        errorTextFirstName = context.translate().firstname_format;
                      }
                      else
                      {
                        errorTextFirstName = null;
                      }
                      return errorTextFirstName;
                    },
                  ),
                  TextFormField(
                    autofocus: false,
                    initialValue: this.formData.email,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    style: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 14,
                      ),
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: context.translate().email,
                      hintStyle: AppStyles.inputText.merge(
                        TextStyle(
                          color: Color(0xFF999999),
                        ),
                      ),
                      errorText: errorTextEmail,
                      errorStyle: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Color(0xffffaea6),
                        ),
                      ),
                      errorMaxLines: 10,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    cursorColor: Colors.black,
                    cursorHeight: 25.0,
                    obscureText: false,
                    onChanged: (value) {
                      this.formData.setEmail(value);
                    },
                    validator: (val){
                      if(val == null || val.isEmpty)
                      {
                        errorTextEmail = context.translate().field_required;
                      }
                      else if(!EmailValidator.validate(val))
                      {
                        errorTextEmail = context.translate().valid_email;
                      }
                      else
                      {
                        errorTextEmail = null;
                      }
                      return errorTextEmail;
                    },
                  ),
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () async {
                          FocusScopeNode currentFocus = FocusScope.of(context);

                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          }

                          if(_formKey.currentState!.validate() && formData.email != null && formData.name != null && formData.firstname != null)
                          {
                            SnackBar? snackBar = null;
                            final network_inf = new NetworkInfoImpl();

                            if(!(await network_inf.isConnected))
                            {
                              snackBar = SnackBar(
                                content: Text(
                                  context.translate().no_internet_connection,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                backgroundColor: AppColors.blazeOrange,
                              );
                            }
                            else
                            {
                              final result_change_user = await ProfilService().change_info_user(formData.email!, formData.name!, formData.firstname!);
                              if(result_change_user == 200)
                              {
                                snackBar = SnackBar(
                                  content: Text(
                                    context.translate().success_editing_profile,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.primaryColor,
                                );
                              }
                              else
                              {
                                snackBar = SnackBar(
                                  content: Text(
                                    context.translate().error_editing_profile,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                              }
                            }
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          }
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 20.0),
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: AppColors.primaryColor,
                            ),
                            color: Colors.white,
                          ),
                          child: Center(
                            child: Text(
                              context.translate().save_modifications,
                              style: AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: AppColors.primaryColor,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {
                          FocusScopeNode currentFocus = FocusScope.of(context);

                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          }

                          showDialog(
                            context: context,
                            builder: (context) {
                              return ChangePasswordAlert();
                            },
                          );
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 20.0),
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: AppColors.primaryColor,
                          ),
                          child: Center(
                            child: Text(
                              context.translate().change_password,
                              style: AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ].expand(
                      (widget) => [
                    widget,
                    const SizedBox(
                      height: 24,
                    )
                  ],
                )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
