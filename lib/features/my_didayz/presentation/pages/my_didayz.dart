import 'dart:ui';

import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/didayz_states.dart';
import 'package:didayz/core/presentation/widgets/add_didayz_alert.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/didayz/presentation/blocs/didayz/didayz_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/card_didayz.dart';
import '../../../../core/presentation/widgets/loading_widget.dart';
import '../../../../core/presentation/widgets/route_transition.dart';
import '../../../../core/resources/images.dart';
import '../../../create/presentation/pages/create_page.dart';
import '../../../didayz/domain/entities/didayz.dart';
import '../../../../core/theme/app_colors.dart';

class MyDidayzPage extends StatefulWidget {
  const MyDidayzPage({super.key});

  @override
  State<MyDidayzPage> createState() => _MyDidayzPageState();
}

class _MyDidayzPageState extends State<MyDidayzPage> {
  int page_didayz = 1, total_page_didayz = 0, page_drafts = 1, total_page_drafts = 0, page_published = 1, total_page_published = 0, limit = 10;
  bool? is_connected = null;
  late List<DidayzEntity> my_didayz = [], my_drafts = [], my_published = [];
  ScrollController _scrollControllerDidayz = ScrollController(), _scrollControllerDrafts = ScrollController(), _scrollControllerPublished = ScrollController();
  States_Didayz selection = States_Didayz.received;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    getMyDidayz('init');
    _scrollControllerDidayz.addListener(() {
      if (selection == States_Didayz.received && _scrollControllerDidayz.position.maxScrollExtent ==
          _scrollControllerDidayz.position.pixels && page_didayz < total_page_didayz) {
        page_didayz = page_didayz + 1;
        getMyDidayz('didayz');
      }
    });
    _scrollControllerDrafts.addListener(() {
      if (selection == States_Didayz.draft && _scrollControllerDrafts.position.maxScrollExtent ==
          _scrollControllerDrafts.position.pixels && page_drafts < total_page_drafts) {
        page_drafts = page_drafts + 1;
        getMyDidayz('drafts');
      }
    });
    _scrollControllerPublished.addListener(() {
      if (selection == States_Didayz.published && _scrollControllerPublished.position.maxScrollExtent ==
          _scrollControllerPublished.position.pixels && page_published < total_page_published) {
        page_published = page_published + 1;
        getMyDidayz('published');
      }
    });
    super.initState();
  }

  void getMyDidayz(String source) async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      this.setState(() {
        is_connected = true;
      });

      if(['init', 'didayz'].contains(source))
      {
        context.read<DidayzCubit>().getDidayzByPlayer(page_didayz, limit);
      }
      if(['init', 'drafts'].contains(source))
      {
        context.read<DidayzCreatorCubit>().getDidayzByCreator(page_drafts, limit);
      }
      if(['init', 'published'].contains(source))
      {
        context.read<DidayzCreatorPublishedCubit>().getDidayzByCreator(page_published, limit);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      key: refreshKey,
      onRefresh: () {
        if(selection == States_Didayz.received)
        {
          return context.read<DidayzCubit>().getDidayzByPlayer(page_didayz, limit);
        }
        else if(selection == States_Didayz.draft)
        {
          return context.read<DidayzCreatorCubit>().getDidayzByCreator(page_drafts, limit);
        }
        else
        {
          return context.read<DidayzCreatorPublishedCubit>().getDidayzByCreator(page_published, limit);
        }
      },
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).viewPadding.top,
              color: AppColors.primaryColor,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 20.0,
                left: 20.0,
                right: 20.0,
              ),
              child: Container(
                width: double.infinity,
                height: 65,
                padding: const EdgeInsets.all(4),
                decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(47),
                  ),
                  shadows: [
                    BoxShadow(
                      color: Color(0x1E4437C1),
                      blurRadius: 20,
                      offset: Offset(0, 4),
                      spreadRadius: 0,
                    )
                  ],
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            selection = States_Didayz.received;
                          });
                        },
                        child: Container(
                          height: double.infinity,
                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 2),
                          decoration: selection == States_Didayz.received ? ShapeDecoration(
                            color: AppColors.primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60),
                            ),
                            shadows: [
                              BoxShadow(
                                color: Color(0x304437C1),
                                blurRadius: 20.93,
                                offset: Offset(0, -1),
                                spreadRadius: 0,
                              ),
                            ],
                          ) : null,
                          child: Center(
                            child: Text(
                              context.translate().received(2),
                              textAlign: TextAlign.center,
                              style: selection == States_Didayz.received ? AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ) : AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: AppColors.primaryColor,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            selection = States_Didayz.draft;
                          });
                        },
                        child: Container(
                          height: double.infinity,
                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 2),
                          decoration: selection == States_Didayz.draft ? ShapeDecoration(
                            color: AppColors.primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60),
                            ),
                            shadows: [
                              BoxShadow(
                                color: Color(0x304437C1),
                                blurRadius: 20.93,
                                offset: Offset(0, -1),
                                spreadRadius: 0,
                              ),
                            ],
                          ) : null,
                          child: Center(
                            child: Text(
                              context.translate().draft(2),
                              textAlign: TextAlign.center,
                              style: selection == States_Didayz.draft ? AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ) : AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: AppColors.primaryColor,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            selection = States_Didayz.published;
                          });
                        },
                        child: Container(
                          height: double.infinity,
                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 2),
                          decoration: selection == States_Didayz.published ? ShapeDecoration(
                            color: AppColors.primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60),
                            ),
                            shadows: [
                              BoxShadow(
                                color: Color(0x304437C1),
                                blurRadius: 20.93,
                                offset: Offset(0, -1),
                                spreadRadius: 0,
                              ),
                            ],
                          ) : null,
                          child: Center(
                            child: Text(
                              context.translate().published(2),
                              textAlign: TextAlign.center,
                              style: selection == States_Didayz.published ? AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ) : AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: AppColors.primaryColor,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            is_connected != null && is_connected! ?
              selection == States_Didayz.received ?
                BlocBuilder<DidayzCubit, DidayzState>
                  (builder: (context, state){
                  return state.when(
                    loading: () {
                      if(my_didayz.length == 0)
                      {
                        return Center(
                          child: LoadingWidget(),
                        );
                      }
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: _display_didayz(),
                      );
                    },
                    error: () => Center(
                      child: Text(
                        context.translate().get_didayz_error,
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    loaded: (didayz, page, total_pages) {
                      total_page_didayz = total_pages;
                      my_didayz = [];
                      my_didayz.addAll(didayz);
                      if(my_didayz.length == 0)
                      {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                          child: Container(
                            padding: EdgeInsets.only(bottom: 15.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(24),),
                              color: Colors.white,
                              boxShadow: [
                                new BoxShadow(
                                  offset: Offset(0.0, 4.0),
                                  color: AppColors.primaryColor.withOpacity(0.1),
                                  blurRadius: 20.0,
                                ),
                              ],
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Container(
                                    height: 215,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(18.0),),
                                      color: AppColors.mauve,
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Image.asset(Images.no_didayz),
                                    ),
                                  ),
                                ),
                                Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 25.0,),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          context.translate().no_didayz_yet,
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 25.0,
                                        right: 25.0,
                                        top: 15.0,
                                      ),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          context.translate().add_first_didayz,
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                          style: AppStyles.defaultDescStyle.merge(
                                            TextStyle(
                                              color: AppColors.grey,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 25.0,
                                        left: 25.0,
                                        right: 25.0,
                                      ),
                                      child: GestureDetector(
                                        onTap: () {
                                          showDialog(
                                            context: context,
                                            builder: (context) {
                                              return AddDidayzAlert();
                                            },
                                          );
                                        },
                                        child: Container(
                                          height: 50,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: AppColors.primaryColor,
                                          ),
                                          child: Center(
                                            child: Text(
                                              context.translate().lets_go,
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      }

                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AddDidayzAlert();
                                  },
                                );
                              },
                              child: Container(
                                margin: const EdgeInsets.only(top: 20.0),
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                    color: AppColors.primaryColor,
                                  ),
                                  color: Colors.white,
                                ),
                                child: Center(
                                  child: Text(
                                    context.translate().add_didayz,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            _display_didayz(),
                          ],
                        ),
                      );
                    },
                  );
                },
                ) :
                selection == States_Didayz.draft ?
                  BlocBuilder<DidayzCreatorCubit, DidayzState>(builder: (context, state){
                    return state.when(
                      loading: () {
                        if(my_drafts.length == 0)
                        {
                          return Center(
                            child: LoadingWidget(),
                          );
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: _display_didayz(),
                        );
                      },
                      error: () => Center(
                        child: Text(
                          context.translate().get_didayz_error,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      loaded: (didayz, page, total_pages) {
                        total_page_drafts = total_pages;
                        my_drafts = [];
                        my_drafts.addAll(didayz);
                        if(my_drafts.length == 0)
                        {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                            child: Container(
                              padding: EdgeInsets.only(bottom: 15.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(24),),
                                color: Colors.white,
                                boxShadow: [
                                  new BoxShadow(
                                    offset: Offset(0.0, 4.0),
                                    color: AppColors.primaryColor.withOpacity(0.1),
                                    blurRadius: 20.0,
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Container(
                                      height: 215,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(18.0),),
                                        color: AppColors.mauve,
                                      ),
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(Images.no_didayz),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 25.0,),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            context.translate().no_didayz_yet,
                                            softWrap: true,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: AppStyles.defaultTextStyle.merge(
                                              TextStyle(
                                                color: Colors.black,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 25.0,
                                          right: 25.0,
                                          top: 15.0,
                                        ),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            context.translate().create_first_didayz,
                                            softWrap: true,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: AppStyles.defaultDescStyle.merge(
                                              TextStyle(
                                                color: AppColors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 25.0,
                                          left: 25.0,
                                          right: 25.0,
                                        ),
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).push(createRoute(CreatePage()));
                                          },
                                          child: Container(
                                            height: 50,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10),
                                              color: AppColors.primaryColor,
                                            ),
                                            child: Center(
                                              child: Text(
                                                context.translate().lets_go,
                                                style: AppStyles.defaultTitleStyle.merge(
                                                  TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        }

                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(createRoute(CreatePage()));
                                },
                                child: Container(
                                  margin: const EdgeInsets.only(top: 20.0),
                                  height: 60,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                      color: AppColors.primaryColor,
                                    ),
                                    color: Colors.white,
                                  ),
                                  child: Center(
                                    child: Text(
                                      context.translate().create_didayz,
                                      style: AppStyles.defaultTitleStyle.merge(
                                        TextStyle(
                                          color: AppColors.primaryColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              _display_didayz(),
                            ],
                          ),
                        );
                      },
                    );
                  },
                  ) :
                  BlocBuilder<DidayzCreatorPublishedCubit, DidayzState>(builder: (context, state){
                    return state.when(
                      loading: () {
                        if(my_published.length == 0)
                        {
                          return Center(
                            child: LoadingWidget(),
                          );
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: _display_didayz(),
                        );
                      },
                      error: () => Center(
                        child: Text(
                          context.translate().get_didayz_error,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      loaded: (didayz, page, total_pages) {
                        total_page_published = total_pages;
                        my_published = [];
                        my_published.addAll(didayz);
                        if(my_published.length == 0)
                        {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                            child: Container(
                              padding: EdgeInsets.only(bottom: 15.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(24),),
                                color: Colors.white,
                                boxShadow: [
                                  new BoxShadow(
                                    offset: Offset(0.0, 4.0),
                                    color: AppColors.primaryColor.withOpacity(0.1),
                                    blurRadius: 20.0,
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Container(
                                      height: 215,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(18.0),),
                                        color: AppColors.mauve,
                                      ),
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(Images.no_didayz),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 25.0,),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            context.translate().no_didayz_yet,
                                            softWrap: true,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: AppStyles.defaultTextStyle.merge(
                                              TextStyle(
                                                color: Colors.black,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 25.0,
                                          right: 25.0,
                                          top: 15.0,
                                        ),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            context.translate().publish_first_didayz,
                                            softWrap: true,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: AppStyles.defaultDescStyle.merge(
                                              TextStyle(
                                                color: AppColors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: _display_didayz(),
                        );
                      },
                    );
                  },
                  ) :
            is_connected != null && !is_connected! ?
              Align(
                alignment: Alignment.center,
                child: Text(
                  context.translate().no_internet_connection,
                  style: AppStyles.defaultTextStyle.merge(
                    TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ) :
            LoadingWidget(),
          ],
        ),
      ),
    );
  }

  Widget _display_didayz() {
    List<DidayzEntity> all_didayz = [];
    if(selection == States_Didayz.received)
    {
      all_didayz.addAll(my_didayz);
    }
    else if(selection == States_Didayz.draft)
    {
      all_didayz.addAll(my_drafts);
    }
    else if(selection == States_Didayz.published)
    {
      all_didayz.addAll(my_published);
    }

    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: all_didayz.length,
      padding: EdgeInsets.only(top: 15.0),
      controller: selection == States_Didayz.received ? _scrollControllerDidayz : (selection == States_Didayz.draft ? _scrollControllerDrafts : _scrollControllerPublished),
      itemBuilder: (context, index) {
        return CardDidayz(all_didayz[index], selection, refreshKey: refreshKey,);
      },
    );
  }
}
