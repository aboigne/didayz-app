part of '../../../di/injection_container.dart';

void _featureDidayzDayDetail() {
  sl
  // DataSources
    ..injectDataSource<DidayzDayDetailRemoteDataSource>(() => DidayzDayDetailRemoteDataSourceImpl(dio: dio))

  // Repositories
    ..injectRepository<DidayzDayDetailRepository>(
          () => DidayzDayDetailRepositoryImpl(
                  remoteDataSource: sl(),
                  networkInfo: sl(),
                ),
    )

  // UseCases
    ..injectUseCase(() => GetDidayzDayDetailUseCase(sl()))

  // Blocs
    ..injectBloc(() => DidayzDayDetailCubit(didayzDayDetailById: sl(),));

  // Interfaces
}
