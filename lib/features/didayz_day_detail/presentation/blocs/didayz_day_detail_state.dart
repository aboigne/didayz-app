part of 'didayz_day_detail_cubit.dart';

@freezed
class DidayzDayDetailState with _$DidayzDayDetailState {
  const DidayzDayDetailState._();

  const factory DidayzDayDetailState.loading() = _Loading;

  const factory DidayzDayDetailState.error() = _Error;

  const factory DidayzDayDetailState.loaded({
    required DidayzDayEntity day,
  }) = _Loaded;
}
