import 'package:bloc/bloc.dart';
import 'package:didayz/features/didayz/domain/entities/didayz_day.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/usecases/get_didayz_detail_usecase.dart';

part 'didayz_day_detail_state.dart';
part 'didayz_day_detail_cubit.freezed.dart';

class DidayzDayDetailCubit extends Cubit<DidayzDayDetailState> {
  DidayzDayDetailCubit({required this.didayzDayDetailById}) : super(DidayzDayDetailState.loading());

  final GetDidayzDayDetailUseCase didayzDayDetailById;

  Future<void> getDidayzDayDetail(int cal_id, int id) async {
    emit(DidayzDayDetailState.loading());
    final result = await didayzDayDetailById(GetDidayzDayDetailUseCaseParams(cal_id: cal_id, id: id));

    result.when(
      success: (didayzDayDetail) {
        emit(DidayzDayDetailState.loaded(day: didayzDayDetail));
      },
      failure: (failure) {
        emit(DidayzDayDetailState.error());
      },
    );
  }
}
