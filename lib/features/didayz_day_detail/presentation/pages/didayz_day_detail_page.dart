import 'dart:io';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:didayz/core/constants/api_constants.dart';
import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/file.dart';
import 'package:didayz/core/presentation/widgets/loading_widget.dart';
import 'package:didayz/core/presentation/widgets/pdf_reader.dart';
import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/resources/vectors.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/spacing.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/didayz_day_detail/presentation/blocs/didayz_day_detail_cubit.dart';
import 'package:didayz/features/didayz_day_detail/service/didayz_day_detail.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:spotify/spotify.dart' as spotify_pck;
import 'package:url_launcher/url_launcher.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/widgets/param_day_alert.dart';
import '../../../../core/presentation/widgets/see_content_alert.dart';
import '../../../../core/resources/images.dart';

import 'dart:developer';


class TrackData {
  String? title;
  String? id;
  String? album;
  List<String>? artists;
  String? image;
  String? spotify_uri;

  TrackData(this.title, this.id, this.album, this.artists, this.image, this.spotify_uri);
}

class DidayzDayDetailPage extends StatefulWidget {
  const DidayzDayDetailPage(this.calId, this.dayId, {super.key, this.content_type = 'Texte'});

  final int calId;
  final int dayId;
  final String? content_type;

  @override
  State<DidayzDayDetailPage> createState() => _DidayzDayDetailPageState();
}

class _DidayzDayDetailPageState extends State<DidayzDayDetailPage> {
  final String _base_url = GlobalConfiguration().getValue('api_base_url');
  final _formKey = GlobalKey<FormState>();
  String? intro_msg = null, texte = null, type_video = null, video_id = null, type_audio = null, spotify_id = null, access_code = null;
  bool? send_mail = null, param_access_code = null;
  XFile? pic = null;
  final ImagePicker _picker = ImagePicker();
  File? audio_file = null, pdf_file = null, video_file = null;
  bool update_audio_widget = false, media_updated = false, is_connected = true;
  ImageSource? media_selected = null;
  ImageProvider? videoThumbnail;

  late TextEditingController _titleController, _albumController, _artistController;
  int currentIndexCarousel = 0;
  TrackData? init_track = null;
  List<TrackData> tracks = [];
  late spotify_pck.SpotifyApi spotifyApi;
  late spotify_pck.SpotifyApiCredentials credentials;
  String clientId = GlobalConfiguration().getValue('spotify_client_id');
  String clientSecret = GlobalConfiguration().getValue('spotify_client_secret');

  @override
  void initState() {
    getMyDay();
    super.initState();
    _titleController = TextEditingController();
    _albumController = TextEditingController();
    _artistController = TextEditingController();
    credentials = spotify_pck.SpotifyApiCredentials(clientId, clientSecret);
    spotifyApi = spotify_pck.SpotifyApi(credentials);
  }

  void getMyDay() async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      context.read<DidayzDayDetailCubit>().getDidayzDayDetail(widget.calId, widget.dayId);
    }
  }

  @override
  void dispose() {
    _titleController.dispose();
    _albumController.dispose();
    _artistController.dispose();
    super.dispose();
  }

  String _display_date(int number, int month, int year) {
    final DateTime date_to_display = DateTime.parse(year.toString() + month.toString().padLeft(2, '0') + number.toString().padLeft(2, '0'));
    return DateFormat.MMMMd('fr').format(date_to_display);
  }

  //we can upload image from camera or from gallery based on parameter
  Future getMedia(ImageSource media, String type_file) async {
    this.setState(() {
      media_selected = media;
    });
    XFile? selected_file;
    bool error = false;
    if(media == ImageSource.gallery)
    {
      if(type_file == 'Video') {
        selected_file = await _picker.pickVideo(source: ImageSource.gallery);
      }
      else {
        selected_file = await _picker.pickImage(source: ImageSource.gallery);
      }
    }
    else if(media == ImageSource.camera)
    {
      if(type_file == 'Video') {
        selected_file = await _picker.pickVideo(source: ImageSource.camera);
      }
      else {
        selected_file = await _picker.pickImage(source: ImageSource.camera);
      }
    }

    if(selected_file != null && !error)
    {
      if(type_file == 'Picture')
      {
        setState(() {
          pic = selected_file;
          media_updated = true;
        });
      }
      else if(type_file == 'Video')
      {
        remove_media('Video');
        final File? f = File(selected_file.path);
        setState(() {
          media_updated = true;
          video_file = f;
        });
        if(f != null)
        {
          getVideoThumbnail(f.path, from_local: true);
        }
      }
    }
  }

  void alert_media(String type_file) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          insetPadding: EdgeInsets.all(20.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  context.translate().media_to_use,
                  style: AppStyles.defaultTitleStyle.merge(
                    TextStyle(
                      color: AppColors.primaryColor,
                    ),
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                    getMedia(ImageSource.gallery, type_file);
                  },
                  child: Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.primaryColor,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.image,
                          color: Colors.white,
                        ),
                        AppGap.regular(),
                        Text(
                          context.translate().from_gallery,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                    getMedia(ImageSource.camera, type_file);
                  },
                  child: Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.primaryColor,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.camera,
                          color: Colors.white,
                        ),
                        AppGap.regular(),
                        Text(
                          context.translate().from_camera,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void remove_media(String type_file) {
    if(type_file == 'Picture')
    {
      setState(() {
        media_updated = true;
        pic = null;
      });
    }
    else if(type_file == 'Video')
    {
      setState(() {
        media_updated = true;
        video_id = null;
        video_file = null;
      });
    }
    else if(type_file == 'Sound')
    {
      setState(() {
        media_updated = true;
        audio_file = null;
        spotify_id = null;
      });
    }
    else if(type_file == 'File')
    {
      setState(() {
        media_updated = true;
        pdf_file = null;
      });
    }
  }

  void launch_snackbar_error(){
    final snackBar = SnackBar(
      content: Text(
        context.translate().error_updating_day,
        style: AppStyles.defaultTextStyle.merge(
          TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: AppColors.blazeOrange,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void launch_snackbar_no_space(){
    final snackBar = SnackBar(
      content: Text(
        context.translate().insufficient_space_didayz,
        style: AppStyles.defaultTextStyle.merge(
          TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: AppColors.blazeOrange,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  getVideoThumbnail(String filename, {bool from_local = false}) async {
    final thumbnailAsUint8List = await VideoThumbnail.thumbnailData(
      video: from_local ? filename : _base_url + ApiConstants.getFile(widget.calId, filename),
      imageFormat: ImageFormat.JPEG,
      maxWidth: 320,
      quality: 50,
    );
    setState(() {
      videoThumbnail = MemoryImage(thumbnailAsUint8List!);
    });
  }

  getAudioSpotifyImage(String idSpotify) async {
    spotifyApi.tracks.get(
      idSpotify,
    ).then((track) {
      List<String> artists_name = [];
      track.artists!.forEach((artist) {
        artists_name.add(artist.name!);
      });

      setState(() {
        init_track = new TrackData(track.name, track.id, track.album!.name, artists_name, track.album!.images![0].url, 'http://open.spotify.com/track/' + track.id!);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if(is_connected)
    {
      return BlocBuilder<DidayzDayDetailCubit, DidayzDayDetailState>
        (builder: (context, state)
      {
        return state.when(
          loading: () =>
              Scaffold(
                appBar: TopBar(),
                body: Center(
                  child: LoadingWidget(),
                ),
              ),
          error: () =>
              Scaffold(
                appBar: TopBar(),
                body: Center(
                  child: Text(
                    context.translate().get_day_error,
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
          loaded: (day) {
            String? msg_introduction = day.IntroMessage,
                type_day = day.Type,
                texte_day = day.Texte,
                pic_day = day.Picture,
                vid_day = day.Video, type_vid_day = day.VideoType,
                aud_day = day.Sound, type_aud_day = day.SoundType,
                pdf_day = day.File,
                access_code_day = day.Code;
            final bool send_mail_day = day.SendResponse;

            if(videoThumbnail == null && (type_video == 'File' || (type_video == null && (type_vid_day == null || type_vid_day == 'File'))) &&
                vid_day != null && vid_day != '' && vid_day.contains('_day_vid') && !media_updated) getVideoThumbnail(vid_day);

            if(init_track == null && type_aud_day == 'Spotify' && aud_day != null && !media_updated) getAudioSpotifyImage(aud_day);

            return Scaffold(
              appBar: TopBar(
                title: Text(
                  _display_date(day.Number, day.Month, day.Year),
                  style: AppStyles.defaultTitleStyle.merge(
                    TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              body: Form(
                key: _formKey,
                child: Scrollbar(
                  child: SingleChildScrollView(
                    physics: ScrollPhysics(),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20.0,
                        right: 20.0,
                        bottom: 20.0,
                        top: 30.0,
                      ),
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              context.translate().content,
                              style: AppStyles.defaultTitleStyle.merge(
                                TextStyle(
                                  color: AppColors.primaryColor,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                context.translate().form_introductory_message_not_mandatory,
                                style: AppStyles.defaultTitleStyle.merge(
                                  TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          TextFormField(
                            initialValue: msg_introduction,
                            scrollPadding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                            ),
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            minLines: 3,
                            textInputAction: TextInputAction.newline,
                            style: AppStyles.inputText.merge(
                              TextStyle(
                                fontSize: 14,
                              ),
                            ),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(15.0),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            cursorColor: Colors.black,
                            cursorHeight: 25.0,
                            obscureText: false,
                            onChanged: (value) {
                              this.setState(() {
                                intro_msg = value;
                              });
                            },
                          ),
                          if(widget.content_type == 'Texte' || widget.content_type == null) Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    context.translate().content,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              TextFormField(
                                initialValue: texte_day,
                                scrollPadding: EdgeInsets.only(
                                  bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                ),
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                minLines: 5,
                                textInputAction: TextInputAction.newline,
                                style: AppStyles.inputText.merge(
                                  TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(15.0),
                                  filled: true,
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                cursorColor: Colors.black,
                                cursorHeight: 25.0,
                                obscureText: false,
                                onChanged: (value) {
                                  this.setState(() {
                                    texte = value;
                                  });
                                },
                              ),
                            ],
                          ),
                          if(widget.content_type == 'Picture') Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    context.translate().import_pic,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: double.infinity,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: (pic_day == null || pic_day == '' || media_updated) && pic == null ? AppColors.primaryColor : null,
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  image: (pic_day != null && pic_day != '' && !media_updated) || pic != null ? DecorationImage(
                                    image: pic_day != null && pic_day != '' && !media_updated ?
                                    Image.network(
                                      _base_url + ApiConstants.getFile(widget.calId, pic_day),
                                      key: UniqueKey(),
                                      loadingBuilder: (context, child, loadingProgress) {
                                        if (loadingProgress == null) {
                                          return child;
                                        } else {
                                          return Center(
                                            child: CircularProgressIndicator(),
                                          );
                                        }
                                      },
                                      errorBuilder: (context, obj, stackTrace) {
                                        return new Image.asset(
                                          Images.didayz_blue,
                                          key: UniqueKey(),
                                          cacheWidth: 400,
                                        );
                                      },
                                    ).image : pic != null ?
                                    Image.file(
                                      File(pic!.path),
                                      width: 200,
                                      height: 200,
                                    ).image :
                                    Image.asset(
                                      Images.didayz_blue,
                                      key: UniqueKey(),
                                      cacheWidth: 400,
                                    ).image,
                                    fit: BoxFit.cover,
                                  ) : null,
                                ),
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: GestureDetector(
                                        onTap: () {
                                          alert_media('Picture');
                                        },
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(50.0),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                            child: Container(
                                              width: 100,
                                              height: 100,
                                              decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.1),
                                                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                              ),
                                              child: SvgPicture.asset(
                                                Vectors.pic_icon,
                                                height: 23,
                                                width: 28,
                                                color: Colors.white,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    if((pic_day != null && pic_day != '' && !media_updated) || pic != null) Align(
                                      alignment: Alignment.bottomRight,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) {
                                                return SeeContentAlert(
                                                  widget.calId,
                                                  'Picture',
                                                  picture_file_from_url: (pic_day != null && pic_day != '' && !media_updated) ? pic_day : null,
                                                  picture_file_from_local: (media_updated && pic != null) ? pic : null,
                                                );
                                              },
                                            );
                                          },
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(50.0),
                                            child: BackdropFilter(
                                              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                              child: Container(
                                                padding: const EdgeInsets.all(8.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.white.withOpacity(0.1),
                                                  borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                                ),
                                                child: Icon(
                                                  Icons.visibility_rounded,
                                                  color: Colors.white,
                                                  size: 25,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          if(widget.content_type == 'Video') Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () async {
                                          this.setState(() {
                                            type_video = 'File';
                                          });
                                        },
                                        child: Container(
                                          height: 45,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: (type_video == 'File' || (type_video == null && (type_vid_day == null || type_vid_day == 'File'))) ? AppColors.primaryColor : AppColors.primaryColor.withOpacity(0.1),
                                          ),
                                          child: Center(
                                            child: Text(
                                              context.translate().my_phone,
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: (type_video == 'File' || (type_video == null && (type_vid_day == null || type_vid_day == 'File'))) ? Colors.white : AppColors.primaryColor,
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8.0,),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () async {
                                          this.setState(() {
                                            type_video = 'Youtube';
                                          });
                                        },
                                        child: Container(
                                          height: 45,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: (type_video == 'Youtube' || (type_video == null && type_vid_day == 'Youtube')) ? AppColors.primaryColor : AppColors.primaryColor.withOpacity(0.1),
                                          ),
                                          child: Center(
                                            child: Text(
                                              context.translate().youtube,
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: (type_video == 'Youtube' || (type_video == null && type_vid_day == 'Youtube')) ? Colors.white : AppColors.primaryColor,
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              if(type_video == 'File' || (type_video == null && (type_vid_day == null || type_vid_day == 'File'))) Container(
                                width: double.infinity,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: ((vid_day == null || vid_day == '' || !vid_day.contains('_day_vid')) && !media_updated) || (media_updated && video_file == null) ? AppColors.primaryColor : null,
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  image: (vid_day != null && vid_day != '' && vid_day.contains('_day_vid') && !media_updated) || (media_updated && video_file != null) ? DecorationImage(
                                    image: ((vid_day != null && vid_day != '' && vid_day.contains('_day_vid') && !media_updated) || (media_updated && video_file != null)) && videoThumbnail != null ?
                                    videoThumbnail! : Image.asset(
                                      Images.didayz_blue,
                                      key: UniqueKey(),
                                      cacheWidth: 400,
                                    ).image,
                                    fit: BoxFit.cover,
                                  ) : null,
                                ),
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: GestureDetector(
                                        onTap: () {
                                          alert_media('Video');
                                        },
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(50.0),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                            child: Container(
                                              width: 100,
                                              height: 100,
                                              decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.1),
                                                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                              ),
                                              child: SvgPicture.asset(
                                                Vectors.pic_icon,
                                                height: 23,
                                                width: 28,
                                                color: Colors.white,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    if((vid_day != null && vid_day != '' && vid_day.contains('_day_vid') && !media_updated) || (media_updated && video_file != null)) Align(
                                      alignment: Alignment.bottomRight,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) {
                                                return SeeContentAlert(
                                                  widget.calId,
                                                  'Video',
                                                  video_file_from_url: (vid_day != null && vid_day != '' && vid_day.contains('_day_vid') && !media_updated) ? vid_day : null,
                                                  video_file_from_local: (media_updated && video_file != null) ? video_file : null,
                                                );
                                              },
                                            );
                                          },
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(50.0),
                                            child: BackdropFilter(
                                              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                              child: Container(
                                                padding: const EdgeInsets.all(8.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.white.withOpacity(0.1),
                                                  borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                                ),
                                                child: Icon(
                                                  Icons.visibility_rounded,
                                                  color: Colors.white,
                                                  size: 25,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              if(type_video == 'Youtube' || (type_video == null && type_vid_day == 'Youtube' && vid_day != null && YoutubePlayer.convertUrlToId(vid_day) != null)) Column(
                                children: [
                                  if(type_video == null && type_vid_day == 'Youtube' && vid_day != null && YoutubePlayer.convertUrlToId(vid_day) != null) Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Container(
                                      width: double.infinity,
                                      height: 150,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        image: DecorationImage(
                                          image: Image.network(
                                            'https://img.youtube.com/vi/' + YoutubePlayer.convertUrlToId(vid_day)! + '/0.jpg',
                                            key: UniqueKey(),
                                            loadingBuilder: (context, child, loadingProgress) {
                                              if (loadingProgress == null) {
                                                return child;
                                              } else {
                                                return Center(
                                                  child: CircularProgressIndicator(),
                                                );
                                              }
                                            },
                                            errorBuilder: (context, obj, stackTrace) {
                                              return new Image.asset(
                                                Images.didayz_blue,
                                                key: UniqueKey(),
                                                cacheWidth: 400,
                                              );
                                            },
                                          ).image,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return SeeContentAlert(
                                                    widget.calId,
                                                    'Video',
                                                    video_file_from_youtube: YoutubePlayer.convertUrlToId(vid_day)!,
                                                  );
                                                },
                                              );
                                            },
                                            child: ClipRRect(
                                              borderRadius: BorderRadius.circular(50.0),
                                              child: BackdropFilter(
                                                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                                child: Container(
                                                  padding: const EdgeInsets.all(8.0),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white.withOpacity(0.1),
                                                    borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                                  ),
                                                  child: Icon(
                                                    Icons.visibility_rounded,
                                                    color: Colors.white,
                                                    size: 25,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  TextFormField(
                                    initialValue: (type_video == null && type_vid_day == 'Youtube' && vid_day != null) ? YoutubePlayer.convertUrlToId(vid_day) : null,
                                    scrollPadding: EdgeInsets.only(
                                      bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                    ),
                                    keyboardType: TextInputType.text,
                                    textInputAction: TextInputAction.next,
                                    style: AppStyles.inputText.merge(
                                      TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(15.0),
                                      filled: true,
                                      fillColor: Colors.white,
                                      hintText: context.translate().type_youtube_id,
                                      hintStyle: AppStyles.inputText.merge(
                                        TextStyle(
                                          color: Color(0xFF999999),
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                    cursorColor: Colors.black,
                                    cursorHeight: 25.0,
                                    obscureText: false,
                                    onChanged: (value) {
                                      String? id = YoutubePlayer.convertUrlToId(
                                        value,
                                      ) ?? '';

                                      this.setState(() {
                                        video_id = id;
                                        media_updated = true;
                                        video_file = null;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                          if(widget.content_type == 'Sound') Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () async {
                                          this.setState(() {
                                            type_audio = 'File';
                                          });
                                        },
                                        child: Container(
                                          height: 45,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: (type_audio == 'File' || (type_audio == null && (type_aud_day == null || type_aud_day == 'File'))) ? AppColors.primaryColor : AppColors.primaryColor.withOpacity(0.1),
                                          ),
                                          child: Center(
                                            child: Text(
                                              context.translate().my_phone,
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: (type_audio == 'File' || (type_audio == null && (type_aud_day == null || type_aud_day == 'File'))) ? Colors.white : AppColors.primaryColor,
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8.0,),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () async {
                                          this.setState(() {
                                            type_audio = 'Spotify';
                                          });
                                        },
                                        child: Container(
                                          height: 45,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: (type_audio == 'Spotify' || (type_audio == null && type_aud_day == 'Spotify')) ? AppColors.primaryColor : AppColors.primaryColor.withOpacity(0.1),
                                          ),
                                          child: Center(
                                            child: Text(
                                              context.translate().day_sound_type_spotify,
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: (type_audio == 'Spotify' || (type_audio == null && type_aud_day == 'Spotify')) ? Colors.white : AppColors.primaryColor,
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              if(type_audio == 'File' || (type_audio == null && (type_aud_day == null || type_aud_day == 'File'))) Container(
                                width: double.infinity,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryColor,
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                ),
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: GestureDetector(
                                        onTap: () async {
                                          FilePickerResult? result_sound = await FilePicker.platform.pickFiles(
                                            type: FileType.audio,
                                          );
                                          if (result_sound != null) {
                                            remove_media('Sound');
                                            setState(() {
                                              audio_file = File(result_sound.files.single.path!);
                                              media_updated = true;
                                            });
                                          }
                                        },
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(50.0),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                            child: Container(
                                              width: 100,
                                              height: 100,
                                              decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.1),
                                                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                              ),
                                              child: SvgPicture.asset(
                                                Vectors.sound_rec_icon,
                                                height: 23,
                                                width: 28,
                                                color: Colors.white,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    if((aud_day != null && aud_day != '' && aud_day.contains('_day_aud') && !media_updated) || (media_updated && audio_file != null)) Align(
                                      alignment: Alignment.bottomRight,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) {
                                                return SeeContentAlert(
                                                  widget.calId,
                                                  'Sound',
                                                  audio_file_from_url: (aud_day != null && aud_day != '' && aud_day.contains('_day_aud') && !media_updated) ? aud_day : null,
                                                  audio_file_from_local: (media_updated && audio_file != null) ? audio_file : null,
                                                );
                                              },
                                            );
                                          },
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(50.0),
                                            child: BackdropFilter(
                                              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                              child: Container(
                                                padding: const EdgeInsets.all(8.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.white.withOpacity(0.1),
                                                  borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                                ),
                                                child: Icon(
                                                  Icons.visibility_rounded,
                                                  color: Colors.white,
                                                  size: 25,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              if(type_audio == 'Spotify' || (type_audio == null && type_aud_day == 'Spotify' && aud_day != null)) Column(
                                children: [
                                  ...[
                                    if(tracks.length > 0) Column(
                                      children: [
                                        CarouselSlider(
                                          items: buildCarousel(tracks),
                                          options: CarouselOptions(
                                            autoPlay: false,
                                            initialPage: 0,
                                            enableInfiniteScroll: false,
                                            enlargeCenterPage: true,
                                            viewportFraction: 1,
                                            onPageChanged: (val, _) {
                                              setState(() {
                                                currentIndexCarousel = val;
                                              });
                                            },
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: buildIndicatorsCarousel(tracks.length),
                                          ),
                                        )
                                      ],
                                    ),
                                    if(tracks.length == 0 && type_audio == null && type_aud_day == 'Spotify' && aud_day != null) Container(
                                      width: double.infinity,
                                      height: 150,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        image: init_track != null ? DecorationImage(
                                          image: Image.network(
                                            init_track!.image!,
                                          ).image,
                                          fit: BoxFit.cover,
                                        ) : null,
                                      ),
                                      child: Stack(
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: const EdgeInsets.all(12.0),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(36.0),
                                                child: BackdropFilter(
                                                  filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(12.0),
                                                    decoration: BoxDecoration(
                                                      color: Colors.black.withOpacity(0.2),
                                                      borderRadius: BorderRadius.all(Radius.circular(36.0),),
                                                    ),
                                                    child: Text(
                                                      init_track != null && init_track!.title != null ? init_track!.title! : '',
                                                      style: AppStyles.defaultTitleStyle.merge(
                                                        TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.bottomRight,
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: GestureDetector(
                                                onTap: () async {
                                                  String final_uri = init_track!.spotify_uri!.replaceFirst('http', 'https');
                                                  final Uri url = Uri.parse(final_uri);
                                                  // Check if Spotify is installed
                                                  if(await canLaunchUrl(url)) {
                                                    // Launch the url which will open Spotify
                                                    launchUrl(url);
                                                  }
                                                },
                                                child: ClipRRect(
                                                  borderRadius: BorderRadius.circular(50.0),
                                                  child: BackdropFilter(
                                                    filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                                    child: Container(
                                                      padding: const EdgeInsets.all(8.0),
                                                      decoration: BoxDecoration(
                                                        color: Colors.white.withOpacity(0.1),
                                                        borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                                      ),
                                                      child: Icon(
                                                        Icons.visibility_rounded,
                                                        color: Colors.white,
                                                        size: 25,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    TextFormField(
                                      controller: _titleController,
                                      scrollPadding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                      ),
                                      keyboardType: TextInputType.text,
                                      textInputAction: TextInputAction.next,
                                      style: AppStyles.inputText.merge(
                                        TextStyle(
                                          fontSize: 14,
                                        ),
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(15.0),
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText: context.translate().optional_title,
                                        hintStyle: AppStyles.inputText.merge(
                                          TextStyle(
                                            color: Color(0xFF999999),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                      ),
                                      cursorColor: Colors.black,
                                      cursorHeight: 25.0,
                                      obscureText: false,
                                    ),
                                    TextFormField(
                                      controller: _albumController,
                                      scrollPadding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                      ),
                                      keyboardType: TextInputType.text,
                                      textInputAction: TextInputAction.next,
                                      style: AppStyles.inputText.merge(
                                        TextStyle(
                                          fontSize: 14,
                                        ),
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(15.0),
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText: context.translate().optional_album,
                                        hintStyle: AppStyles.inputText.merge(
                                          TextStyle(
                                            color: Color(0xFF999999),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                      ),
                                      cursorColor: Colors.black,
                                      cursorHeight: 25.0,
                                      obscureText: false,
                                    ),
                                    TextFormField(
                                      controller: _artistController,
                                      scrollPadding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                      ),
                                      keyboardType: TextInputType.text,
                                      textInputAction: TextInputAction.next,
                                      style: AppStyles.inputText.merge(
                                        TextStyle(
                                          fontSize: 14,
                                        ),
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(15.0),
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText: context.translate().optional_artist,
                                        hintStyle: AppStyles.inputText.merge(
                                          TextStyle(
                                            color: Color(0xFF999999),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                      ),
                                      cursorColor: Colors.black,
                                      cursorHeight: 25.0,
                                      obscureText: false,
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        FocusScopeNode currentFocus = FocusScope.of(context);

                                        if (!currentFocus.hasPrimaryFocus) {
                                          currentFocus.unfocus();
                                        }

                                        String q = '';
                                        if(_titleController.text.isNotEmpty) {
                                          q += '%20track:' + _titleController.text;
                                        }

                                        if(_albumController.text.isNotEmpty) {
                                          q += '%20album:' + _albumController.text;
                                        }

                                        if(_artistController.text.isNotEmpty) {
                                          q += '%20artist:' + _artistController.text;
                                        }

                                        final resultsTracks = await spotifyApi.search.get(
                                          q,
                                          types: [spotify_pck.SearchType.track],
                                        ).first(20);

                                        List<TrackData> my_tracks = [];

                                        resultsTracks.forEach((pages){
                                          if (pages.items == null || pages.items!.length == 0) {
                                            SnackBar snackBar = SnackBar(
                                              content: Text(
                                                context.translate().no_result_found,
                                                style: AppStyles.defaultTextStyle.merge(
                                                  TextStyle(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              backgroundColor: AppColors.blazeOrange,
                                            );
                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                          }

                                          pages.items!.forEach((track) {
                                            List<String> artists_name = [];
                                            track.artists!.forEach((artist) {
                                              artists_name.add(artist.name);
                                            });

                                            TrackData crtTrack = new TrackData(track.name, track.id, track.album.name, artists_name, track.album.images[0].url, 'http://open.spotify.com/track/' + track.id);
                                            my_tracks.add(crtTrack);
                                          });
                                        });
                                        setState(() {
                                          tracks = my_tracks;
                                        });
                                      },
                                      child: Container(
                                        height: 60,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                          color: AppColors.primaryColor,
                                        ),
                                        child: Center(
                                          child: Text(
                                            context.translate().search,
                                            style: AppStyles.defaultTitleStyle.merge(
                                              TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ].expand(
                                        (widget) => [
                                      widget,
                                      const SizedBox(
                                        height: 8,
                                      )
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                          if(widget.content_type == 'File') Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    context.translate().import_file,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: double.infinity,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryColor,
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                ),
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: GestureDetector(
                                        onTap: () async {
                                          FilePickerResult? result_file = await FilePicker.platform.pickFiles(
                                            type: FileType.custom,
                                            allowedExtensions: ['pdf'],
                                          );
                                          if (result_file != null) {
                                            remove_media('File');
                                            setState(() {
                                              pdf_file = File(result_file.files.single.path!);
                                              media_updated = true;
                                            });
                                          }
                                        },
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(50.0),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                            child: Container(
                                              width: 100,
                                              height: 100,
                                              decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.1),
                                                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                              ),
                                              child: SvgPicture.asset(
                                                Vectors.upload_file_icon,
                                                color: Colors.white,
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    if((pdf_day != null && pdf_day != '' && !media_updated) || (media_updated && pdf_file != null)) Align(
                                      alignment: Alignment.bottomRight,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).push(
                                              createRoute(
                                                PDFReaderFile(
                                                  pdfFile: (pdf_day != null && pdf_day != '' && !media_updated) ? _base_url + ApiConstants.getFile(widget.calId, pdf_day) : pdf_file!.path,
                                                  is_picked: (pdf_day != null && pdf_day != '' && !media_updated) ? false : true,
                                                ),
                                              ),
                                            );
                                          },
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(50.0),
                                            child: BackdropFilter(
                                              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                              child: Container(
                                                padding: const EdgeInsets.all(8.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.white.withOpacity(0.1),
                                                  borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                                ),
                                                child: Icon(
                                                  Icons.visibility_rounded,
                                                  color: Colors.white,
                                                  size: 25,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30.0, bottom: 20.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                context.translate().settings,
                                style: AppStyles.defaultTitleStyle.merge(
                                  TextStyle(
                                    color: AppColors.primaryColor,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    context.translate().access_code,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                  Switch(
                                    value: param_access_code != null ? param_access_code! : (access_code_day != null && access_code_day != '' ? true : false),
                                    activeColor: AppColors.primaryColor,
                                    onChanged: (bool value) {
                                      setState(() {
                                        param_access_code = value;
                                      });
                                    },
                                  )
                                ],
                              ),
                              if(param_access_code != null ? param_access_code! : (access_code_day != null && access_code_day != '' ? true : false)) TextFormField(
                                initialValue: access_code != null ? access_code : access_code_day,
                                scrollPadding: EdgeInsets.only(
                                  bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                ),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                style: AppStyles.inputText.merge(
                                  TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(15.0),
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: context.translate().many_access_code,
                                  hintStyle: TextStyle(
                                    fontFamily: 'Raleway',
                                    fontSize: 14,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                cursorColor: Colors.black,
                                cursorHeight: 25.0,
                                obscureText: false,
                                onChanged: (value) {
                                  this.setState(() {
                                    access_code = value;
                                  });
                                },
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0, bottom: 20.0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: GestureDetector(
                                    onTap: () {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return ParamDayAlert(true);
                                        },
                                      );
                                    },
                                    child: Text(
                                      context.translate().how_it_works,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Color(0xFF7B7B7B),
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    context.translate().receive_answer,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                  Switch(
                                    value: send_mail != null ? send_mail! : send_mail_day,
                                    activeColor: AppColors.primaryColor,
                                    onChanged: (bool value) {
                                      setState(() {
                                        send_mail = value;
                                      });
                                    },
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0, bottom: 20.0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: GestureDetector(
                                    onTap: () {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return ParamDayAlert(false);
                                        },
                                      );
                                    },
                                    child: Text(
                                      context.translate().how_it_works,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Color(0xFF7B7B7B),
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: GestureDetector(
                              onTap: () async {
                                FocusScopeNode currentFocus = FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }

                                if(
                                (widget.content_type == null &&
                                    (
                                        ((type_day == 'Texte' || type_day == null) && (texte == '' || (texte == null && texte_day == null)))
                                            ||
                                            (media_updated && (
                                                (type_day == 'Picture' && pic == null)
                                                    || (type_day == 'Video' && video_file == null && video_id == null)
                                                    || (type_day == 'Sound' && audio_file == null && spotify_id == null)
                                                    || (type_day == 'File' && pdf_file == null)
                                            ))
                                    )
                                ) ||
                                    (
                                        (widget.content_type == 'Texte' && (texte == '' || (texte == null && texte_day == null)))
                                            ||
                                            (media_updated && (
                                                (widget.content_type == 'Picture' && pic == null)
                                                    || (widget.content_type == 'Video' && video_file == null && video_id == null)
                                                    || (widget.content_type == 'Sound' && audio_file == null && spotify_id == null)
                                                    || (widget.content_type == 'File' && pdf_file == null)
                                            ))
                                    ))
                                {
                                  final SnackBar snackBar = SnackBar(
                                    content: Text(
                                      context.translate().error_content_day_empty,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    backgroundColor: AppColors.blazeOrange,
                                  );
                                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                  return;
                                }

                                final network_inf = new NetworkInfoImpl();
                                if(!(await network_inf.isConnected))
                                {
                                  final snackBar = SnackBar(
                                    content: Text(
                                      context.translate().no_internet_connection,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    backgroundColor: AppColors.blazeOrange,
                                  );
                                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                  return;
                                }

                                if(intro_msg != null && intro_msg != msg_introduction)
                                {
                                  final result_updt_intro_msg = await DidayzDayDetailService().change_intro_msg(widget.calId, widget.dayId, intro_msg);
                                  if(result_updt_intro_msg != 200) {
                                    launch_snackbar_error();
                                    return;
                                  }
                                }

                                if(access_code != null && access_code != access_code_day)
                                {
                                  final result_updt_code = await DidayzDayDetailService().change_code(widget.calId, widget.dayId, access_code);
                                  if(result_updt_code != 200) {
                                    launch_snackbar_error();
                                    return;
                                  }
                                }

                                if(send_mail != null && send_mail != send_mail_day)
                                {
                                  final result_updt_send_mail = await DidayzDayDetailService().change_send_rep(widget.calId, widget.dayId, send_mail!);
                                  if(result_updt_send_mail != 200) {
                                    launch_snackbar_error();
                                    return;
                                  }
                                }

                                if(widget.content_type != null && widget.content_type != type_day)
                                {
                                  final result_updt_type = await DidayzDayDetailService().change_type(widget.calId, widget.dayId, widget.content_type == null ? 'Texte' : widget.content_type!);
                                  if(result_updt_type != 200) {
                                    launch_snackbar_error();
                                    return;
                                  }

                                  if(type_day == 'Texte' || (type_day == null && widget.content_type != 'Texte'))/* Si l'ancien type était un texte */
                                  {
                                    final result_clear_texte = await DidayzDayDetailService().change_texte(widget.calId, widget.dayId, null);
                                    if(result_clear_texte != 200) {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                  else if(type_day == 'Picture' && pic_day != null)/* Si l'ancien type était une image */
                                  {
                                    final result_updt_pic = await DidayzDayDetailService().change_pic(widget.calId, widget.dayId, null, 0);
                                    if(result_updt_pic == 200)
                                    {
                                      await FileService().delete_file('didayz_' + widget.calId.toString(), pic_day, widget.calId);
                                    }
                                    else {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                  else if(type_day == 'Video')/* Si l'ancien type était une vidéo */
                                  {
                                    final result_updt_videotype = await DidayzDayDetailService().change_video_type(widget.calId, widget.dayId, null);
                                    if(result_updt_videotype == 200 && vid_day != null)
                                    {
                                      //On vide le champ Video (et on supprime le fichier potentiel du serveur)
                                      final result_updt_video = await DidayzDayDetailService().change_vid(widget.calId, widget.dayId, null, type_video, 0);
                                      if(result_updt_video == 200)
                                      {
                                        await FileService().delete_file('didayz_' + widget.calId.toString(), vid_day, widget.calId);
                                      }
                                      else {
                                        launch_snackbar_error();
                                        return;
                                      }
                                    }
                                    else {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                  else if(type_day == 'Sound')/* Si l'ancien type était un son */
                                  {
                                    final result_updt_audiotype = await DidayzDayDetailService().change_audio_type(widget.calId, widget.dayId, null);
                                    if(result_updt_audiotype == 200 && aud_day != null)
                                    {
                                      //On vide le champ Sound (et on supprime le fichier potentiel du serveur)
                                      final result_updt_audio = await DidayzDayDetailService().change_aud(widget.calId, widget.dayId, null, type_audio, 0);
                                      if(result_updt_audio == 200)
                                      {
                                        await FileService().delete_file('didayz_' + widget.calId.toString(), aud_day, widget.calId);
                                      }
                                      else {
                                        launch_snackbar_error();
                                        return;
                                      }
                                    }
                                    else {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                  else if(type_day == 'File')/* Si l'ancien type était un fichier */
                                  {
                                    //On vide le champ File (et on supprime le fichier du serveur)
                                    final result_updt_file = await DidayzDayDetailService().change_file(widget.calId, widget.dayId, null, 0);
                                    if(result_updt_file == 200 && pdf_day != null)
                                    {
                                      await FileService().delete_file('didayz_' + widget.calId.toString(), pdf_day, widget.calId);
                                    }
                                    else {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                }

                                if((widget.content_type == 'Texte' || (widget.content_type == null && (type_day == 'Texte' || type_day == null))) && texte != null && texte != texte_day)/* Si le type est un Texte et qu'il a été modifié */
                                {
                                  final result_updt_texte = await DidayzDayDetailService().change_texte(widget.calId, widget.dayId, texte);
                                  if(result_updt_texte != 200) {
                                    launch_snackbar_error();
                                    return;
                                  }
                                }

                                if((widget.content_type == 'Picture' || (widget.content_type == null && type_day == 'Picture')) && media_updated)/* Si le type est une Image et qu'elle a été modifiée */
                                {
                                  int pic_size = 0;
                                  File? pic_file = null;
                                  if(pic != null)
                                  {
                                    pic_file = await File(pic!.path);
                                    pic_size = pic_file.lengthSync();
                                  }

                                  final result_updt_pic = await DidayzDayDetailService().change_pic(widget.calId, widget.dayId, pic != null ? media_selected == ImageSource.gallery ? pic!.name : 'from_camera.jpeg' : null, pic_size);
                                  if(result_updt_pic == 200)
                                  {
                                    if(pic != null && pic_file != null)
                                    {
                                      final result_upload_file = await FileService().upload_file_day(pic_file, '_day_pic', widget.calId, widget.dayId, media_selected == ImageSource.gallery ? '0' : '1');

                                      if(result_upload_file != 200)
                                      {
                                        launch_snackbar_error();
                                        return;
                                      }
                                    }
                                    else if(pic_day != null)
                                    {
                                      await FileService().delete_file('didayz_' + widget.calId.toString(), pic_day, widget.calId);
                                    }
                                  }
                                  else if(result_updt_pic == 507)
                                  {
                                    launch_snackbar_no_space();
                                    return;
                                  }
                                  else {
                                    launch_snackbar_error();
                                    return;
                                  }
                                }

                                if(widget.content_type == 'Video' || (widget.content_type == null && type_day == 'Video'))/* Si le type est une Vidéo */
                                {
                                  if(type_video != null && type_video != type_vid_day)/* Si le type de vidéo a été modifié */
                                  {
                                    final result_updt_videotype = await DidayzDayDetailService().change_video_type(widget.calId, widget.dayId, type_video!);
                                    if(result_updt_videotype != 200) {
                                      launch_snackbar_error();
                                      return;
                                    }

                                    if((type_vid_day == 'File' || type_vid_day == null) && vid_day != null)/* Si l'ancien type de vidéo était un fichier */
                                    {
                                      await FileService().delete_file('didayz_' + widget.calId.toString(), vid_day, widget.calId);
                                    }
                                  }

                                  if(media_updated)
                                  {
                                    String vid_type = (type_video == 'File' || (type_video == null && (type_vid_day == 'File' || type_vid_day == null))) ? 'File' : 'Youtube';
                                    String? video_name = vid_type == 'File' ? video_file!.path.split('/').last : video_id;

                                    int vid_size = 0;
                                    if(vid_type == 'File' && video_file != null)
                                    {
                                      vid_size = video_file!.lengthSync();
                                    }

                                    final result_updt_video = await DidayzDayDetailService().change_vid(widget.calId, widget.dayId, video_name, vid_type, vid_size);

                                    if(result_updt_video == 200)
                                    {
                                      if(vid_type == 'File' && video_file != null)
                                      {
                                        final result_upload_file = await FileService().upload_file_day(video_file!, '_day_vid', widget.calId, widget.dayId, media_selected == ImageSource.gallery ? '0' : '2');

                                        if(result_upload_file != 200)
                                        {
                                          launch_snackbar_error();
                                          return;
                                        }
                                      }
                                    }
                                    else if(result_updt_video == 507)
                                    {
                                      launch_snackbar_no_space();
                                      return;
                                    }
                                    else {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                }

                                if(widget.content_type == 'Sound' || (widget.content_type == null && type_day == 'Sound'))/* Si le type est un Son */
                                {
                                  if(type_audio != null && type_audio != type_aud_day)/* Si le type de son a été modifié */
                                  {
                                    final result_updt_audiotype = await DidayzDayDetailService().change_audio_type(widget.calId, widget.dayId, type_audio!);
                                    if(result_updt_audiotype != 200) {
                                      launch_snackbar_error();
                                      return;
                                    }

                                    if((type_aud_day == 'File' || type_aud_day == null) && aud_day != null)/* Si l'ancien type de Son était un fichier */
                                    {
                                      await FileService().delete_file('didayz_' + widget.calId.toString(), aud_day, widget.calId);
                                    }
                                  }

                                  if(media_updated)
                                  {
                                    String aud_type = (type_audio == 'File' || (type_audio == null && (type_aud_day == 'File' || type_aud_day == null))) ? 'File' : 'Spotify';
                                    String? audio_name = aud_type == 'File' ? audio_file!.path.split('/').last : spotify_id;

                                    int aud_size = 0;
                                    if(aud_type == 'File' && audio_file != null)
                                    {
                                      aud_size = audio_file!.lengthSync();
                                    }

                                    final result_updt_audio = await DidayzDayDetailService().change_aud(widget.calId, widget.dayId, audio_name, aud_type, aud_size);

                                    if(result_updt_audio == 200)
                                    {
                                      if(aud_type == 'File' && audio_file != null)
                                      {
                                        final result_upload_file = await FileService().upload_file_day(audio_file!, '_day_aud', widget.calId, widget.dayId, '0');
                                        if(result_upload_file != 200)
                                        {
                                          launch_snackbar_error();
                                          return;
                                        }
                                      }
                                    }
                                    else if(result_updt_audio == 507)
                                    {
                                      launch_snackbar_no_space();
                                      return;
                                    }
                                    else {
                                      launch_snackbar_error();
                                      return;
                                    }
                                  }
                                }

                                if((widget.content_type == 'File' || (widget.content_type == null && type_day == 'File')) && media_updated)/* Si le type est un Fichier et qu'il a été modifié */
                                {
                                  int pdf_size = 0;
                                  if(pdf_file != null)
                                  {
                                    pdf_size = pdf_file!.lengthSync();
                                  }

                                  final result_updt_file = await DidayzDayDetailService().change_file(widget.calId, widget.dayId, pdf_file != null ? pdf_file!.path.split('/').last : null, pdf_size);
                                  if(result_updt_file == 200)
                                  {
                                    if(pdf_file != null)
                                    {
                                      final result_upload_file = await FileService().upload_file_day(pdf_file!, '_day_file', widget.calId, widget.dayId, '0');

                                      if(result_upload_file != 200)
                                      {
                                        launch_snackbar_error();
                                        return;
                                      }
                                    }
                                    else if(pic_day != null)
                                    {
                                      await FileService().delete_file('didayz_' + widget.calId.toString(), pdf_day!, widget.calId);
                                    }
                                  }
                                  else if(result_updt_file == 507)
                                  {
                                    launch_snackbar_no_space();
                                    return;
                                  }
                                  else {
                                    launch_snackbar_error();
                                    return;
                                  }
                                }

                                final snackBar = SnackBar(
                                  content: Text(
                                    context.translate().update_day_success,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.primaryColor,
                                );
                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                context.read<DidayzDayDetailCubit>().getDidayzDayDetail(widget.calId, widget.dayId);
                              },
                              child: Container(
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: AppColors.primaryColor,
                                ),
                                child: Center(
                                  child: Text(
                                    context.translate().save_modifications,
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
      );
    }
    else
    {
      return Scaffold(
        appBar: TopBar(),
        body: Center(
          child: Text(
            context.translate().no_internet_connection,
            style: AppStyles.defaultTextStyle.merge(
              TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ),
      );
    }
  }

  List<Widget> buildCarousel(List<TrackData> tracks) {
    final List<Widget> listTracks = <Widget>[];

    tracks.forEach((trck) {
      listTracks.add(
        Container(
          height: 150,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0),),
            border: trck.id == spotify_id ? Border.all(
              color: AppColors.primaryColor,
              width: 2.0,
            ) : null,
            image: DecorationImage(
              image: Image.network(
                trck.image!,
              ).image,
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(36.0),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                      child: Container(
                        padding: const EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.2),
                          borderRadius: BorderRadius.all(Radius.circular(36.0),),
                        ),
                        child: Text(
                          trck.title != null ? trck.title! : '',
                          style: AppStyles.defaultTitleStyle.merge(
                            TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: GestureDetector(
                    onTap: () {
                      this.setState(() {
                        spotify_id = spotify_id == trck.id ? null : trck.id;
                        media_updated = true;
                      });
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(36.0),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                        child: Container(
                          padding: const EdgeInsets.all(12.0),
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.2),
                            borderRadius: BorderRadius.all(Radius.circular(36.0),),
                          ),
                          child: Text(
                            spotify_id == trck.id ? context.translate().unselect : context.translate().select,
                            style: AppStyles.defaultTitleStyle.merge(
                              TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () async {
                      String final_uri = trck.spotify_uri!.replaceFirst('http', 'https');
                      final Uri url = Uri.parse(final_uri);
                      // Check if Spotify is installed
                      if(await canLaunchUrl(url)) {
                        // Launch the url which will open Spotify
                        launchUrl(url);
                      }
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0),),
                          ),
                          child: Icon(
                            Icons.visibility_rounded,
                            color: Colors.white,
                            size: 25,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
    return listTracks;
  }

  List<Widget> buildIndicatorsCarousel(int length_msg) {
    final List<Widget> listInd = <Widget>[];

    for(int idx=0; idx < length_msg; idx++)
    {
      if(idx == currentIndexCarousel)
      {
        listInd.add(
          Container(
            height: 5.0,
            width: 10.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: AppColors.primaryColor,
            ),
          ),
        );
      }
      else
      {
        listInd.add(
          Container(
            height: 5.0,
            width: 5.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AppColors.primaryColor,
            ),
          ),
        );
      }

      if(idx < length_msg - 1)
      {
        listInd.add(
          SizedBox(
            width: 5.0,
          ),
        );
      }
    }

    return listInd;
  }
}
