import 'dart:convert';
import 'dart:ui';

import 'package:video_player/video_player.dart';
import 'package:audioplayers/audioplayers.dart' as AudioPlay;
import 'package:didayz/core/constants/api_constants.dart';
import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/widgets/loading_widget.dart';
import 'package:didayz/core/presentation/widgets/pdf_reader.dart';
import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/didayz_day_detail/presentation/blocs/didayz_day_detail_cubit.dart';
import 'package:didayz/features/didayz_day_detail/service/didayz_day_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:spotify/spotify.dart' as spotify_pck;
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../../core/extension/context.dart';
import 'didayz_day_detail_page.dart';

class DidayzDayDetailReadOnlyPage extends StatefulWidget {
  const DidayzDayDetailReadOnlyPage(this.calId, this.dayId, {super.key, this.isVideoYoutube, this.video});

  final int calId;
  final int dayId;
  final bool? isVideoYoutube;
  final String? video;

  @override
  State<DidayzDayDetailReadOnlyPage> createState() => _DidayzDayDetailReadOnlyPageState();
}

class _DidayzDayDetailReadOnlyPageState extends State<DidayzDayDetailReadOnlyPage> {
  final String _base_url = GlobalConfiguration().getValue('api_base_url');

  String? textToSend = null;
  bool codeOk = false, skip_intro = false, is_connected = true;
  late TextEditingController _controllerTextCode;

  /* Vidéo */
  Duration _videoposition = new Duration();
  /* From Local or URL */
  VideoPlayerController? _videoPlayerController = null;
  //CustomVideoPlayerController? _customVideoPlayerController = null;
  /* From Youtube */
  YoutubePlayerController? _controllerYT;
  bool _youtubeismute = false;
  String? youtube_title, youtube_channel;

  /* Audio */
  AudioPlay.AudioPlayer player = AudioPlay.AudioPlayer();
  int maxduration = 100, currentpos = 0;
  /* From Spotify */
  TrackData? init_track = null;
  late spotify_pck.SpotifyApi spotifyApi;
  late spotify_pck.SpotifyApiCredentials credentials;
  String clientId = GlobalConfiguration().getValue('spotify_client_id');
  String clientSecret = GlobalConfiguration().getValue('spotify_client_secret');

  @override
  void initState() {
    getMyDay();
    _controllerTextCode = TextEditingController();
    credentials = spotify_pck.SpotifyApiCredentials(clientId, clientSecret);
    spotifyApi = spotify_pck.SpotifyApi(credentials);

    if(widget.video != null && YoutubePlayer.convertUrlToId(widget.video!) != null && widget.isVideoYoutube != null && widget.isVideoYoutube!)
    {
      _controllerYT = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(widget.video!)!,
        flags: const YoutubePlayerFlags(
          hideControls: true,
          hideThumbnail: true,
          mute: false,
          autoPlay: false,
          disableDragSeek: false,
          loop: false,
          isLive: false,
          forceHD: false,
          enableCaption: true,
        ),
      )..addListener(() {
        setState(() {
          _videoposition = _controllerYT!.value.position;
        });
      });
      getYoutubeMetadata(YoutubePlayer.convertUrlToId(widget.video!)!);
    }
    if(widget.video != null && (widget.isVideoYoutube == null || !widget.isVideoYoutube!))
    {
      _videoPlayerController = VideoPlayerController.network(
        _base_url + ApiConstants.getFile(widget.calId, widget.video!),
      )..addListener(() {
        setState(() {
          _videoposition = _videoPlayerController!.value.position;
        });
      })..initialize().then((_) {
        setState(() {});
      });
    }

    /*if(_videoPlayerController != null){
      _customVideoPlayerController = CustomVideoPlayerController(
        context: context,
        videoPlayerController: _videoPlayerController!,
      );
    }*/

    super.initState();
  }

  void getYoutubeMetadata(String id_yt) async {
    String yt_url = "https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=$id_yt&format=json";
    var res = await http.get(Uri.parse(yt_url));
    try {
      if (res.statusCode == 200) {
        setState(() {
          youtube_title = json.decode(res.body)['title'];
          youtube_channel = json.decode(res.body)['author_name'];
        });
      }
    } on FormatException catch (e) {
      print('invalid JSON' + e.toString());
    }
  }

  void getMyDay() async {
    final network_inf = new NetworkInfoImpl();
    if(!(await network_inf.isConnected))
    {
      this.setState(() {
        is_connected = false;
      });
    }
    else
    {
      context.read<DidayzDayDetailCubit>().getDidayzDayDetail(widget.calId, widget.dayId);
    }
  }

  @override
  void dispose() {
    _controllerTextCode.dispose();
    if(_controllerYT != null) _controllerYT!.dispose();
    if(_videoPlayerController != null) _videoPlayerController!.dispose();
    //if(_customVideoPlayerController != null) _customVideoPlayerController!.dispose();
    super.dispose();
  }

  String _display_date(int number, int month, int year) {
    DateTime date_to_display = DateTime.parse(year.toString() + month.toString().padLeft(2, '0') + number.toString().padLeft(2, '0'));
    return DateFormat.MMMMd('fr').format(date_to_display);
  }

  getAudioSpotifyImage(String idSpotify) async {
    spotifyApi.tracks.get(
      idSpotify,
    ).then((track) {
      List<String> artists_name = [];
      track.artists!.forEach((artist) {
        artists_name.add(artist.name!);
      });

      setState(() {
        init_track = new TrackData(track.name, track.id, track.album!.name, artists_name, track.album!.images![0].url, 'http://open.spotify.com/track/' + track.id!);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if(is_connected)
    {
      return BlocBuilder<DidayzDayDetailCubit, DidayzDayDetailState>
        (builder: (context, state)
      {
        return state.when(
          loading: () =>
              Scaffold(
                appBar: TopBar(),
                body: Center(
                  child: LoadingWidget(),
                ),
              ),
          error: () {
            return Scaffold(
              appBar: TopBar(),
              body: Center(
                child: Text(
                  context.translate().get_day_error,
                  style: AppStyles.defaultTextStyle.merge(
                    TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            );
          },
          loaded: (day) {
            String? msg_introduction = day.IntroMessage,
                type_day = day.Type,
                texte_day = day.Texte,
                pic_day = day.Picture,
                vid_day = day.Video, type_vid_day = day.VideoType,
                aud_day = day.Sound, type_aud_day = day.SoundType,
                pdf_day = day.File,
                access_code_day = day.Code;
            final bool send_mail_day = day.SendResponse;
            final int id_cal = day.calendarId;

            if(type_day == 'Sound' && (type_aud_day == 'File' || type_aud_day == null) && aud_day != null)
            {
              Future.delayed(Duration.zero, () async {
                player.onDurationChanged.listen((d) {
                  setState(() {
                    maxduration = d.inMilliseconds;
                  });
                });

                player.onPositionChanged.listen((p){
                  setState(() {
                    currentpos = p.inMilliseconds;
                  });
                });
              });
            }

            if(init_track == null && type_aud_day == 'Spotify' && aud_day != null) getAudioSpotifyImage(aud_day);

            return Scaffold(
              appBar: TopBar(
                title: Text(
                  _display_date(day.Number, day.Month, day.Year),
                  style: AppStyles.defaultTitleStyle.merge(
                    TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              body: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      physics: ScrollPhysics(),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Column(
                              children: [
                                _display_day_content(type_day != null ? type_day : 'Texte', id_cal, texte_day, pic_day, vid_day, type_vid_day, aud_day, type_aud_day, pdf_day),
                                if (send_mail_day) Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      ...[
                                        Text(
                                          context.translate().answer,
                                          textAlign: TextAlign.left,
                                          style: AppStyles.defaultTitleStyle.merge(
                                            TextStyle(
                                                color: Colors.black,
                                                fontSize: 16.0,
                                            ),
                                          ),
                                        ),
                                        TextField(
                                          scrollPadding: EdgeInsets.only(
                                            bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                                          ),
                                          keyboardType: TextInputType.multiline,
                                          textInputAction: TextInputAction.newline,
                                          maxLines: null,
                                          minLines: 3,
                                          style: AppStyles.inputText.merge(
                                            TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(15.0),
                                            filled: true,
                                            fillColor: Colors.white,
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                          ),
                                          cursorColor: Colors.black,
                                          cursorHeight: 25.0,
                                          obscureText: false,
                                          onChanged: (value) {
                                            this.setState(() {
                                              textToSend = value;
                                            });
                                          },
                                        ),
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: AppColors.primaryColor,
                                            foregroundColor: Colors.white,
                                            minimumSize: Size.fromHeight(60),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(10.0),
                                            ),
                                          ),
                                          onPressed: () async {
                                            FocusScopeNode currentFocus = FocusScope.of(context);

                                            if (!currentFocus.hasPrimaryFocus) {
                                              currentFocus.unfocus();
                                            }

                                            if(textToSend != null)
                                            {
                                              final result_updt_pic = await DidayzDayDetailService().send_response(widget.calId, _display_date(day.Number, day.Month, day.Year), textToSend!);
                                              if(result_updt_pic == 200)
                                              {
                                                SnackBar snackBar = SnackBar(
                                                  content: Text(
                                                    context.translate().answer_send_success,
                                                    style: AppStyles.defaultTextStyle.merge(
                                                      TextStyle(
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                  backgroundColor: AppColors.primaryColor,
                                                );
                                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                              }
                                              else
                                              {
                                                SnackBar snackBar = SnackBar(
                                                  content: Text(
                                                    context.translate().answer_send_error,
                                                    style: AppStyles.defaultTextStyle.merge(
                                                      TextStyle(
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                  backgroundColor: AppColors.blazeOrange,
                                                );
                                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                              }
                                            }
                                            else
                                            {
                                              SnackBar snackBar = SnackBar(
                                                content: Text(
                                                  context.translate().answer_send_required,
                                                  style: AppStyles.defaultTextStyle.merge(
                                                    TextStyle(
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                                backgroundColor: AppColors.blazeOrange,
                                              );
                                              ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                            }
                                          },
                                          child: Text(
                                            context.translate().send,
                                            style: AppStyles.defaultTitleStyle.merge(
                                              TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ].expand(
                                            (widget) => [
                                          widget,
                                          const SizedBox(
                                            height: 10,
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            if ((access_code_day == null || access_code_day == '' || codeOk) && msg_introduction != null && !skip_intro) Positioned(
                              child: Container(
                                height: MediaQuery.of(context).size.height - (MediaQuery.of(context).padding.top + kToolbarHeight) - 50,
                                child: BackdropFilter(
                                  filter: ImageFilter.blur(
                                    sigmaX: 10,
                                    sigmaY: 10,
                                  ),
                                  child: Dialog(
                                    insetPadding: EdgeInsets.all(10.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: _intro_msgContent(msg_introduction),
                                  ),
                                ),
                              ),
                            ),
                            if (access_code_day != null && access_code_day != '' && !codeOk) Positioned(
                              child: Container(
                                height: MediaQuery.of(context).size.height - (MediaQuery.of(context).padding.top + kToolbarHeight) - 50,
                                child: BackdropFilter(
                                  filter: ImageFilter.blur(
                                    sigmaX: 10,
                                    sigmaY: 10,
                                  ),
                                  child: Dialog(
                                    insetPadding: EdgeInsets.all(10.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: _codeContent(access_code_day),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  if (((access_code_day == null || access_code_day == '' || codeOk) && (msg_introduction == null || skip_intro)) && type_day == 'Video' && type_vid_day == 'Youtube' && _controllerYT != null) Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(20.0),
                    color: AppColors.primaryColor,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              _videoposition.inMinutes.toString() + ':' + (_videoposition.inSeconds % 60).toString().padLeft(2, '0'),
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Slider(
                              inactiveColor: Colors.white.withOpacity(0.5),
                              activeColor: Colors.white,
                              thumbColor: Colors.white,
                              value: _controllerYT!.value.metaData.duration.inMilliseconds > 0 ? (_videoposition.inMilliseconds / _controllerYT!.value.metaData.duration.inMilliseconds) * 100 : 0,
                              min: 0.0,
                              max: 100.0,
                              onChanged: (value) {
                                double new_position = (value * _controllerYT!.value.metaData.duration.inMilliseconds) / 100;
                                _controllerYT!.seekTo(Duration(milliseconds: new_position.round()));
                                setState(() {
                                  _videoposition = Duration(milliseconds: new_position.round());
                                });
                              },
                            ),
                            Text(
                              _controllerYT!.value.metaData.duration.inMinutes.toString() + ':' + (_controllerYT!.value.metaData.duration.inSeconds % 60).toString().padLeft(2, '0'),
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                double newpos = _videoposition.inSeconds - 5.0;
                                _controllerYT!.seekTo(Duration(seconds: newpos.round()));
                                setState(() {
                                  _videoposition = newpos < 0 ? Duration(seconds: 0) : Duration(seconds: newpos.round());
                                });
                              },
                              icon: Icon(
                                Icons.fast_rewind_outlined,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                if(_controllerYT!.value.isPlaying)
                                {
                                  _controllerYT!.pause();
                                  setState(() {});
                                }
                                else
                                {
                                  _controllerYT!.play();
                                  setState(() {});
                                }
                              },
                              icon: Icon(
                                _controllerYT!.value.isPlaying ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                double newpos = _videoposition.inSeconds + 5.0;
                                _controllerYT!.seekTo(Duration(seconds: newpos.round()));
                                setState(() {
                                  _videoposition = Duration(seconds: newpos.round()) > _controllerYT!.value.metaData.duration ? _controllerYT!.value.metaData.duration : Duration(seconds: newpos.round());
                                });
                              },
                              icon: Icon(
                                Icons.fast_forward_outlined,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if(!_youtubeismute)
                                    {
                                      _controllerYT!.mute();
                                      setState(() {
                                        _youtubeismute = true;
                                      });
                                    }
                                    else
                                    {
                                      _controllerYT!.unMute();
                                      setState(() {
                                        _youtubeismute = false;
                                      });
                                    }
                                  },
                                  icon: Icon(
                                    !_youtubeismute ? Icons.volume_off_outlined : Icons.volume_up_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  child: SliderTheme(
                                    child: Slider(
                                      inactiveColor: Colors.white.withOpacity(0.5),
                                      activeColor: Colors.white,
                                      value: _controllerYT!.value.volume.toDouble(),
                                      min: 0.0,
                                      max: 100,
                                      onChanged: (value) {
                                        _controllerYT!.setVolume(value.round());
                                      },
                                    ),
                                    data: SliderTheme.of(context).copyWith(
                                      thumbShape: SliderComponentShape.noThumb,
                                      trackShape: RectangularSliderTrackShape(),
                                      overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                _controllerYT!.toggleFullScreenMode();
                              },
                              icon: Icon(
                                Icons.crop_free_outlined,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                  else if(((access_code_day == null || access_code_day == '' || codeOk) && (msg_introduction == null || skip_intro)) && type_day == 'Video' && _videoPlayerController != null) Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(20.0),
                    color: AppColors.primaryColor,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              _videoposition.inMinutes.toString() + ':' + (_videoposition.inSeconds % 60).toString().padLeft(2, '0'),
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Slider(
                              inactiveColor: Colors.white.withOpacity(0.5),
                              activeColor: Colors.white,
                              thumbColor: Colors.white,
                              value: _videoPlayerController!.value.duration.inMilliseconds > 0 ? (_videoposition.inMilliseconds / _videoPlayerController!.value.duration.inMilliseconds) * 100 : 0,
                              min: 0.0,
                              max: 100.0,
                              onChanged: (value) {
                                double new_position = (value * _videoPlayerController!.value.duration.inMilliseconds) / 100;
                                _videoPlayerController!.seekTo(Duration(milliseconds: new_position.round()));
                                setState(() {
                                  _videoposition = Duration(milliseconds: new_position.round());
                                });
                              },
                            ),
                            Text(
                              _videoPlayerController!.value.duration.inMinutes.toString() + ':' + (_videoPlayerController!.value.duration.inSeconds % 60).toString().padLeft(2, '0'),
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                double newpos = _videoposition.inSeconds - 5.0;
                                _videoPlayerController!.seekTo(Duration(seconds: newpos.round()));
                                setState(() {
                                  _videoposition = newpos < 0 ? Duration(seconds: 0) : Duration(seconds: newpos.round());
                                });
                              },
                              icon: Icon(
                                Icons.fast_rewind_outlined,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                if(_videoPlayerController!.value.isPlaying)
                                {
                                  _videoPlayerController!.pause();
                                  setState(() {});
                                }
                                else
                                {
                                  _videoPlayerController!.play();
                                  setState(() {});
                                }
                              },
                              icon: Icon(
                                _videoPlayerController!.value.isPlaying ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                double newpos = _videoposition.inSeconds + 5.0;
                                _videoPlayerController!.seekTo(Duration(seconds: newpos.round()));
                                setState(() {
                                  _videoposition = Duration(seconds: newpos.round()) > _videoPlayerController!.value.duration ? _videoPlayerController!.value.duration : Duration(seconds: newpos.round());
                                });
                              },
                              icon: Icon(
                                Icons.fast_forward_outlined,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                if(_videoPlayerController!.value.volume > 0)
                                {
                                  _videoPlayerController!.setVolume(0.0);
                                }
                                else
                                {
                                  _videoPlayerController!.setVolume(1.0);
                                }
                              },
                              icon: Icon(
                                _videoPlayerController!.value.volume > 0 ? Icons.volume_off_outlined : Icons.volume_up_outlined,
                                color: Colors.white,
                              ),
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              child: SliderTheme(
                                child: Slider(
                                  inactiveColor: Colors.white.withOpacity(0.5),
                                  activeColor: Colors.white,
                                  value: _videoPlayerController!.value.volume,
                                  min: 0.0,
                                  max: 1.0,
                                  onChanged: (value) {
                                    _videoPlayerController!.setVolume(value);
                                  },
                                ),
                                data: SliderTheme.of(context).copyWith(
                                  thumbShape: SliderComponentShape.noThumb,
                                  trackShape: RectangularSliderTrackShape(),
                                  overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          },
        );
      },
      );
    }
    else
    {
      return Scaffold(
        appBar: TopBar(),
        body: Center(
          child: Text(
            context.translate().no_internet_connection,
            style: AppStyles.defaultTextStyle.merge(
              TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ),
      );
    }
  }

  Widget _display_day_content(init_type_day, cal_id, txt, picture, video, type_vid, audio, type_aud, pdf) {
    switch (init_type_day) {
      case 'Texte':
        return Container(
          width: double.infinity,
          padding: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
            color: AppColors.mauve.withOpacity(0.1),
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Text(
            txt != null ? txt : '',
            style: AppStyles.defaultTextStyle.merge(
              TextStyle(
                color: AppColors.primaryColor,
              ),
            ),
          ),
        );
      case 'Picture':
        return Stack(
          children: [
            const Center(
              child: Padding(
                padding: EdgeInsets.only(top: 30.0),
                child: LoadingWidget(),
              ),
            ),
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: InteractiveViewer(
                  minScale: 0.1,
                  maxScale: 2.5,
                  child: FadeInImage.memoryNetwork(
                      width: double.infinity,
                      fit: BoxFit.cover,
                      placeholder: kTransparentImage,
                      image: _base_url + ApiConstants.getFile(widget.calId, picture),
                    ),
                ),
              ),
            ),
          ],
        );
      case 'Video':
        switch(type_vid){
          case 'File':
          {
            if(_videoPlayerController != null /*&& _customVideoPlayerController != null*/)
            {
              return AspectRatio(
                aspectRatio: _videoPlayerController!.value.aspectRatio,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: VideoPlayer(
                    _videoPlayerController!,
                  ),
                ),
              );
            }
            return SizedBox();
          }
          case 'Youtube':
          {
            if(_controllerYT != null)
            {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: YoutubePlayer(
                      controller: _controllerYT!,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          context.translate().form_title + ' : ' + (youtube_title != null ? youtube_title! : context.translate().unknown_title),
                          textAlign: TextAlign.left,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 8,),
                        Text(
                          context.translate().channel + ' : ' + (youtube_channel != null ? youtube_channel! : context.translate().unknown_channel),
                          textAlign: TextAlign.left,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 12.0,
                            ),
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
            else
            {
              return SizedBox();
            }
          }
          default:
          {
            if(_videoPlayerController != null /*&& _customVideoPlayerController != null*/)
            {
              return AspectRatio(
                aspectRatio: _videoPlayerController!.value.aspectRatio,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: VideoPlayer(
                    _videoPlayerController!,
                  ),
                ),
              );
            }
            return SizedBox();
          }
        }
      case "Sound":
        switch(type_aud){
          case 'File':
          {
            return Container(
              width: double.infinity,
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                color: AppColors.primaryColor,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        Duration(milliseconds:currentpos).inMinutes.toString() + ':' + (Duration(milliseconds:currentpos).inSeconds % 60).toString().padLeft(2, '0'),
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Slider(
                        inactiveColor: Colors.white.withOpacity(0.5),
                        activeColor: Colors.white,
                        thumbColor: Colors.white,
                        value: currentpos.toDouble(),
                        min: 0.0,
                        max: maxduration.toDouble(),
                        onChanged: (value) {
                          player.seek(Duration(milliseconds: value.round()));
                          setState(() {
                            currentpos = value.round();
                          });
                        },
                      ),
                      Text(
                        Duration(milliseconds:maxduration).inMinutes.toString() + ':' + (Duration(milliseconds:maxduration).inSeconds % 60).toString().padLeft(2, '0'),
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        iconSize: 30,
                        onPressed: () {
                          double newpos = Duration(milliseconds:currentpos).inSeconds - 5.0;
                          player.seek(Duration(seconds: newpos.round()));
                          setState(() {
                            currentpos = newpos < 0 ? 0 : newpos.round();
                          });
                        },
                        icon: Icon(
                          Icons.fast_rewind_outlined,
                          color: Colors.white,
                        ),
                      ),
                      IconButton(
                        iconSize: 30,
                        onPressed: () {
                          if(player.state == AudioPlay.PlayerState.paused)
                          {
                            player.resume();
                            setState(() {});
                          }
                          else if(player.state == AudioPlay.PlayerState.playing)
                          {
                            player.pause();
                            setState(() {});
                          }
                          else
                          {
                            player.play(new AudioPlay.UrlSource(_base_url + ApiConstants.getFile(widget.calId, audio)));
                            setState(() {});
                          }
                        },
                        icon: Icon(
                          player.state == AudioPlay.PlayerState.playing ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                          color: Colors.white,
                        ),
                      ),
                      IconButton(
                        iconSize: 30,
                        onPressed: () {
                          double newpos = Duration(milliseconds:currentpos).inSeconds + 5.0;
                          player.seek(Duration(seconds: newpos.round()));
                          setState(() {
                            currentpos = newpos.round() > Duration(milliseconds:maxduration).inSeconds ? maxduration : newpos.round();
                          });
                        },
                        icon: Icon(
                          Icons.fast_forward_outlined,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }
          case 'Spotify':
          {
            if(audio != null)
            {
              return Container(
                width: double.infinity,
                padding: EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  color: AppColors.primaryColor,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if(init_track != null && init_track!.image != null) ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        init_track!.image!,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ),
                    if(init_track != null && init_track!.image != null) SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                init_track != null && init_track!.title != null ? init_track!.title! : '',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Text(
                                (init_track != null && init_track!.album != null ? init_track!.album! : '')
                                    + (init_track != null && init_track!.album != null && init_track!.artists != null && init_track!.artists!.length > 0 ? ' - ' : '')
                                    + (init_track != null && init_track!.artists != null && init_track!.artists!.length > 0 ? init_track!.artists!.join(', ') : ''),
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    fontSize: 12,
                                    color: Colors.white.withOpacity(0.6),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            iconSize: 30,
                            onPressed: () async {
                              if(init_track != null && init_track!.spotify_uri != null)
                              {
                                String final_uri = init_track!.spotify_uri!.replaceFirst('http', 'https');
                                final Uri url = Uri.parse(final_uri);
                                // Check if Spotify is installed
                                if(await canLaunchUrl(url)) {
                                  // Launch the url which will open Spotify
                                  launchUrl(url);
                                }
                              }
                            },
                            icon: Icon(
                              Icons.play_arrow_outlined,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }
            else
            {
              return Text(
                context.translate().get_day_error,
                style: AppStyles.defaultTextStyle.merge(
                  TextStyle(
                    color: Colors.black,
                  ),
                ),
              );
            }
          }
          default:
          {
            return Container(
              width: double.infinity,
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                color: AppColors.primaryColor,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        Duration(milliseconds:currentpos).inMinutes.toString() + ':' + (Duration(milliseconds:currentpos).inSeconds % 60).toString().padLeft(2, '0'),
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Slider(
                        inactiveColor: Colors.white.withOpacity(0.5),
                        activeColor: Colors.white,
                        thumbColor: Colors.white,
                        value: currentpos.toDouble(),
                        min: 0.0,
                        max: maxduration.toDouble(),
                        onChanged: (value) {
                          player.seek(Duration(milliseconds: value.round()));
                          setState(() {
                            currentpos = value.round();
                          });
                        },
                      ),
                      Text(
                        Duration(milliseconds:maxduration).inMinutes.toString() + ':' + (Duration(milliseconds:maxduration).inSeconds % 60).toString().padLeft(2, '0'),
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        iconSize: 30,
                        onPressed: () {
                          double newpos = Duration(milliseconds:currentpos).inSeconds - 5.0;
                          player.seek(Duration(seconds: newpos.round()));
                          setState(() {
                            currentpos = newpos < 0 ? 0 : newpos.round();
                          });
                        },
                        icon: Icon(
                          Icons.fast_rewind_outlined,
                          color: Colors.white,
                        ),
                      ),
                      IconButton(
                        iconSize: 30,
                        onPressed: () {
                          if(player.state == AudioPlay.PlayerState.paused)
                          {
                            player.resume();
                            setState(() {});
                          }
                          else if(player.state == AudioPlay.PlayerState.playing)
                          {
                            player.pause();
                            setState(() {});
                          }
                          else
                          {
                            player.play(new AudioPlay.UrlSource(_base_url + ApiConstants.getFile(widget.calId, audio)));
                            setState(() {});
                          }
                        },
                        icon: Icon(
                          player.state == AudioPlay.PlayerState.playing ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                          color: Colors.white,
                        ),
                      ),
                      IconButton(
                        iconSize: 30,
                        onPressed: () {
                          double newpos = Duration(milliseconds:currentpos).inSeconds + 5.0;
                          player.seek(Duration(seconds: newpos.round()));
                          setState(() {
                            currentpos = newpos.round() > Duration(milliseconds:maxduration).inSeconds ? maxduration : newpos.round();
                          });
                        },
                        icon: Icon(
                          Icons.fast_forward_outlined,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }
        }
      case "File":
        return ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              createRoute(
                PDFReaderFile(
                  pdfFile: _base_url + ApiConstants.getFile(widget.calId, pdf),
                  is_picked: false,
                ),
              ),
            );
          },
          child: Text(
            context.translate().consult,
            style: AppStyles.defaultTitleStyle.merge(
              TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.primaryColor,
            foregroundColor: Colors.white,
            minimumSize: Size.fromHeight(60),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        );
      default:
        return Container(
          padding: EdgeInsets.all(20.0),
          width: double.infinity,
          decoration: BoxDecoration(
            color: AppColors.mauve.withOpacity(0.1),
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Text(
            txt != null ? txt : '',
            style: AppStyles.defaultTextStyle.merge(
              TextStyle(
                color: AppColors.primaryColor,
              ),
            ),
          ),
        );
    }
  }

  Container _codeContent(String code){
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ...[
            Text(
              context.translate().enter_code,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: TextField(
                controller: _controllerTextCode,
                scrollPadding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                ),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                style: AppStyles.inputText.merge(
                  TextStyle(
                    fontSize: 14,
                  ),
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(15.0),
                  filled: true,
                  fillColor: Colors.white,
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                cursorColor: Colors.black,
                cursorHeight: 25.0,
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.primaryColor,
                foregroundColor: Colors.white,
                minimumSize: Size.fromHeight(60),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }

                for(final singleCode in code.split(";")){
                  if(singleCode.trim().toLowerCase() == _controllerTextCode.text.trim().toLowerCase()){
                    setState((){
                      codeOk = true;
                    });
                    break;
                  }
                }

                if(codeOk != true)
                {
                  _controllerTextCode.clear();

                  SnackBar snackBar = SnackBar(
                    content: Text(
                      context.translate().incorrect_code,
                      style: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    backgroundColor: AppColors.blazeOrange,
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              },
              child: Text(
                context.translate().validate,
                style: AppStyles.defaultTitleStyle.merge(
                  TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ].expand(
                (widget) =>
            [
              widget,
              const SizedBox(
                height: 20,
              )
            ],
          )
        ],
      ),
    );
  }

  Container _intro_msgContent(String intro_msg){
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            intro_msg,
            style: AppStyles.defaultTextStyle.merge(
              TextStyle(
                color: Color(0xFF999999),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.primaryColor,
                foregroundColor: Colors.white,
                minimumSize: Size.fromHeight(60),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                this.setState(() {
                  skip_intro = true;
                });
              },
              child: Text(
                context.translate().ok,
                style: AppStyles.defaultTitleStyle.merge(
                  TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
