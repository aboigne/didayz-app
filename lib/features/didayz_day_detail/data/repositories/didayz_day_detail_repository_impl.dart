import 'dart:async';
import 'package:didayz/features/didayz/data/models/didayz_day_model.dart';

import '../../../../core/domain/entities/result.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../../didayz/domain/entities/didayz_day.dart';
import '../../domain/repositories/didayz_day_detail_repository.dart';
import '../datasources/didayz_detail_remote_data_source.dart';

class DidayzDayDetailRepositoryImpl implements DidayzDayDetailRepository {
  DidayzDayDetailRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  final DidayzDayDetailRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  @override
  Future<Result<DidayzDayEntity>> getDidayzDayDetail(int cal_id, int id) async {
    if (!(await networkInfo.isConnected)) {
      return const Result.failure(Failure.offline());
    }

    try {
      final result = await remoteDataSource.getDidayzDayDetail(cal_id, id);
      final didayzDayDetail = DidayzDayModel.fromJson(result);
      return Result.success(didayzDayDetail.toDomain());
    } on ServerException {
      return const Result.failure(Failure.server());
    }
  }
}
