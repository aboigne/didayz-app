import '../../../../core/domain/entities/result.dart';
import '../../../didayz/domain/entities/didayz_day.dart';

abstract class DidayzDayDetailRepository {
  Future<Result<DidayzDayEntity>> getDidayzDayDetail(int cal_id, int id);
}
