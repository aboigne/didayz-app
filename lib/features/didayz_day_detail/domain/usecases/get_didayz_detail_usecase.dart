import 'package:didayz/features/didayz/domain/entities/didayz_day.dart';

import '../../../../../core/domain/entities/result.dart';
import '../../../../../core/domain/usecases/usecase.dart';
import '../repositories/didayz_day_detail_repository.dart';

class GetDidayzDayDetailUseCase extends UseCase<DidayzDayEntity, GetDidayzDayDetailUseCaseParams> {
  GetDidayzDayDetailUseCase(this.repository);

  final DidayzDayDetailRepository repository;

  @override
  Future<Result<DidayzDayEntity>> call(GetDidayzDayDetailUseCaseParams params) async {
    return repository.getDidayzDayDetail(params.cal_id, params.id);
  }
}

class GetDidayzDayDetailUseCaseParams {
  const GetDidayzDayDetailUseCaseParams({
    required this.cal_id,
    required this.id,
  });

  final int cal_id;
  final int id;
}
