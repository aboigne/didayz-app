import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

import '../../../core/constants/api_constants.dart';

class DidayzDayDetailService {
  final String domain = GlobalConfiguration().getValue('api_base_url');

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Future<int> change_intro_msg(int cal_id, int day_id, String? intro_msg) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'msgIntro': intro_msg});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_intro_msg),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_code(int cal_id, int day_id, String? code) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'code': code});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_code),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_send_rep(int cal_id, int day_id, bool send_rep) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'sendRep': send_rep});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_send_rep),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_type(int cal_id, int day_id, String type) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'type': type});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_type),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_texte(int cal_id, int day_id, String? texte) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'texte': texte});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_texte),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_pic(int cal_id, int day_id, String? picture, int crt_file_size) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'picture': picture, 'crtFileSize': crt_file_size});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_day_pic),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_video_type(int cal_id, int day_id, String? video_type) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'videotype': video_type});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_video_type),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_vid(int cal_id, int day_id, String? video, String? video_type, int crt_file_size) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'video': video, 'video_type': video_type, 'crtFileSize': crt_file_size});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_day_vid),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_audio_type(int cal_id, int day_id, String? audio_type) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );

      if(key_encrypted == null)
      {
        return 500;
      }

      final encryptionKey = base64Url.decode(
        key_encrypted,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: '');

      if(crtToken != null && crtToken != '')
      {
        final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'audiotype': audio_type});
        final result = await http.post(
          Uri.parse(domain + ApiConstants.change_day_audio_type),
          headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
          body: body,
        );
        error_code = result.statusCode;
      }
      else
      {
        error_code = 500;
      }
    } on Exception catch (_) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_aud(int cal_id, int day_id, String? audio, String? audio_type, int crt_file_size) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'audio': audio, 'audio_type': audio_type, 'crtFileSize': crt_file_size});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_day_aud),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> change_file(int cal_id, int day_id, String? file, int crt_file_size) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': cal_id, 'day_id': day_id, 'file': file, 'crtFileSize': crt_file_size});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_day_file),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> send_response(int cal_id, String date_day, String msg_to_send) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': cal_id, 'day': date_day, 'response': msg_to_send});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.send_reponse_day),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }
}
