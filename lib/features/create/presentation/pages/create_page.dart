import 'dart:io';

import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/didayz.dart';
import 'package:didayz/core/presentation/service/didayz_states.dart';
import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../../core/extension/context.dart';
import '../../../../core/presentation/service/file.dart';
import '../../../../core/resources/images.dart';
import '../../../../core/theme/spacing.dart';
import '../../../didayz_detail/presentation/pages/didayz_detail_page.dart';

class CreatePage extends StatefulWidget {
  const CreatePage({super.key, this.refreshKey = null});
  final refreshKey;

  @override
  State<CreatePage> createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  TextEditingController didayz_date_end = TextEditingController();
  String? didayz_name = null, didayz_key = null, didayz_intro_msg = null;
  int? didayz_duration = null;
  final _formKey = GlobalKey<FormState>();
  final ImagePicker _picker = ImagePicker();
  XFile? pic = null;
  ImageSource? media_selected = null;

  @override
  void initState() {
    didayz_date_end.text = '';
    super.initState();
  }

  //we can upload image from camera or from gallery based on parameter
  Future getImage(ImageSource media) async {
    this.setState(() {
      media_selected = media;
    });
    XFile? selected_file;
    bool error = false;
    if(media == ImageSource.gallery)
    {
      selected_file = await _picker.pickImage(source: ImageSource.gallery);
    }
    else if(media == ImageSource.camera)
    {
      selected_file = await _picker.pickImage(source: ImageSource.camera);
    }

    if(selected_file != null && !error)
    {
      setState(() {
        pic = selected_file;
      });
    }
  }

  void alert_image() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.all(20.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    context.translate().media_to_use,
                    style: AppStyles.defaultTitleStyle.merge(
                      TextStyle(
                        color: AppColors.primaryColor,
                      ),
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () async {
                      Navigator.pop(context);
                      getImage(ImageSource.gallery);
                    },
                    child: Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.primaryColor,
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.image,
                            color: Colors.white,
                          ),
                          AppGap.regular(),
                          Text(
                            context.translate().from_gallery,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      Navigator.pop(context);
                      getImage(ImageSource.camera);
                    },
                    child: Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.primaryColor,
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.camera,
                            color: Colors.white,
                          ),
                          AppGap.regular(),
                          Text(
                            context.translate().from_camera,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
    );
  }

  void remove_image() {
    setState(() {
      pic = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: Text(
          context.translate().new_didayz,
          style: AppStyles.defaultTitleStyle.merge(
            TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Scrollbar(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            child: Padding(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: Column(
                children: [
                  ...[
                    Container(
                      width: 150,
                      height: 150,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:BorderRadius.circular(100),
                              ),
                              child: SizedBox(
                                height: 18,
                                width: 18,
                                child: IconButton(
                                  padding: new EdgeInsets.all(0.0),
                                  icon: const Icon(
                                    Icons.edit,
                                    color: Colors.black,
                                    size: 18,
                                  ),
                                  onPressed: alert_image,
                                ),
                              ),
                            ),
                            if(pic != null) Container(
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:BorderRadius.circular(100),
                              ),
                              child: SizedBox(
                                height: 18,
                                width: 18,
                                child: IconButton(
                                  padding: new EdgeInsets.all(0.0),
                                  icon: const Icon(
                                    Icons.remove_outlined,
                                    color: Colors.black,
                                    size: 18,
                                  ),
                                  onPressed: remove_image,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                          image: pic != null ?
                          Image.file(
                            File(pic!.path),
                            width: 200,
                            height: 200,
                          ).image :
                          Image.asset(
                            Images.didayz_blue,
                            key: UniqueKey(),
                            cacheWidth: 400,
                          ).image,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().form_title,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        hintText: context.translate().form_title,
                        hintStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Color(0xFF999999),
                          ),
                        ),
                        errorStyle: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Color(0xffffaea6),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      cursorColor: Colors.black,
                      cursorHeight: 25.0,
                      obscureText: false,
                      onChanged: (value) {
                        this.setState(() {
                          didayz_name = value;
                        });
                      },
                      validator: (value){
                        if(value == null || value.isEmpty || value == '')
                        {
                          return context.translate().field_required;
                        }
                        else if(value.length > 50)
                        {
                          return context.translate().form_error_50char;
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      textInputAction: TextInputAction.newline,
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().form_introductory_message_not_mandatory,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        helperText: context.translate().form_introductory_message_info,
                        helperStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Colors.black38,
                            fontSize: 12,
                          ),
                        ),
                        helperMaxLines: 3,
                        hintText: context.translate().form_introductory_message,
                        hintStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Color(0xFF999999),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      cursorColor: Colors.black,
                      cursorHeight: 25.0,
                      obscureText: false,
                      onChanged: (value) {
                        this.setState(() {
                          didayz_intro_msg = value;
                        });
                      },
                    ),
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      controller: didayz_date_end,
                      readOnly: true,
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().form_end_day,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        helperText: context.translate().form_end_day_info,
                        helperStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Colors.black38,
                            fontSize: 12,
                          ),
                        ),
                        helperMaxLines: 3,
                        hintText: context.translate().form_end_day,
                        hintStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Color(0xFF999999),
                          ),
                        ),
                        errorStyle: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Color(0xffffaea6),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      validator: (value){
                        if(value == null || value.isEmpty || value == '')
                        {
                          return context.translate().field_required;
                        }
                        return null;
                      },
                      onTap: () async {
                        final date_end_format = !didayz_date_end.text.isEmpty ? didayz_date_end.text.split('/')[2] + '-' + didayz_date_end.text.split('/')[1] + '-' + didayz_date_end.text.split('/')[0] : '';
                        final DateTime? pickedDate = await showDatePicker(
                          context: context,
                          builder: (context, child) {
                            return Theme(
                              data: Theme.of(context).copyWith(
                                colorScheme: ColorScheme.light(
                                  primary: AppColors.primaryColor, // header background color
                                  onPrimary: Colors.black, // header text color
                                  onSurface: AppColors.primaryColor, // body text color
                                ),
                                textButtonTheme: TextButtonThemeData(
                                  style: TextButton.styleFrom(
                                    foregroundColor: AppColors.primaryColor, // button text color
                                  ),
                                ),
                              ),
                              child: child!,
                            );
                          },
                          initialDate: didayz_date_end.text.isEmpty ? DateTime.now() : DateTime.parse(date_end_format),
                          firstDate: DateTime.now(),
                          lastDate: DateTime.now().add(const Duration(days: 3650)),
                        );

                        if (pickedDate != null) {
                          final String formattedDate = DateFormat('dd/MM/yyyy').format(pickedDate);
                          setState(() {
                            didayz_date_end.text = formattedDate; //set output date to TextField value.
                          });
                        }
                      },
                    ),
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().form_duration,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        hintText: context.translate().form_duration_bis,
                        hintStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Color(0xFF999999),
                          ),
                        ),
                        helperText: context.translate().form_duration_info,
                        helperStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Colors.black38,
                            fontSize: 12,
                          ),
                        ),
                        helperMaxLines: 3,
                        errorStyle: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Color(0xffffaea6),
                          ),
                        ),
                        errorMaxLines: 3,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      cursorColor: Colors.black,
                      cursorHeight: 25.0,
                      obscureText: false,
                      onChanged: (value) {
                        this.setState(() {
                          didayz_duration = int.parse(value);
                        });
                      },
                      validator: (value){
                        if(value == null || value.isEmpty || value == '')
                        {
                          return context.translate().field_required;
                        }
                        else if(int.parse(value) <= 0 || int.parse(value) > 31)
                        {
                          return context.translate().form_error_duration;
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      scrollPadding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                      ),
                      textInputAction: TextInputAction.next,
                      style: AppStyles.inputText.merge(
                        TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        filled: true,
                        fillColor: Colors.white,
                        alignLabelWithHint: true,
                        labelText: context.translate().form_key,
                        labelStyle: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        hintText: context.translate().form_key_bis,
                        hintStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Color(0xFF999999),
                          ),
                        ),
                        helperText: context.translate().form_key_info,
                        helperStyle: AppStyles.inputText.merge(
                          TextStyle(
                            color: Colors.black38,
                            fontSize: 12,
                          ),
                        ),
                        helperMaxLines: 4,
                        errorStyle: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Color(0xffffaea6),
                          ),
                        ),
                        errorMaxLines: 3,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      cursorColor: Colors.black,
                      cursorHeight: 25.0,
                      obscureText: false,
                      onChanged: (value) {
                        this.setState(() {
                          didayz_key = value;
                        });
                      },
                      validator: (value){
                        final validCharacters = RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$');
                        if(value == null || value.isEmpty || value == '')
                        {
                          return context.translate().field_required;
                        }
                        else if(!validCharacters.hasMatch(value))
                        {
                          return context.translate().form_key_error_format;
                        }
                        else if(value.length > 20)
                        {
                          return context.translate().form_error_20char;
                        }
                        return null;
                      },
                    ),
                    GestureDetector(
                      onTap: () async {
                        final FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }

                        if(_formKey.currentState!.validate())
                        {
                          final network_inf = new NetworkInfoImpl();
                          if(!(await network_inf.isConnected))
                          {
                            final snackBar = SnackBar(
                              content: Text(
                                context.translate().no_internet_connection,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          }
                          else
                          {
                            final date_end_format = !didayz_date_end.text.isEmpty ? didayz_date_end.text.split('/')[2] + '-' + didayz_date_end.text.split('/')[1] + '-' + didayz_date_end.text.split('/')[0] : '';
                            final result_didayz_created = await DidayzService().create_didayz(didayz_name!, didayz_key!, date_end_format, didayz_duration!, didayz_intro_msg, pic != null ? media_selected == ImageSource.gallery ? pic!.name : 'from_camera.jpeg' : null);
                            late SnackBar snackBar;
                            bool redirect_to_cal = false;
                            switch (result_didayz_created['statusCode']) {
                              case 200:
                                if(pic != null)
                                {
                                  final result_upload_file = await FileService().upload_file(pic!, '_didayz_pic', result_didayz_created['cal_id']!, media_selected == ImageSource.gallery ? '0' : '1');
                                  switch (result_upload_file) {
                                    case 200:
                                      snackBar = SnackBar(
                                        content: Text(
                                          context.translate().didayz_creation_success,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.primaryColor,
                                      );
                                      redirect_to_cal = true;
                                      break;
                                    default:
                                      snackBar = SnackBar(
                                        content: Text(
                                          context.translate().didayz_creation_error,
                                          style: AppStyles.defaultTextStyle.merge(
                                            TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        backgroundColor: AppColors.blazeOrange,
                                      );
                                  }
                                }
                                else
                                {
                                  snackBar = SnackBar(
                                    content: Text(
                                      context.translate().didayz_creation_success,
                                      style: AppStyles.defaultTextStyle.merge(
                                        TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    backgroundColor: AppColors.primaryColor,
                                  );
                                  redirect_to_cal = true;
                                }
                                break;
                              case 400:
                                snackBar = SnackBar(
                                  content: Text(
                                    context.translate().didayz_creation_error_key,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                                break;
                              default:
                                snackBar = SnackBar(
                                  content: Text(
                                    context.translate().didayz_creation_error,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                            }
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);

                            if(redirect_to_cal)
                            {
                              await Navigator.of(context).push(createRoute(DidayzDetailPage(result_didayz_created['cal_id']!, States_Didayz.draft)));
                              if(widget.refreshKey != null) {
                                widget.refreshKey!.currentState!.show();
                              }
                              Navigator.of(context).pop();
                            }
                          }
                        }
                      },
                      child: Container(
                        margin: const EdgeInsets.only(top: 20.0),
                        height: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: AppColors.primaryColor,
                          ),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Text(
                            context.translate().create,
                            style: AppStyles.defaultTitleStyle.merge(
                              TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ].expand(
                        (widget) =>
                    [
                      widget,
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
