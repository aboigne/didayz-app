import '../../../../core/domain/entities/result.dart';
import '../entities/page_of_didayz.dart';

abstract class DidayzRepository {
  Future<Result<PageOfDidayz>> getDidayzByCreatorByPage(int page, {int limit, int is_publish});
  Future<Result<PageOfDidayz>> getDidayzByPlayerByPage(int page, {int limit});
}
