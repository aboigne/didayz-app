// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserEntity _$UserEntityFromJson(Map<String, dynamic> json) => UserEntity(
      id: json['id'] as int,
      Name: json['Name'] as String,
      FirstName: json['FirstName'] as String,
      Email: json['Email'] as String,
    );

Map<String, dynamic> _$UserEntityToJson(UserEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'Name': instance.Name,
      'FirstName': instance.FirstName,
      'Email': instance.Email,
    };
