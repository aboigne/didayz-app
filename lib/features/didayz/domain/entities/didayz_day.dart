import 'package:freezed_annotation/freezed_annotation.dart';

part 'didayz_day.freezed.dart';

@freezed
class DidayzDayEntity with _$DidayzDayEntity {
  const factory DidayzDayEntity({
    required int id,
    required int calendarId,
    required int Number,
    required int Month,
    required int Year,
    required String Type,
    String? Texte,
    String? Picture,
    String? VideoType,
    String? Video,
    String? SoundType,
    String? Sound,
    String? File,
    String? Code,
    String? IntroMessage,
    required bool SendResponse,
  }) = _DidayzDayEntity;
}
