// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'page_of_didayz.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PageOfDidayz {
  int get page => throw _privateConstructorUsedError;
  List<DidayzEntity> get results => throw _privateConstructorUsedError;
  int get totalPages => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PageOfDidayzCopyWith<PageOfDidayz> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PageOfDidayzCopyWith<$Res> {
  factory $PageOfDidayzCopyWith(
          PageOfDidayz value, $Res Function(PageOfDidayz) then) =
      _$PageOfDidayzCopyWithImpl<$Res, PageOfDidayz>;
  @useResult
  $Res call({int page, List<DidayzEntity> results, int totalPages});
}

/// @nodoc
class _$PageOfDidayzCopyWithImpl<$Res, $Val extends PageOfDidayz>
    implements $PageOfDidayzCopyWith<$Res> {
  _$PageOfDidayzCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? results = null,
    Object? totalPages = null,
  }) {
    return _then(_value.copyWith(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<DidayzEntity>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PageOfDidayzCopyWith<$Res>
    implements $PageOfDidayzCopyWith<$Res> {
  factory _$$_PageOfDidayzCopyWith(
          _$_PageOfDidayz value, $Res Function(_$_PageOfDidayz) then) =
      __$$_PageOfDidayzCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int page, List<DidayzEntity> results, int totalPages});
}

/// @nodoc
class __$$_PageOfDidayzCopyWithImpl<$Res>
    extends _$PageOfDidayzCopyWithImpl<$Res, _$_PageOfDidayz>
    implements _$$_PageOfDidayzCopyWith<$Res> {
  __$$_PageOfDidayzCopyWithImpl(
      _$_PageOfDidayz _value, $Res Function(_$_PageOfDidayz) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? results = null,
    Object? totalPages = null,
  }) {
    return _then(_$_PageOfDidayz(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<DidayzEntity>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_PageOfDidayz implements _PageOfDidayz {
  const _$_PageOfDidayz(
      {required this.page,
      required final List<DidayzEntity> results,
      required this.totalPages})
      : _results = results;

  @override
  final int page;
  final List<DidayzEntity> _results;
  @override
  List<DidayzEntity> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  final int totalPages;

  @override
  String toString() {
    return 'PageOfDidayz(page: $page, results: $results, totalPages: $totalPages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PageOfDidayz &&
            (identical(other.page, page) || other.page == page) &&
            const DeepCollectionEquality().equals(other._results, _results) &&
            (identical(other.totalPages, totalPages) ||
                other.totalPages == totalPages));
  }

  @override
  int get hashCode => Object.hash(runtimeType, page,
      const DeepCollectionEquality().hash(_results), totalPages);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PageOfDidayzCopyWith<_$_PageOfDidayz> get copyWith =>
      __$$_PageOfDidayzCopyWithImpl<_$_PageOfDidayz>(this, _$identity);
}

abstract class _PageOfDidayz implements PageOfDidayz {
  const factory _PageOfDidayz(
      {required final int page,
      required final List<DidayzEntity> results,
      required final int totalPages}) = _$_PageOfDidayz;

  @override
  int get page;
  @override
  List<DidayzEntity> get results;
  @override
  int get totalPages;
  @override
  @JsonKey(ignore: true)
  _$$_PageOfDidayzCopyWith<_$_PageOfDidayz> get copyWith =>
      throw _privateConstructorUsedError;
}
