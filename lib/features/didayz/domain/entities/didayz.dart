import 'package:freezed_annotation/freezed_annotation.dart';

import 'user.dart';

part 'didayz.freezed.dart';

@freezed
class DidayzEntity with _$DidayzEntity {
  const factory DidayzEntity({
    required int id,
    required String Name,
    required String Key,
    String? IntroMsg,
    String? Background,
    required DateTime DateEnd,
    required int NBDays,
    double? Progress,
    required bool IsPublished,
    required UserEntity Creator
  }) = _DidayzEntity;
}

