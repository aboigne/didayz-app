import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../features/didayz/domain/entities/didayz.dart';

part 'page_of_didayz.freezed.dart';

@freezed
class PageOfDidayz with _$PageOfDidayz {
  const factory PageOfDidayz({
    required int page,
    required List<DidayzEntity> results,
    required int totalPages,
  }) = _PageOfDidayz;
}
