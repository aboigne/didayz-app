// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'didayz_day.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DidayzDayEntity {
  int get id => throw _privateConstructorUsedError;
  int get calendarId => throw _privateConstructorUsedError;
  int get Number => throw _privateConstructorUsedError;
  int get Month => throw _privateConstructorUsedError;
  int get Year => throw _privateConstructorUsedError;
  String get Type => throw _privateConstructorUsedError;
  String? get Texte => throw _privateConstructorUsedError;
  String? get Picture => throw _privateConstructorUsedError;
  String? get VideoType => throw _privateConstructorUsedError;
  String? get Video => throw _privateConstructorUsedError;
  String? get SoundType => throw _privateConstructorUsedError;
  String? get Sound => throw _privateConstructorUsedError;
  String? get File => throw _privateConstructorUsedError;
  String? get Code => throw _privateConstructorUsedError;
  String? get IntroMessage => throw _privateConstructorUsedError;
  bool get SendResponse => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DidayzDayEntityCopyWith<DidayzDayEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DidayzDayEntityCopyWith<$Res> {
  factory $DidayzDayEntityCopyWith(
          DidayzDayEntity value, $Res Function(DidayzDayEntity) then) =
      _$DidayzDayEntityCopyWithImpl<$Res, DidayzDayEntity>;
  @useResult
  $Res call(
      {int id,
      int calendarId,
      int Number,
      int Month,
      int Year,
      String Type,
      String? Texte,
      String? Picture,
      String? VideoType,
      String? Video,
      String? SoundType,
      String? Sound,
      String? File,
      String? Code,
      String? IntroMessage,
      bool SendResponse});
}

/// @nodoc
class _$DidayzDayEntityCopyWithImpl<$Res, $Val extends DidayzDayEntity>
    implements $DidayzDayEntityCopyWith<$Res> {
  _$DidayzDayEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? calendarId = null,
    Object? Number = null,
    Object? Month = null,
    Object? Year = null,
    Object? Type = null,
    Object? Texte = freezed,
    Object? Picture = freezed,
    Object? VideoType = freezed,
    Object? Video = freezed,
    Object? SoundType = freezed,
    Object? Sound = freezed,
    Object? File = freezed,
    Object? Code = freezed,
    Object? IntroMessage = freezed,
    Object? SendResponse = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      calendarId: null == calendarId
          ? _value.calendarId
          : calendarId // ignore: cast_nullable_to_non_nullable
              as int,
      Number: null == Number
          ? _value.Number
          : Number // ignore: cast_nullable_to_non_nullable
              as int,
      Month: null == Month
          ? _value.Month
          : Month // ignore: cast_nullable_to_non_nullable
              as int,
      Year: null == Year
          ? _value.Year
          : Year // ignore: cast_nullable_to_non_nullable
              as int,
      Type: null == Type
          ? _value.Type
          : Type // ignore: cast_nullable_to_non_nullable
              as String,
      Texte: freezed == Texte
          ? _value.Texte
          : Texte // ignore: cast_nullable_to_non_nullable
              as String?,
      Picture: freezed == Picture
          ? _value.Picture
          : Picture // ignore: cast_nullable_to_non_nullable
              as String?,
      VideoType: freezed == VideoType
          ? _value.VideoType
          : VideoType // ignore: cast_nullable_to_non_nullable
              as String?,
      Video: freezed == Video
          ? _value.Video
          : Video // ignore: cast_nullable_to_non_nullable
              as String?,
      SoundType: freezed == SoundType
          ? _value.SoundType
          : SoundType // ignore: cast_nullable_to_non_nullable
              as String?,
      Sound: freezed == Sound
          ? _value.Sound
          : Sound // ignore: cast_nullable_to_non_nullable
              as String?,
      File: freezed == File
          ? _value.File
          : File // ignore: cast_nullable_to_non_nullable
              as String?,
      Code: freezed == Code
          ? _value.Code
          : Code // ignore: cast_nullable_to_non_nullable
              as String?,
      IntroMessage: freezed == IntroMessage
          ? _value.IntroMessage
          : IntroMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      SendResponse: null == SendResponse
          ? _value.SendResponse
          : SendResponse // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DidayzDayEntityCopyWith<$Res>
    implements $DidayzDayEntityCopyWith<$Res> {
  factory _$$_DidayzDayEntityCopyWith(
          _$_DidayzDayEntity value, $Res Function(_$_DidayzDayEntity) then) =
      __$$_DidayzDayEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      int calendarId,
      int Number,
      int Month,
      int Year,
      String Type,
      String? Texte,
      String? Picture,
      String? VideoType,
      String? Video,
      String? SoundType,
      String? Sound,
      String? File,
      String? Code,
      String? IntroMessage,
      bool SendResponse});
}

/// @nodoc
class __$$_DidayzDayEntityCopyWithImpl<$Res>
    extends _$DidayzDayEntityCopyWithImpl<$Res, _$_DidayzDayEntity>
    implements _$$_DidayzDayEntityCopyWith<$Res> {
  __$$_DidayzDayEntityCopyWithImpl(
      _$_DidayzDayEntity _value, $Res Function(_$_DidayzDayEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? calendarId = null,
    Object? Number = null,
    Object? Month = null,
    Object? Year = null,
    Object? Type = null,
    Object? Texte = freezed,
    Object? Picture = freezed,
    Object? VideoType = freezed,
    Object? Video = freezed,
    Object? SoundType = freezed,
    Object? Sound = freezed,
    Object? File = freezed,
    Object? Code = freezed,
    Object? IntroMessage = freezed,
    Object? SendResponse = null,
  }) {
    return _then(_$_DidayzDayEntity(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      calendarId: null == calendarId
          ? _value.calendarId
          : calendarId // ignore: cast_nullable_to_non_nullable
              as int,
      Number: null == Number
          ? _value.Number
          : Number // ignore: cast_nullable_to_non_nullable
              as int,
      Month: null == Month
          ? _value.Month
          : Month // ignore: cast_nullable_to_non_nullable
              as int,
      Year: null == Year
          ? _value.Year
          : Year // ignore: cast_nullable_to_non_nullable
              as int,
      Type: null == Type
          ? _value.Type
          : Type // ignore: cast_nullable_to_non_nullable
              as String,
      Texte: freezed == Texte
          ? _value.Texte
          : Texte // ignore: cast_nullable_to_non_nullable
              as String?,
      Picture: freezed == Picture
          ? _value.Picture
          : Picture // ignore: cast_nullable_to_non_nullable
              as String?,
      VideoType: freezed == VideoType
          ? _value.VideoType
          : VideoType // ignore: cast_nullable_to_non_nullable
              as String?,
      Video: freezed == Video
          ? _value.Video
          : Video // ignore: cast_nullable_to_non_nullable
              as String?,
      SoundType: freezed == SoundType
          ? _value.SoundType
          : SoundType // ignore: cast_nullable_to_non_nullable
              as String?,
      Sound: freezed == Sound
          ? _value.Sound
          : Sound // ignore: cast_nullable_to_non_nullable
              as String?,
      File: freezed == File
          ? _value.File
          : File // ignore: cast_nullable_to_non_nullable
              as String?,
      Code: freezed == Code
          ? _value.Code
          : Code // ignore: cast_nullable_to_non_nullable
              as String?,
      IntroMessage: freezed == IntroMessage
          ? _value.IntroMessage
          : IntroMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      SendResponse: null == SendResponse
          ? _value.SendResponse
          : SendResponse // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_DidayzDayEntity implements _DidayzDayEntity {
  const _$_DidayzDayEntity(
      {required this.id,
      required this.calendarId,
      required this.Number,
      required this.Month,
      required this.Year,
      required this.Type,
      this.Texte,
      this.Picture,
      this.VideoType,
      this.Video,
      this.SoundType,
      this.Sound,
      this.File,
      this.Code,
      this.IntroMessage,
      required this.SendResponse});

  @override
  final int id;
  @override
  final int calendarId;
  @override
  final int Number;
  @override
  final int Month;
  @override
  final int Year;
  @override
  final String Type;
  @override
  final String? Texte;
  @override
  final String? Picture;
  @override
  final String? VideoType;
  @override
  final String? Video;
  @override
  final String? SoundType;
  @override
  final String? Sound;
  @override
  final String? File;
  @override
  final String? Code;
  @override
  final String? IntroMessage;
  @override
  final bool SendResponse;

  @override
  String toString() {
    return 'DidayzDayEntity(id: $id, calendarId: $calendarId, Number: $Number, Month: $Month, Year: $Year, Type: $Type, Texte: $Texte, Picture: $Picture, VideoType: $VideoType, Video: $Video, SoundType: $SoundType, Sound: $Sound, File: $File, Code: $Code, IntroMessage: $IntroMessage, SendResponse: $SendResponse)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DidayzDayEntity &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.calendarId, calendarId) ||
                other.calendarId == calendarId) &&
            (identical(other.Number, Number) || other.Number == Number) &&
            (identical(other.Month, Month) || other.Month == Month) &&
            (identical(other.Year, Year) || other.Year == Year) &&
            (identical(other.Type, Type) || other.Type == Type) &&
            (identical(other.Texte, Texte) || other.Texte == Texte) &&
            (identical(other.Picture, Picture) || other.Picture == Picture) &&
            (identical(other.VideoType, VideoType) ||
                other.VideoType == VideoType) &&
            (identical(other.Video, Video) || other.Video == Video) &&
            (identical(other.SoundType, SoundType) ||
                other.SoundType == SoundType) &&
            (identical(other.Sound, Sound) || other.Sound == Sound) &&
            (identical(other.File, File) || other.File == File) &&
            (identical(other.Code, Code) || other.Code == Code) &&
            (identical(other.IntroMessage, IntroMessage) ||
                other.IntroMessage == IntroMessage) &&
            (identical(other.SendResponse, SendResponse) ||
                other.SendResponse == SendResponse));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      calendarId,
      Number,
      Month,
      Year,
      Type,
      Texte,
      Picture,
      VideoType,
      Video,
      SoundType,
      Sound,
      File,
      Code,
      IntroMessage,
      SendResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DidayzDayEntityCopyWith<_$_DidayzDayEntity> get copyWith =>
      __$$_DidayzDayEntityCopyWithImpl<_$_DidayzDayEntity>(this, _$identity);
}

abstract class _DidayzDayEntity implements DidayzDayEntity {
  const factory _DidayzDayEntity(
      {required final int id,
      required final int calendarId,
      required final int Number,
      required final int Month,
      required final int Year,
      required final String Type,
      final String? Texte,
      final String? Picture,
      final String? VideoType,
      final String? Video,
      final String? SoundType,
      final String? Sound,
      final String? File,
      final String? Code,
      final String? IntroMessage,
      required final bool SendResponse}) = _$_DidayzDayEntity;

  @override
  int get id;
  @override
  int get calendarId;
  @override
  int get Number;
  @override
  int get Month;
  @override
  int get Year;
  @override
  String get Type;
  @override
  String? get Texte;
  @override
  String? get Picture;
  @override
  String? get VideoType;
  @override
  String? get Video;
  @override
  String? get SoundType;
  @override
  String? get Sound;
  @override
  String? get File;
  @override
  String? get Code;
  @override
  String? get IntroMessage;
  @override
  bool get SendResponse;
  @override
  @JsonKey(ignore: true)
  _$$_DidayzDayEntityCopyWith<_$_DidayzDayEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
