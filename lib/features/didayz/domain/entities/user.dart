import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
@JsonSerializable()
class UserEntity with _$UserEntity {
  const factory UserEntity({
    required int id,
    required String Name,
    required String FirstName,
    required String Email,
  }) = _UserEntity;

  factory UserEntity.fromJson(Map<String, dynamic> json) {
    return UserEntity(
        id: json['id'],
        Name: json['Name'],
        FirstName: json['FirstName'],
        Email: json['Email']);
  }
}

