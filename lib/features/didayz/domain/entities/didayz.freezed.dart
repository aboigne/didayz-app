// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'didayz.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DidayzEntity {
  int get id => throw _privateConstructorUsedError;
  String get Name => throw _privateConstructorUsedError;
  String get Key => throw _privateConstructorUsedError;
  String? get IntroMsg => throw _privateConstructorUsedError;
  String? get Background => throw _privateConstructorUsedError;
  DateTime get DateEnd => throw _privateConstructorUsedError;
  int get NBDays => throw _privateConstructorUsedError;
  double? get Progress => throw _privateConstructorUsedError;
  bool get IsPublished => throw _privateConstructorUsedError;
  UserEntity get Creator => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DidayzEntityCopyWith<DidayzEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DidayzEntityCopyWith<$Res> {
  factory $DidayzEntityCopyWith(
          DidayzEntity value, $Res Function(DidayzEntity) then) =
      _$DidayzEntityCopyWithImpl<$Res, DidayzEntity>;
  @useResult
  $Res call(
      {int id,
      String Name,
      String Key,
      String? IntroMsg,
      String? Background,
      DateTime DateEnd,
      int NBDays,
      double? Progress,
      bool IsPublished,
      UserEntity Creator});

  $UserEntityCopyWith<$Res> get Creator;
}

/// @nodoc
class _$DidayzEntityCopyWithImpl<$Res, $Val extends DidayzEntity>
    implements $DidayzEntityCopyWith<$Res> {
  _$DidayzEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Name = null,
    Object? Key = null,
    Object? IntroMsg = freezed,
    Object? Background = freezed,
    Object? DateEnd = null,
    Object? NBDays = null,
    Object? Progress = freezed,
    Object? IsPublished = null,
    Object? Creator = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Name: null == Name
          ? _value.Name
          : Name // ignore: cast_nullable_to_non_nullable
              as String,
      Key: null == Key
          ? _value.Key
          : Key // ignore: cast_nullable_to_non_nullable
              as String,
      IntroMsg: freezed == IntroMsg
          ? _value.IntroMsg
          : IntroMsg // ignore: cast_nullable_to_non_nullable
              as String?,
      Background: freezed == Background
          ? _value.Background
          : Background // ignore: cast_nullable_to_non_nullable
              as String?,
      DateEnd: null == DateEnd
          ? _value.DateEnd
          : DateEnd // ignore: cast_nullable_to_non_nullable
              as DateTime,
      NBDays: null == NBDays
          ? _value.NBDays
          : NBDays // ignore: cast_nullable_to_non_nullable
              as int,
      Progress: freezed == Progress
          ? _value.Progress
          : Progress // ignore: cast_nullable_to_non_nullable
              as double?,
      IsPublished: null == IsPublished
          ? _value.IsPublished
          : IsPublished // ignore: cast_nullable_to_non_nullable
              as bool,
      Creator: null == Creator
          ? _value.Creator
          : Creator // ignore: cast_nullable_to_non_nullable
              as UserEntity,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $UserEntityCopyWith<$Res> get Creator {
    return $UserEntityCopyWith<$Res>(_value.Creator, (value) {
      return _then(_value.copyWith(Creator: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_DidayzEntityCopyWith<$Res>
    implements $DidayzEntityCopyWith<$Res> {
  factory _$$_DidayzEntityCopyWith(
          _$_DidayzEntity value, $Res Function(_$_DidayzEntity) then) =
      __$$_DidayzEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String Name,
      String Key,
      String? IntroMsg,
      String? Background,
      DateTime DateEnd,
      int NBDays,
      double? Progress,
      bool IsPublished,
      UserEntity Creator});

  @override
  $UserEntityCopyWith<$Res> get Creator;
}

/// @nodoc
class __$$_DidayzEntityCopyWithImpl<$Res>
    extends _$DidayzEntityCopyWithImpl<$Res, _$_DidayzEntity>
    implements _$$_DidayzEntityCopyWith<$Res> {
  __$$_DidayzEntityCopyWithImpl(
      _$_DidayzEntity _value, $Res Function(_$_DidayzEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Name = null,
    Object? Key = null,
    Object? IntroMsg = freezed,
    Object? Background = freezed,
    Object? DateEnd = null,
    Object? NBDays = null,
    Object? Progress = freezed,
    Object? IsPublished = null,
    Object? Creator = null,
  }) {
    return _then(_$_DidayzEntity(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Name: null == Name
          ? _value.Name
          : Name // ignore: cast_nullable_to_non_nullable
              as String,
      Key: null == Key
          ? _value.Key
          : Key // ignore: cast_nullable_to_non_nullable
              as String,
      IntroMsg: freezed == IntroMsg
          ? _value.IntroMsg
          : IntroMsg // ignore: cast_nullable_to_non_nullable
              as String?,
      Background: freezed == Background
          ? _value.Background
          : Background // ignore: cast_nullable_to_non_nullable
              as String?,
      DateEnd: null == DateEnd
          ? _value.DateEnd
          : DateEnd // ignore: cast_nullable_to_non_nullable
              as DateTime,
      NBDays: null == NBDays
          ? _value.NBDays
          : NBDays // ignore: cast_nullable_to_non_nullable
              as int,
      Progress: freezed == Progress
          ? _value.Progress
          : Progress // ignore: cast_nullable_to_non_nullable
              as double?,
      IsPublished: null == IsPublished
          ? _value.IsPublished
          : IsPublished // ignore: cast_nullable_to_non_nullable
              as bool,
      Creator: null == Creator
          ? _value.Creator
          : Creator // ignore: cast_nullable_to_non_nullable
              as UserEntity,
    ));
  }
}

/// @nodoc

class _$_DidayzEntity implements _DidayzEntity {
  const _$_DidayzEntity(
      {required this.id,
      required this.Name,
      required this.Key,
      this.IntroMsg,
      this.Background,
      required this.DateEnd,
      required this.NBDays,
      this.Progress,
      required this.IsPublished,
      required this.Creator});

  @override
  final int id;
  @override
  final String Name;
  @override
  final String Key;
  @override
  final String? IntroMsg;
  @override
  final String? Background;
  @override
  final DateTime DateEnd;
  @override
  final int NBDays;
  @override
  final double? Progress;
  @override
  final bool IsPublished;
  @override
  final UserEntity Creator;

  @override
  String toString() {
    return 'DidayzEntity(id: $id, Name: $Name, Key: $Key, IntroMsg: $IntroMsg, Background: $Background, DateEnd: $DateEnd, NBDays: $NBDays, Progress: $Progress, IsPublished: $IsPublished, Creator: $Creator)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DidayzEntity &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.Name, Name) || other.Name == Name) &&
            (identical(other.Key, Key) || other.Key == Key) &&
            (identical(other.IntroMsg, IntroMsg) ||
                other.IntroMsg == IntroMsg) &&
            (identical(other.Background, Background) ||
                other.Background == Background) &&
            (identical(other.DateEnd, DateEnd) || other.DateEnd == DateEnd) &&
            (identical(other.NBDays, NBDays) || other.NBDays == NBDays) &&
            (identical(other.Progress, Progress) ||
                other.Progress == Progress) &&
            (identical(other.IsPublished, IsPublished) ||
                other.IsPublished == IsPublished) &&
            (identical(other.Creator, Creator) || other.Creator == Creator));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, Name, Key, IntroMsg,
      Background, DateEnd, NBDays, Progress, IsPublished, Creator);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DidayzEntityCopyWith<_$_DidayzEntity> get copyWith =>
      __$$_DidayzEntityCopyWithImpl<_$_DidayzEntity>(this, _$identity);
}

abstract class _DidayzEntity implements DidayzEntity {
  const factory _DidayzEntity(
      {required final int id,
      required final String Name,
      required final String Key,
      final String? IntroMsg,
      final String? Background,
      required final DateTime DateEnd,
      required final int NBDays,
      final double? Progress,
      required final bool IsPublished,
      required final UserEntity Creator}) = _$_DidayzEntity;

  @override
  int get id;
  @override
  String get Name;
  @override
  String get Key;
  @override
  String? get IntroMsg;
  @override
  String? get Background;
  @override
  DateTime get DateEnd;
  @override
  int get NBDays;
  @override
  double? get Progress;
  @override
  bool get IsPublished;
  @override
  UserEntity get Creator;
  @override
  @JsonKey(ignore: true)
  _$$_DidayzEntityCopyWith<_$_DidayzEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
