import 'package:didayz/features/didayz/domain/entities/page_of_didayz.dart';
import 'package:didayz/features/didayz/domain/repositories/didayz_repository.dart';

import '../../../../../core/domain/entities/result.dart';
import '../../../../../core/domain/usecases/usecase.dart';

class GetDidayzByPlayerByPageUseCase extends UseCase<PageOfDidayz, GetDidayzByPageUseCaseParams> {
  GetDidayzByPlayerByPageUseCase(this.repository);

  final DidayzRepository repository;

  @override
  Future<Result<PageOfDidayz>> call(GetDidayzByPageUseCaseParams params) async {
    return repository.getDidayzByPlayerByPage(params.page, limit: params.limit);
  }
}

class GetDidayzByCreatorByPageUseCase extends UseCase<PageOfDidayz, GetDidayzByPageUseCaseParams> {
  GetDidayzByCreatorByPageUseCase(this.repository);

  final DidayzRepository repository;

  @override
  Future<Result<PageOfDidayz>> call(GetDidayzByPageUseCaseParams params) async {
    return repository.getDidayzByCreatorByPage(params.page, limit: params.limit, is_publish: params.is_publish == null ? 0 : params.is_publish!);
  }
}

class GetDidayzByPageUseCaseParams {
  const GetDidayzByPageUseCaseParams({
    required this.page,
    required this.limit,
    this.is_publish
  });

  final int page;
  final int limit;
  final int? is_publish;
}
