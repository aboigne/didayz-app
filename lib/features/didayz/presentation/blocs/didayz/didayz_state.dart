part of 'didayz_cubit.dart';

@freezed
class DidayzState with _$DidayzState {
  const DidayzState._();

  const factory DidayzState.loading() = _Loading;

  const factory DidayzState.error() = _Error;

  const factory DidayzState.loaded({
    required List<DidayzEntity> didayz,
    required int page,
    required int total_pages,
  }) = _Loaded;

}
