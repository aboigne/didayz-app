import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/didayz.dart';
import '../../../domain/usecases/get_didayz_by_page_usecase.dart';

part 'didayz_state.dart';
part 'didayz_cubit.freezed.dart';

class DidayzCubit extends Cubit<DidayzState> {
  DidayzCubit({required this.didayzByPlayer}) : super(DidayzState.loading());

  final GetDidayzByPlayerByPageUseCase didayzByPlayer;

  Future<void> getDidayzByPlayer(int page, int limit) async {
    emit(DidayzState.loading());
    final result = await didayzByPlayer(GetDidayzByPageUseCaseParams(page: page, limit: limit));

    result.when(
      success: (pageOfDidayz) {
        emit(DidayzState.loaded(didayz: pageOfDidayz.results, page: pageOfDidayz.page, total_pages: pageOfDidayz.totalPages));
      },
      failure: (failure) {
        emit(DidayzState.error());
      },
    );
  }
}

class DidayzCreatorCubit extends Cubit<DidayzState> {
  DidayzCreatorCubit({required this.didayzByCreator}) : super(DidayzState.loading());

  final GetDidayzByCreatorByPageUseCase didayzByCreator;

  Future<void> getDidayzByCreator(int page, int limit) async {
    emit(DidayzState.loading());
    final result = await didayzByCreator(GetDidayzByPageUseCaseParams(page: page, limit: limit));

    result.when(
      success: (pageOfDidayz) {
        emit(DidayzState.loaded(didayz: pageOfDidayz.results, page: pageOfDidayz.page, total_pages: pageOfDidayz.totalPages));
      },
      failure: (failure) {
        emit(DidayzState.error());
      },
    );
  }
}

class DidayzCreatorPublishedCubit extends Cubit<DidayzState> {
  DidayzCreatorPublishedCubit({required this.didayzByCreator}) : super(DidayzState.loading());

  final GetDidayzByCreatorByPageUseCase didayzByCreator;

  Future<void> getDidayzByCreator(int page, int limit) async {
    emit(DidayzState.loading());
    final result = await didayzByCreator(GetDidayzByPageUseCaseParams(page: page, limit: limit, is_publish: 1));

    result.when(
      success: (pageOfDidayz) {
        emit(DidayzState.loaded(didayz: pageOfDidayz.results, page: pageOfDidayz.page, total_pages: pageOfDidayz.totalPages));
      },
      failure: (failure) {
        emit(DidayzState.error());
      },
    );
  }
}
