import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';

import '../../../../../core/constants/api_constants.dart';
import '../../../../../core/data/datasources/remote_data_source.dart';

abstract class DidayzRemoteDataSource {
  Future<dynamic> getDidayzByCreatorByPage(int page, {int limit=10, int is_publish=0});
  Future<dynamic> getDidayzByPlayerByPage(int page, {int limit=10});
}

class DidayzRemoteDataSourceImpl extends RemoteDataSource implements DidayzRemoteDataSource {
  DidayzRemoteDataSourceImpl({required super.dio});

  final String domain = GlobalConfiguration().getValue('api_base_url');
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  @override
  Future getDidayzByCreatorByPage(int page, {int limit=10, int is_publish=0}) async {
    final key_encrypted = await secureStorage.read(
      key: 'encryptionKey',
    );
    final encryptionKey = base64Url.decode(
      key_encrypted!,
    );

    final boxUser = await Hive.openBox(
      'user',
      encryptionCipher: HiveAesCipher(encryptionKey),
    );

    return performGetRequestApi(
      apiEndpoint: ApiConstants.calendarsByUser(page, limit, is_publish),
      token: boxUser.get('token'),
    );
  }

  @override
  Future getDidayzByPlayerByPage(int page, {int limit=10}) async {
    final key_encrypted = await secureStorage.read(
      key: 'encryptionKey',
    );
    final encryptionKey = base64Url.decode(
      key_encrypted!,
    );

    final boxUser = await Hive.openBox(
      'user',
      encryptionCipher: HiveAesCipher(encryptionKey),
    );

    return performGetRequestApi(
        apiEndpoint: ApiConstants.calendarsByPlayer(page, limit),
        token: boxUser.get('token')
    );
  }
}
