import 'dart:async';

import 'package:didayz/features/didayz/data/datasources/didayz_remote_data_source.dart';
import 'package:didayz/features/didayz/domain/entities/page_of_didayz.dart';

import '../../../../core/domain/entities/result.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/repositories/didayz_repository.dart';
import '../models/page_of_didayz_model.dart';

class DidayzRepositoryImpl implements DidayzRepository {
  DidayzRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  final DidayzRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  @override
  Future<Result<PageOfDidayz>> getDidayzByCreatorByPage(int page, {int limit = 10, int is_publish = 0}) async {
    if (!(await networkInfo.isConnected)) {
      return const Result.failure(Failure.offline());
    }

    try {
      final result = await remoteDataSource.getDidayzByCreatorByPage(page, limit: limit, is_publish: is_publish);
      final pageOfDidayz = PageOfDidayzModel.fromJson(result);
      return Result.success(pageOfDidayz.toDomain());
    } on ServerException {
      return const Result.failure(Failure.server());
    }
  }

  @override
  Future<Result<PageOfDidayz>> getDidayzByPlayerByPage(int page, {int limit = 10}) async {
    if (!(await networkInfo.isConnected)) {
      return const Result.failure(Failure.offline());
    }

    try {
      final result = await remoteDataSource.getDidayzByPlayerByPage(page, limit: limit);
      final pageOfDidayz = PageOfDidayzModel.fromJson(result);
      return Result.success(pageOfDidayz.toDomain());
    } on ServerException {
      return const Result.failure(Failure.server());
    }
  }
}
