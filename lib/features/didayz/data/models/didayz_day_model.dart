import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

import '../../domain/entities/didayz_day.dart';

part 'didayz_day_model.freezed.dart';
part 'didayz_day_model.g.dart';

@freezed
@HiveType(typeId: 0)
class DidayzDayModel with _$DidayzDayModel {
  const factory DidayzDayModel({
    @HiveField(0) @JsonKey(name: 'id') required int id,
    @HiveField(1) @JsonKey(name: 'Number') required int Number,
    @HiveField(2) @JsonKey(name: 'Month') required int Month,
    @HiveField(3) @JsonKey(name: 'Year') required int Year,
    @HiveField(4) @JsonKey(name: 'IntroMessage') String? IntroMsg,
    @HiveField(5) @JsonKey(name: 'SendResponse') required bool SendResponse,
    @HiveField(6) @JsonKey(name: 'Type') required String TypeDay,
    @HiveField(7) @JsonKey(name: 'Texte') String? Texte,
    @HiveField(8) @JsonKey(name: 'Picture') String? Picture,
    @HiveField(9) @JsonKey(name: 'VideoType') String? VideoType,
    @HiveField(10) @JsonKey(name: 'Video') String? Video,
    @HiveField(11) @JsonKey(name: 'SoundType') String? SoundType,
    @HiveField(12) @JsonKey(name: 'Sound') String? Sound,
    @HiveField(13) @JsonKey(name: 'File') String? File,
    @HiveField(14) @JsonKey(name: 'Code') String? Code,
    @HiveField(15) @JsonKey(name: 'calendarId') required int calendarId,
  }) = _DidayzDayModel;

  factory DidayzDayModel.fromJson(dynamic json) => _$DidayzDayModelFromJson(json);

  factory DidayzDayModel.fromDomain(DidayzDayEntity entity) {
    return DidayzDayModel(
      id: entity.id,
      Number: entity.Number,
      Month: entity.Month,
      Year: entity.Year,
      TypeDay: entity.Type,
      calendarId: entity.calendarId,
      SendResponse: entity.SendResponse,
      IntroMsg: entity.IntroMessage,
      Texte: entity.Texte,
      Picture: entity.Picture,
      VideoType: entity.VideoType,
      Video: entity.Video,
      SoundType: entity.SoundType,
      Sound: entity.Sound,
      File: entity.File,
      Code: entity.Code,
    );
  }
}

extension DidayzDayModelX on DidayzDayModel {
  DidayzDayEntity toDomain() {
    return DidayzDayEntity(
      id: id,
      calendarId: calendarId,
      Number: Number,
      Month: Month,
      Year: Year,
      SendResponse: SendResponse,
      Type: TypeDay,
      IntroMessage: IntroMsg,
      Texte: Texte,
      Picture: Picture,
      VideoType: VideoType,
      Video: Video,
      SoundType: SoundType,
      Sound: Sound,
      File: File,
      Code: Code,
    );
  }
}


