// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'page_of_didayz_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PageOfDidayzModel _$$_PageOfDidayzModelFromJson(Map<String, dynamic> json) =>
    _$_PageOfDidayzModel(
      page: json['currentPage'] as int,
      results: (json['calendar'] as List<dynamic>)
          .map((e) => DidayzModel.fromJson(e))
          .toList(),
      totalPages: json['totalPages'] as int,
    );

Map<String, dynamic> _$$_PageOfDidayzModelToJson(
        _$_PageOfDidayzModel instance) =>
    <String, dynamic>{
      'currentPage': instance.page,
      'calendar': instance.results,
      'totalPages': instance.totalPages,
    };
