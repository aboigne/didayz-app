import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

import '../../domain/entities/user.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
@HiveType(typeId: 1)
class UserModel with _$UserModel {
  const factory UserModel({
    @HiveField(0) @JsonKey(name: 'Id') required int id,
    @HiveField(1) @JsonKey(name: 'Name') required String Name,
    @HiveField(2) @JsonKey(name: 'Firstname') required String FirstName,
    @HiveField(3) @JsonKey(name: 'Email') required String Email,
  }) = _UserModel;

  factory UserModel.fromJson(dynamic json) => _$UserModelFromJson(json);

  factory UserModel.fromDomain(UserEntity entity) {
    return UserModel(
        id: entity.id,
        Name: entity.Name,
        FirstName: entity.FirstName,
        Email: entity.Email,
    );
  }
}

extension UserModelX on UserModel {
  UserEntity toDomain() {
    return UserEntity(
        id: id,
        Name: Name,
        FirstName: FirstName,
        Email: Email,
    );
  }
}

