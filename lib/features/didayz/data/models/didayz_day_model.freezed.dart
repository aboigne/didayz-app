// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'didayz_day_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DidayzDayModel _$DidayzDayModelFromJson(Map<String, dynamic> json) {
  return _DidayzDayModel.fromJson(json);
}

/// @nodoc
mixin _$DidayzDayModel {
  @HiveField(0)
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: 'Number')
  int get Number => throw _privateConstructorUsedError;
  @HiveField(2)
  @JsonKey(name: 'Month')
  int get Month => throw _privateConstructorUsedError;
  @HiveField(3)
  @JsonKey(name: 'Year')
  int get Year => throw _privateConstructorUsedError;
  @HiveField(4)
  @JsonKey(name: 'IntroMessage')
  String? get IntroMsg => throw _privateConstructorUsedError;
  @HiveField(5)
  @JsonKey(name: 'SendResponse')
  bool get SendResponse => throw _privateConstructorUsedError;
  @HiveField(6)
  @JsonKey(name: 'Type')
  String get TypeDay => throw _privateConstructorUsedError;
  @HiveField(7)
  @JsonKey(name: 'Texte')
  String? get Texte => throw _privateConstructorUsedError;
  @HiveField(8)
  @JsonKey(name: 'Picture')
  String? get Picture => throw _privateConstructorUsedError;
  @HiveField(9)
  @JsonKey(name: 'VideoType')
  String? get VideoType => throw _privateConstructorUsedError;
  @HiveField(10)
  @JsonKey(name: 'Video')
  String? get Video => throw _privateConstructorUsedError;
  @HiveField(11)
  @JsonKey(name: 'SoundType')
  String? get SoundType => throw _privateConstructorUsedError;
  @HiveField(12)
  @JsonKey(name: 'Sound')
  String? get Sound => throw _privateConstructorUsedError;
  @HiveField(13)
  @JsonKey(name: 'File')
  String? get File => throw _privateConstructorUsedError;
  @HiveField(14)
  @JsonKey(name: 'Code')
  String? get Code => throw _privateConstructorUsedError;
  @HiveField(15)
  @JsonKey(name: 'calendarId')
  int get calendarId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DidayzDayModelCopyWith<DidayzDayModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DidayzDayModelCopyWith<$Res> {
  factory $DidayzDayModelCopyWith(
          DidayzDayModel value, $Res Function(DidayzDayModel) then) =
      _$DidayzDayModelCopyWithImpl<$Res, DidayzDayModel>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: 'id') int id,
      @HiveField(1) @JsonKey(name: 'Number') int Number,
      @HiveField(2) @JsonKey(name: 'Month') int Month,
      @HiveField(3) @JsonKey(name: 'Year') int Year,
      @HiveField(4) @JsonKey(name: 'IntroMessage') String? IntroMsg,
      @HiveField(5) @JsonKey(name: 'SendResponse') bool SendResponse,
      @HiveField(6) @JsonKey(name: 'Type') String TypeDay,
      @HiveField(7) @JsonKey(name: 'Texte') String? Texte,
      @HiveField(8) @JsonKey(name: 'Picture') String? Picture,
      @HiveField(9) @JsonKey(name: 'VideoType') String? VideoType,
      @HiveField(10) @JsonKey(name: 'Video') String? Video,
      @HiveField(11) @JsonKey(name: 'SoundType') String? SoundType,
      @HiveField(12) @JsonKey(name: 'Sound') String? Sound,
      @HiveField(13) @JsonKey(name: 'File') String? File,
      @HiveField(14) @JsonKey(name: 'Code') String? Code,
      @HiveField(15) @JsonKey(name: 'calendarId') int calendarId});
}

/// @nodoc
class _$DidayzDayModelCopyWithImpl<$Res, $Val extends DidayzDayModel>
    implements $DidayzDayModelCopyWith<$Res> {
  _$DidayzDayModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Number = null,
    Object? Month = null,
    Object? Year = null,
    Object? IntroMsg = freezed,
    Object? SendResponse = null,
    Object? TypeDay = null,
    Object? Texte = freezed,
    Object? Picture = freezed,
    Object? VideoType = freezed,
    Object? Video = freezed,
    Object? SoundType = freezed,
    Object? Sound = freezed,
    Object? File = freezed,
    Object? Code = freezed,
    Object? calendarId = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Number: null == Number
          ? _value.Number
          : Number // ignore: cast_nullable_to_non_nullable
              as int,
      Month: null == Month
          ? _value.Month
          : Month // ignore: cast_nullable_to_non_nullable
              as int,
      Year: null == Year
          ? _value.Year
          : Year // ignore: cast_nullable_to_non_nullable
              as int,
      IntroMsg: freezed == IntroMsg
          ? _value.IntroMsg
          : IntroMsg // ignore: cast_nullable_to_non_nullable
              as String?,
      SendResponse: null == SendResponse
          ? _value.SendResponse
          : SendResponse // ignore: cast_nullable_to_non_nullable
              as bool,
      TypeDay: null == TypeDay
          ? _value.TypeDay
          : TypeDay // ignore: cast_nullable_to_non_nullable
              as String,
      Texte: freezed == Texte
          ? _value.Texte
          : Texte // ignore: cast_nullable_to_non_nullable
              as String?,
      Picture: freezed == Picture
          ? _value.Picture
          : Picture // ignore: cast_nullable_to_non_nullable
              as String?,
      VideoType: freezed == VideoType
          ? _value.VideoType
          : VideoType // ignore: cast_nullable_to_non_nullable
              as String?,
      Video: freezed == Video
          ? _value.Video
          : Video // ignore: cast_nullable_to_non_nullable
              as String?,
      SoundType: freezed == SoundType
          ? _value.SoundType
          : SoundType // ignore: cast_nullable_to_non_nullable
              as String?,
      Sound: freezed == Sound
          ? _value.Sound
          : Sound // ignore: cast_nullable_to_non_nullable
              as String?,
      File: freezed == File
          ? _value.File
          : File // ignore: cast_nullable_to_non_nullable
              as String?,
      Code: freezed == Code
          ? _value.Code
          : Code // ignore: cast_nullable_to_non_nullable
              as String?,
      calendarId: null == calendarId
          ? _value.calendarId
          : calendarId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DidayzDayModelCopyWith<$Res>
    implements $DidayzDayModelCopyWith<$Res> {
  factory _$$_DidayzDayModelCopyWith(
          _$_DidayzDayModel value, $Res Function(_$_DidayzDayModel) then) =
      __$$_DidayzDayModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: 'id') int id,
      @HiveField(1) @JsonKey(name: 'Number') int Number,
      @HiveField(2) @JsonKey(name: 'Month') int Month,
      @HiveField(3) @JsonKey(name: 'Year') int Year,
      @HiveField(4) @JsonKey(name: 'IntroMessage') String? IntroMsg,
      @HiveField(5) @JsonKey(name: 'SendResponse') bool SendResponse,
      @HiveField(6) @JsonKey(name: 'Type') String TypeDay,
      @HiveField(7) @JsonKey(name: 'Texte') String? Texte,
      @HiveField(8) @JsonKey(name: 'Picture') String? Picture,
      @HiveField(9) @JsonKey(name: 'VideoType') String? VideoType,
      @HiveField(10) @JsonKey(name: 'Video') String? Video,
      @HiveField(11) @JsonKey(name: 'SoundType') String? SoundType,
      @HiveField(12) @JsonKey(name: 'Sound') String? Sound,
      @HiveField(13) @JsonKey(name: 'File') String? File,
      @HiveField(14) @JsonKey(name: 'Code') String? Code,
      @HiveField(15) @JsonKey(name: 'calendarId') int calendarId});
}

/// @nodoc
class __$$_DidayzDayModelCopyWithImpl<$Res>
    extends _$DidayzDayModelCopyWithImpl<$Res, _$_DidayzDayModel>
    implements _$$_DidayzDayModelCopyWith<$Res> {
  __$$_DidayzDayModelCopyWithImpl(
      _$_DidayzDayModel _value, $Res Function(_$_DidayzDayModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Number = null,
    Object? Month = null,
    Object? Year = null,
    Object? IntroMsg = freezed,
    Object? SendResponse = null,
    Object? TypeDay = null,
    Object? Texte = freezed,
    Object? Picture = freezed,
    Object? VideoType = freezed,
    Object? Video = freezed,
    Object? SoundType = freezed,
    Object? Sound = freezed,
    Object? File = freezed,
    Object? Code = freezed,
    Object? calendarId = null,
  }) {
    return _then(_$_DidayzDayModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Number: null == Number
          ? _value.Number
          : Number // ignore: cast_nullable_to_non_nullable
              as int,
      Month: null == Month
          ? _value.Month
          : Month // ignore: cast_nullable_to_non_nullable
              as int,
      Year: null == Year
          ? _value.Year
          : Year // ignore: cast_nullable_to_non_nullable
              as int,
      IntroMsg: freezed == IntroMsg
          ? _value.IntroMsg
          : IntroMsg // ignore: cast_nullable_to_non_nullable
              as String?,
      SendResponse: null == SendResponse
          ? _value.SendResponse
          : SendResponse // ignore: cast_nullable_to_non_nullable
              as bool,
      TypeDay: null == TypeDay
          ? _value.TypeDay
          : TypeDay // ignore: cast_nullable_to_non_nullable
              as String,
      Texte: freezed == Texte
          ? _value.Texte
          : Texte // ignore: cast_nullable_to_non_nullable
              as String?,
      Picture: freezed == Picture
          ? _value.Picture
          : Picture // ignore: cast_nullable_to_non_nullable
              as String?,
      VideoType: freezed == VideoType
          ? _value.VideoType
          : VideoType // ignore: cast_nullable_to_non_nullable
              as String?,
      Video: freezed == Video
          ? _value.Video
          : Video // ignore: cast_nullable_to_non_nullable
              as String?,
      SoundType: freezed == SoundType
          ? _value.SoundType
          : SoundType // ignore: cast_nullable_to_non_nullable
              as String?,
      Sound: freezed == Sound
          ? _value.Sound
          : Sound // ignore: cast_nullable_to_non_nullable
              as String?,
      File: freezed == File
          ? _value.File
          : File // ignore: cast_nullable_to_non_nullable
              as String?,
      Code: freezed == Code
          ? _value.Code
          : Code // ignore: cast_nullable_to_non_nullable
              as String?,
      calendarId: null == calendarId
          ? _value.calendarId
          : calendarId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DidayzDayModel implements _DidayzDayModel {
  const _$_DidayzDayModel(
      {@HiveField(0) @JsonKey(name: 'id') required this.id,
      @HiveField(1) @JsonKey(name: 'Number') required this.Number,
      @HiveField(2) @JsonKey(name: 'Month') required this.Month,
      @HiveField(3) @JsonKey(name: 'Year') required this.Year,
      @HiveField(4) @JsonKey(name: 'IntroMessage') this.IntroMsg,
      @HiveField(5) @JsonKey(name: 'SendResponse') required this.SendResponse,
      @HiveField(6) @JsonKey(name: 'Type') required this.TypeDay,
      @HiveField(7) @JsonKey(name: 'Texte') this.Texte,
      @HiveField(8) @JsonKey(name: 'Picture') this.Picture,
      @HiveField(9) @JsonKey(name: 'VideoType') this.VideoType,
      @HiveField(10) @JsonKey(name: 'Video') this.Video,
      @HiveField(11) @JsonKey(name: 'SoundType') this.SoundType,
      @HiveField(12) @JsonKey(name: 'Sound') this.Sound,
      @HiveField(13) @JsonKey(name: 'File') this.File,
      @HiveField(14) @JsonKey(name: 'Code') this.Code,
      @HiveField(15) @JsonKey(name: 'calendarId') required this.calendarId});

  factory _$_DidayzDayModel.fromJson(Map<String, dynamic> json) =>
      _$$_DidayzDayModelFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: 'id')
  final int id;
  @override
  @HiveField(1)
  @JsonKey(name: 'Number')
  final int Number;
  @override
  @HiveField(2)
  @JsonKey(name: 'Month')
  final int Month;
  @override
  @HiveField(3)
  @JsonKey(name: 'Year')
  final int Year;
  @override
  @HiveField(4)
  @JsonKey(name: 'IntroMessage')
  final String? IntroMsg;
  @override
  @HiveField(5)
  @JsonKey(name: 'SendResponse')
  final bool SendResponse;
  @override
  @HiveField(6)
  @JsonKey(name: 'Type')
  final String TypeDay;
  @override
  @HiveField(7)
  @JsonKey(name: 'Texte')
  final String? Texte;
  @override
  @HiveField(8)
  @JsonKey(name: 'Picture')
  final String? Picture;
  @override
  @HiveField(9)
  @JsonKey(name: 'VideoType')
  final String? VideoType;
  @override
  @HiveField(10)
  @JsonKey(name: 'Video')
  final String? Video;
  @override
  @HiveField(11)
  @JsonKey(name: 'SoundType')
  final String? SoundType;
  @override
  @HiveField(12)
  @JsonKey(name: 'Sound')
  final String? Sound;
  @override
  @HiveField(13)
  @JsonKey(name: 'File')
  final String? File;
  @override
  @HiveField(14)
  @JsonKey(name: 'Code')
  final String? Code;
  @override
  @HiveField(15)
  @JsonKey(name: 'calendarId')
  final int calendarId;

  @override
  String toString() {
    return 'DidayzDayModel(id: $id, Number: $Number, Month: $Month, Year: $Year, IntroMsg: $IntroMsg, SendResponse: $SendResponse, TypeDay: $TypeDay, Texte: $Texte, Picture: $Picture, VideoType: $VideoType, Video: $Video, SoundType: $SoundType, Sound: $Sound, File: $File, Code: $Code, calendarId: $calendarId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DidayzDayModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.Number, Number) || other.Number == Number) &&
            (identical(other.Month, Month) || other.Month == Month) &&
            (identical(other.Year, Year) || other.Year == Year) &&
            (identical(other.IntroMsg, IntroMsg) ||
                other.IntroMsg == IntroMsg) &&
            (identical(other.SendResponse, SendResponse) ||
                other.SendResponse == SendResponse) &&
            (identical(other.TypeDay, TypeDay) || other.TypeDay == TypeDay) &&
            (identical(other.Texte, Texte) || other.Texte == Texte) &&
            (identical(other.Picture, Picture) || other.Picture == Picture) &&
            (identical(other.VideoType, VideoType) ||
                other.VideoType == VideoType) &&
            (identical(other.Video, Video) || other.Video == Video) &&
            (identical(other.SoundType, SoundType) ||
                other.SoundType == SoundType) &&
            (identical(other.Sound, Sound) || other.Sound == Sound) &&
            (identical(other.File, File) || other.File == File) &&
            (identical(other.Code, Code) || other.Code == Code) &&
            (identical(other.calendarId, calendarId) ||
                other.calendarId == calendarId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      Number,
      Month,
      Year,
      IntroMsg,
      SendResponse,
      TypeDay,
      Texte,
      Picture,
      VideoType,
      Video,
      SoundType,
      Sound,
      File,
      Code,
      calendarId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DidayzDayModelCopyWith<_$_DidayzDayModel> get copyWith =>
      __$$_DidayzDayModelCopyWithImpl<_$_DidayzDayModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DidayzDayModelToJson(
      this,
    );
  }
}

abstract class _DidayzDayModel implements DidayzDayModel {
  const factory _DidayzDayModel(
      {@HiveField(0)
      @JsonKey(name: 'id')
          required final int id,
      @HiveField(1)
      @JsonKey(name: 'Number')
          required final int Number,
      @HiveField(2)
      @JsonKey(name: 'Month')
          required final int Month,
      @HiveField(3)
      @JsonKey(name: 'Year')
          required final int Year,
      @HiveField(4)
      @JsonKey(name: 'IntroMessage')
          final String? IntroMsg,
      @HiveField(5)
      @JsonKey(name: 'SendResponse')
          required final bool SendResponse,
      @HiveField(6)
      @JsonKey(name: 'Type')
          required final String TypeDay,
      @HiveField(7)
      @JsonKey(name: 'Texte')
          final String? Texte,
      @HiveField(8)
      @JsonKey(name: 'Picture')
          final String? Picture,
      @HiveField(9)
      @JsonKey(name: 'VideoType')
          final String? VideoType,
      @HiveField(10)
      @JsonKey(name: 'Video')
          final String? Video,
      @HiveField(11)
      @JsonKey(name: 'SoundType')
          final String? SoundType,
      @HiveField(12)
      @JsonKey(name: 'Sound')
          final String? Sound,
      @HiveField(13)
      @JsonKey(name: 'File')
          final String? File,
      @HiveField(14)
      @JsonKey(name: 'Code')
          final String? Code,
      @HiveField(15)
      @JsonKey(name: 'calendarId')
          required final int calendarId}) = _$_DidayzDayModel;

  factory _DidayzDayModel.fromJson(Map<String, dynamic> json) =
      _$_DidayzDayModel.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: 'id')
  int get id;
  @override
  @HiveField(1)
  @JsonKey(name: 'Number')
  int get Number;
  @override
  @HiveField(2)
  @JsonKey(name: 'Month')
  int get Month;
  @override
  @HiveField(3)
  @JsonKey(name: 'Year')
  int get Year;
  @override
  @HiveField(4)
  @JsonKey(name: 'IntroMessage')
  String? get IntroMsg;
  @override
  @HiveField(5)
  @JsonKey(name: 'SendResponse')
  bool get SendResponse;
  @override
  @HiveField(6)
  @JsonKey(name: 'Type')
  String get TypeDay;
  @override
  @HiveField(7)
  @JsonKey(name: 'Texte')
  String? get Texte;
  @override
  @HiveField(8)
  @JsonKey(name: 'Picture')
  String? get Picture;
  @override
  @HiveField(9)
  @JsonKey(name: 'VideoType')
  String? get VideoType;
  @override
  @HiveField(10)
  @JsonKey(name: 'Video')
  String? get Video;
  @override
  @HiveField(11)
  @JsonKey(name: 'SoundType')
  String? get SoundType;
  @override
  @HiveField(12)
  @JsonKey(name: 'Sound')
  String? get Sound;
  @override
  @HiveField(13)
  @JsonKey(name: 'File')
  String? get File;
  @override
  @HiveField(14)
  @JsonKey(name: 'Code')
  String? get Code;
  @override
  @HiveField(15)
  @JsonKey(name: 'calendarId')
  int get calendarId;
  @override
  @JsonKey(ignore: true)
  _$$_DidayzDayModelCopyWith<_$_DidayzDayModel> get copyWith =>
      throw _privateConstructorUsedError;
}
