// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'didayz_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DidayzModel _$DidayzModelFromJson(Map<String, dynamic> json) {
  return _DidayzModel.fromJson(json);
}

/// @nodoc
mixin _$DidayzModel {
  @HiveField(0)
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: 'Name')
  String get Name => throw _privateConstructorUsedError;
  @HiveField(2)
  @JsonKey(name: 'Key')
  String get Key => throw _privateConstructorUsedError;
  @HiveField(3)
  @JsonKey(name: 'IntroMessage')
  String? get IntroMsg => throw _privateConstructorUsedError;
  @HiveField(4)
  @JsonKey(name: 'BackgroundPicture')
  String? get Background => throw _privateConstructorUsedError;
  @HiveField(5)
  @JsonKey(name: 'DateEnd')
  DateTime get DateEnd => throw _privateConstructorUsedError;
  @HiveField(6)
  @JsonKey(name: 'NBDays')
  int get NBDays => throw _privateConstructorUsedError;
  @HiveField(7)
  @JsonKey(name: 'IsPublished')
  bool get IsPublished => throw _privateConstructorUsedError;
  @HiveField(8)
  @JsonKey(name: 'Progress')
  double? get Progress => throw _privateConstructorUsedError;
  @HiveField(9)
  @JsonKey(name: 'user')
  UserModel get Creator => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DidayzModelCopyWith<DidayzModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DidayzModelCopyWith<$Res> {
  factory $DidayzModelCopyWith(
          DidayzModel value, $Res Function(DidayzModel) then) =
      _$DidayzModelCopyWithImpl<$Res, DidayzModel>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: 'id') int id,
      @HiveField(1) @JsonKey(name: 'Name') String Name,
      @HiveField(2) @JsonKey(name: 'Key') String Key,
      @HiveField(3) @JsonKey(name: 'IntroMessage') String? IntroMsg,
      @HiveField(4) @JsonKey(name: 'BackgroundPicture') String? Background,
      @HiveField(5) @JsonKey(name: 'DateEnd') DateTime DateEnd,
      @HiveField(6) @JsonKey(name: 'NBDays') int NBDays,
      @HiveField(7) @JsonKey(name: 'IsPublished') bool IsPublished,
      @HiveField(8) @JsonKey(name: 'Progress') double? Progress,
      @HiveField(9) @JsonKey(name: 'user') UserModel Creator});

  $UserModelCopyWith<$Res> get Creator;
}

/// @nodoc
class _$DidayzModelCopyWithImpl<$Res, $Val extends DidayzModel>
    implements $DidayzModelCopyWith<$Res> {
  _$DidayzModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Name = null,
    Object? Key = null,
    Object? IntroMsg = freezed,
    Object? Background = freezed,
    Object? DateEnd = null,
    Object? NBDays = null,
    Object? IsPublished = null,
    Object? Progress = freezed,
    Object? Creator = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Name: null == Name
          ? _value.Name
          : Name // ignore: cast_nullable_to_non_nullable
              as String,
      Key: null == Key
          ? _value.Key
          : Key // ignore: cast_nullable_to_non_nullable
              as String,
      IntroMsg: freezed == IntroMsg
          ? _value.IntroMsg
          : IntroMsg // ignore: cast_nullable_to_non_nullable
              as String?,
      Background: freezed == Background
          ? _value.Background
          : Background // ignore: cast_nullable_to_non_nullable
              as String?,
      DateEnd: null == DateEnd
          ? _value.DateEnd
          : DateEnd // ignore: cast_nullable_to_non_nullable
              as DateTime,
      NBDays: null == NBDays
          ? _value.NBDays
          : NBDays // ignore: cast_nullable_to_non_nullable
              as int,
      IsPublished: null == IsPublished
          ? _value.IsPublished
          : IsPublished // ignore: cast_nullable_to_non_nullable
              as bool,
      Progress: freezed == Progress
          ? _value.Progress
          : Progress // ignore: cast_nullable_to_non_nullable
              as double?,
      Creator: null == Creator
          ? _value.Creator
          : Creator // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $UserModelCopyWith<$Res> get Creator {
    return $UserModelCopyWith<$Res>(_value.Creator, (value) {
      return _then(_value.copyWith(Creator: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_DidayzModelCopyWith<$Res>
    implements $DidayzModelCopyWith<$Res> {
  factory _$$_DidayzModelCopyWith(
          _$_DidayzModel value, $Res Function(_$_DidayzModel) then) =
      __$$_DidayzModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: 'id') int id,
      @HiveField(1) @JsonKey(name: 'Name') String Name,
      @HiveField(2) @JsonKey(name: 'Key') String Key,
      @HiveField(3) @JsonKey(name: 'IntroMessage') String? IntroMsg,
      @HiveField(4) @JsonKey(name: 'BackgroundPicture') String? Background,
      @HiveField(5) @JsonKey(name: 'DateEnd') DateTime DateEnd,
      @HiveField(6) @JsonKey(name: 'NBDays') int NBDays,
      @HiveField(7) @JsonKey(name: 'IsPublished') bool IsPublished,
      @HiveField(8) @JsonKey(name: 'Progress') double? Progress,
      @HiveField(9) @JsonKey(name: 'user') UserModel Creator});

  @override
  $UserModelCopyWith<$Res> get Creator;
}

/// @nodoc
class __$$_DidayzModelCopyWithImpl<$Res>
    extends _$DidayzModelCopyWithImpl<$Res, _$_DidayzModel>
    implements _$$_DidayzModelCopyWith<$Res> {
  __$$_DidayzModelCopyWithImpl(
      _$_DidayzModel _value, $Res Function(_$_DidayzModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Name = null,
    Object? Key = null,
    Object? IntroMsg = freezed,
    Object? Background = freezed,
    Object? DateEnd = null,
    Object? NBDays = null,
    Object? IsPublished = null,
    Object? Progress = freezed,
    Object? Creator = null,
  }) {
    return _then(_$_DidayzModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Name: null == Name
          ? _value.Name
          : Name // ignore: cast_nullable_to_non_nullable
              as String,
      Key: null == Key
          ? _value.Key
          : Key // ignore: cast_nullable_to_non_nullable
              as String,
      IntroMsg: freezed == IntroMsg
          ? _value.IntroMsg
          : IntroMsg // ignore: cast_nullable_to_non_nullable
              as String?,
      Background: freezed == Background
          ? _value.Background
          : Background // ignore: cast_nullable_to_non_nullable
              as String?,
      DateEnd: null == DateEnd
          ? _value.DateEnd
          : DateEnd // ignore: cast_nullable_to_non_nullable
              as DateTime,
      NBDays: null == NBDays
          ? _value.NBDays
          : NBDays // ignore: cast_nullable_to_non_nullable
              as int,
      IsPublished: null == IsPublished
          ? _value.IsPublished
          : IsPublished // ignore: cast_nullable_to_non_nullable
              as bool,
      Progress: freezed == Progress
          ? _value.Progress
          : Progress // ignore: cast_nullable_to_non_nullable
              as double?,
      Creator: null == Creator
          ? _value.Creator
          : Creator // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DidayzModel implements _DidayzModel {
  const _$_DidayzModel(
      {@HiveField(0) @JsonKey(name: 'id') required this.id,
      @HiveField(1) @JsonKey(name: 'Name') required this.Name,
      @HiveField(2) @JsonKey(name: 'Key') required this.Key,
      @HiveField(3) @JsonKey(name: 'IntroMessage') this.IntroMsg,
      @HiveField(4) @JsonKey(name: 'BackgroundPicture') this.Background,
      @HiveField(5) @JsonKey(name: 'DateEnd') required this.DateEnd,
      @HiveField(6) @JsonKey(name: 'NBDays') required this.NBDays,
      @HiveField(7) @JsonKey(name: 'IsPublished') required this.IsPublished,
      @HiveField(8) @JsonKey(name: 'Progress') this.Progress,
      @HiveField(9) @JsonKey(name: 'user') required this.Creator});

  factory _$_DidayzModel.fromJson(Map<String, dynamic> json) =>
      _$$_DidayzModelFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: 'id')
  final int id;
  @override
  @HiveField(1)
  @JsonKey(name: 'Name')
  final String Name;
  @override
  @HiveField(2)
  @JsonKey(name: 'Key')
  final String Key;
  @override
  @HiveField(3)
  @JsonKey(name: 'IntroMessage')
  final String? IntroMsg;
  @override
  @HiveField(4)
  @JsonKey(name: 'BackgroundPicture')
  final String? Background;
  @override
  @HiveField(5)
  @JsonKey(name: 'DateEnd')
  final DateTime DateEnd;
  @override
  @HiveField(6)
  @JsonKey(name: 'NBDays')
  final int NBDays;
  @override
  @HiveField(7)
  @JsonKey(name: 'IsPublished')
  final bool IsPublished;
  @override
  @HiveField(8)
  @JsonKey(name: 'Progress')
  final double? Progress;
  @override
  @HiveField(9)
  @JsonKey(name: 'user')
  final UserModel Creator;

  @override
  String toString() {
    return 'DidayzModel(id: $id, Name: $Name, Key: $Key, IntroMsg: $IntroMsg, Background: $Background, DateEnd: $DateEnd, NBDays: $NBDays, IsPublished: $IsPublished, Progress: $Progress, Creator: $Creator)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DidayzModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.Name, Name) || other.Name == Name) &&
            (identical(other.Key, Key) || other.Key == Key) &&
            (identical(other.IntroMsg, IntroMsg) ||
                other.IntroMsg == IntroMsg) &&
            (identical(other.Background, Background) ||
                other.Background == Background) &&
            (identical(other.DateEnd, DateEnd) || other.DateEnd == DateEnd) &&
            (identical(other.NBDays, NBDays) || other.NBDays == NBDays) &&
            (identical(other.IsPublished, IsPublished) ||
                other.IsPublished == IsPublished) &&
            (identical(other.Progress, Progress) ||
                other.Progress == Progress) &&
            (identical(other.Creator, Creator) || other.Creator == Creator));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, Name, Key, IntroMsg,
      Background, DateEnd, NBDays, IsPublished, Progress, Creator);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DidayzModelCopyWith<_$_DidayzModel> get copyWith =>
      __$$_DidayzModelCopyWithImpl<_$_DidayzModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DidayzModelToJson(
      this,
    );
  }
}

abstract class _DidayzModel implements DidayzModel {
  const factory _DidayzModel(
      {@HiveField(0)
      @JsonKey(name: 'id')
          required final int id,
      @HiveField(1)
      @JsonKey(name: 'Name')
          required final String Name,
      @HiveField(2)
      @JsonKey(name: 'Key')
          required final String Key,
      @HiveField(3)
      @JsonKey(name: 'IntroMessage')
          final String? IntroMsg,
      @HiveField(4)
      @JsonKey(name: 'BackgroundPicture')
          final String? Background,
      @HiveField(5)
      @JsonKey(name: 'DateEnd')
          required final DateTime DateEnd,
      @HiveField(6)
      @JsonKey(name: 'NBDays')
          required final int NBDays,
      @HiveField(7)
      @JsonKey(name: 'IsPublished')
          required final bool IsPublished,
      @HiveField(8)
      @JsonKey(name: 'Progress')
          final double? Progress,
      @HiveField(9)
      @JsonKey(name: 'user')
          required final UserModel Creator}) = _$_DidayzModel;

  factory _DidayzModel.fromJson(Map<String, dynamic> json) =
      _$_DidayzModel.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: 'id')
  int get id;
  @override
  @HiveField(1)
  @JsonKey(name: 'Name')
  String get Name;
  @override
  @HiveField(2)
  @JsonKey(name: 'Key')
  String get Key;
  @override
  @HiveField(3)
  @JsonKey(name: 'IntroMessage')
  String? get IntroMsg;
  @override
  @HiveField(4)
  @JsonKey(name: 'BackgroundPicture')
  String? get Background;
  @override
  @HiveField(5)
  @JsonKey(name: 'DateEnd')
  DateTime get DateEnd;
  @override
  @HiveField(6)
  @JsonKey(name: 'NBDays')
  int get NBDays;
  @override
  @HiveField(7)
  @JsonKey(name: 'IsPublished')
  bool get IsPublished;
  @override
  @HiveField(8)
  @JsonKey(name: 'Progress')
  double? get Progress;
  @override
  @HiveField(9)
  @JsonKey(name: 'user')
  UserModel get Creator;
  @override
  @JsonKey(ignore: true)
  _$$_DidayzModelCopyWith<_$_DidayzModel> get copyWith =>
      throw _privateConstructorUsedError;
}
