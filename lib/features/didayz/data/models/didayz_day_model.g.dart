// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'didayz_day_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DidayzDayModelAdapter extends TypeAdapter<_$_DidayzDayModel> {
  @override
  final int typeId = 0;

  @override
  _$_DidayzDayModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return _$_DidayzDayModel(
      id: fields[0] as int,
      Number: fields[1] as int,
      Month: fields[2] as int,
      Year: fields[3] as int,
      TypeDay: fields[4] as String,
      calendarId: fields[5] as int,
      SendResponse: fields[6] as bool,
      IntroMsg: fields[7] as String,
      Texte: fields[8] as String,
      Picture: fields[9] as String,
      VideoType: fields[10] as String,
      Video: fields[11] as String,
      SoundType: fields[12] as String,
      Sound: fields[13] as String,
      File: fields[14] as String,
      Code: fields[15] as String,
    );
  }

  @override
  void write(BinaryWriter writer, DidayzDayModel obj) {
    writer.writeByte(0);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DidayzDayModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DidayzDayModel _$$_DidayzDayModelFromJson(Map<String, dynamic> json) =>
    _$_DidayzDayModel(
      id: json['id'] as int,
      Number: json['Number'] as int,
      Month: json['Month'] as int,
      Year: json['Year'] as int,
      IntroMsg: json['IntroMessage'] as String?,
      SendResponse: json['SendResponse'] as bool,
      TypeDay: json['Type'] as String,
      Texte: json['Texte'] as String?,
      Picture: json['Picture'] as String?,
      VideoType: json['VideoType'] as String?,
      Video: json['Video'] as String?,
      SoundType: json['SoundType'] as String?,
      Sound: json['Sound'] as String?,
      File: json['File'] as String?,
      Code: json['Code'] as String?,
      calendarId: json['calendarId'] as int,
    );

Map<String, dynamic> _$$_DidayzDayModelToJson(_$_DidayzDayModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'Number': instance.Number,
      'Month': instance.Month,
      'Year': instance.Year,
      'IntroMessage': instance.IntroMsg,
      'SendResponse': instance.SendResponse,
      'Type': instance.TypeDay,
      'Texte': instance.Texte,
      'Picture': instance.Picture,
      'VideoType': instance.VideoType,
      'Video': instance.Video,
      'SoundType': instance.SoundType,
      'Sound': instance.Sound,
      'File': instance.File,
      'Code': instance.Code,
      'calendarId': instance.calendarId,
    };
