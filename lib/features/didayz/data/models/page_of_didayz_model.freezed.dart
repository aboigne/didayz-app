// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'page_of_didayz_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PageOfDidayzModel _$PageOfDidayzModelFromJson(Map<String, dynamic> json) {
  return _PageOfDidayzModel.fromJson(json);
}

/// @nodoc
mixin _$PageOfDidayzModel {
  @JsonKey(name: 'currentPage')
  int get page => throw _privateConstructorUsedError;
  @JsonKey(name: 'calendar')
  List<DidayzModel> get results => throw _privateConstructorUsedError;
  @JsonKey(name: 'totalPages')
  int get totalPages => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PageOfDidayzModelCopyWith<PageOfDidayzModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PageOfDidayzModelCopyWith<$Res> {
  factory $PageOfDidayzModelCopyWith(
          PageOfDidayzModel value, $Res Function(PageOfDidayzModel) then) =
      _$PageOfDidayzModelCopyWithImpl<$Res, PageOfDidayzModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'currentPage') int page,
      @JsonKey(name: 'calendar') List<DidayzModel> results,
      @JsonKey(name: 'totalPages') int totalPages});
}

/// @nodoc
class _$PageOfDidayzModelCopyWithImpl<$Res, $Val extends PageOfDidayzModel>
    implements $PageOfDidayzModelCopyWith<$Res> {
  _$PageOfDidayzModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? results = null,
    Object? totalPages = null,
  }) {
    return _then(_value.copyWith(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<DidayzModel>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PageOfDidayzModelCopyWith<$Res>
    implements $PageOfDidayzModelCopyWith<$Res> {
  factory _$$_PageOfDidayzModelCopyWith(_$_PageOfDidayzModel value,
          $Res Function(_$_PageOfDidayzModel) then) =
      __$$_PageOfDidayzModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'currentPage') int page,
      @JsonKey(name: 'calendar') List<DidayzModel> results,
      @JsonKey(name: 'totalPages') int totalPages});
}

/// @nodoc
class __$$_PageOfDidayzModelCopyWithImpl<$Res>
    extends _$PageOfDidayzModelCopyWithImpl<$Res, _$_PageOfDidayzModel>
    implements _$$_PageOfDidayzModelCopyWith<$Res> {
  __$$_PageOfDidayzModelCopyWithImpl(
      _$_PageOfDidayzModel _value, $Res Function(_$_PageOfDidayzModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? results = null,
    Object? totalPages = null,
  }) {
    return _then(_$_PageOfDidayzModel(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<DidayzModel>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PageOfDidayzModel implements _PageOfDidayzModel {
  const _$_PageOfDidayzModel(
      {@JsonKey(name: 'currentPage') required this.page,
      @JsonKey(name: 'calendar') required final List<DidayzModel> results,
      @JsonKey(name: 'totalPages') required this.totalPages})
      : _results = results;

  factory _$_PageOfDidayzModel.fromJson(Map<String, dynamic> json) =>
      _$$_PageOfDidayzModelFromJson(json);

  @override
  @JsonKey(name: 'currentPage')
  final int page;
  final List<DidayzModel> _results;
  @override
  @JsonKey(name: 'calendar')
  List<DidayzModel> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  @JsonKey(name: 'totalPages')
  final int totalPages;

  @override
  String toString() {
    return 'PageOfDidayzModel(page: $page, results: $results, totalPages: $totalPages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PageOfDidayzModel &&
            (identical(other.page, page) || other.page == page) &&
            const DeepCollectionEquality().equals(other._results, _results) &&
            (identical(other.totalPages, totalPages) ||
                other.totalPages == totalPages));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, page,
      const DeepCollectionEquality().hash(_results), totalPages);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PageOfDidayzModelCopyWith<_$_PageOfDidayzModel> get copyWith =>
      __$$_PageOfDidayzModelCopyWithImpl<_$_PageOfDidayzModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PageOfDidayzModelToJson(
      this,
    );
  }
}

abstract class _PageOfDidayzModel implements PageOfDidayzModel {
  const factory _PageOfDidayzModel(
          {@JsonKey(name: 'currentPage') required final int page,
          @JsonKey(name: 'calendar') required final List<DidayzModel> results,
          @JsonKey(name: 'totalPages') required final int totalPages}) =
      _$_PageOfDidayzModel;

  factory _PageOfDidayzModel.fromJson(Map<String, dynamic> json) =
      _$_PageOfDidayzModel.fromJson;

  @override
  @JsonKey(name: 'currentPage')
  int get page;
  @override
  @JsonKey(name: 'calendar')
  List<DidayzModel> get results;
  @override
  @JsonKey(name: 'totalPages')
  int get totalPages;
  @override
  @JsonKey(ignore: true)
  _$$_PageOfDidayzModelCopyWith<_$_PageOfDidayzModel> get copyWith =>
      throw _privateConstructorUsedError;
}
