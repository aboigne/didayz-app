// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return _UserModel.fromJson(json);
}

/// @nodoc
mixin _$UserModel {
  @HiveField(0)
  @JsonKey(name: 'Id')
  int get id => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: 'Name')
  String get Name => throw _privateConstructorUsedError;
  @HiveField(2)
  @JsonKey(name: 'Firstname')
  String get FirstName => throw _privateConstructorUsedError;
  @HiveField(3)
  @JsonKey(name: 'Email')
  String get Email => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserModelCopyWith<UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserModelCopyWith<$Res> {
  factory $UserModelCopyWith(UserModel value, $Res Function(UserModel) then) =
      _$UserModelCopyWithImpl<$Res, UserModel>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: 'Id') int id,
      @HiveField(1) @JsonKey(name: 'Name') String Name,
      @HiveField(2) @JsonKey(name: 'Firstname') String FirstName,
      @HiveField(3) @JsonKey(name: 'Email') String Email});
}

/// @nodoc
class _$UserModelCopyWithImpl<$Res, $Val extends UserModel>
    implements $UserModelCopyWith<$Res> {
  _$UserModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Name = null,
    Object? FirstName = null,
    Object? Email = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Name: null == Name
          ? _value.Name
          : Name // ignore: cast_nullable_to_non_nullable
              as String,
      FirstName: null == FirstName
          ? _value.FirstName
          : FirstName // ignore: cast_nullable_to_non_nullable
              as String,
      Email: null == Email
          ? _value.Email
          : Email // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserModelCopyWith<$Res> implements $UserModelCopyWith<$Res> {
  factory _$$_UserModelCopyWith(
          _$_UserModel value, $Res Function(_$_UserModel) then) =
      __$$_UserModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: 'Id') int id,
      @HiveField(1) @JsonKey(name: 'Name') String Name,
      @HiveField(2) @JsonKey(name: 'Firstname') String FirstName,
      @HiveField(3) @JsonKey(name: 'Email') String Email});
}

/// @nodoc
class __$$_UserModelCopyWithImpl<$Res>
    extends _$UserModelCopyWithImpl<$Res, _$_UserModel>
    implements _$$_UserModelCopyWith<$Res> {
  __$$_UserModelCopyWithImpl(
      _$_UserModel _value, $Res Function(_$_UserModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? Name = null,
    Object? FirstName = null,
    Object? Email = null,
  }) {
    return _then(_$_UserModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      Name: null == Name
          ? _value.Name
          : Name // ignore: cast_nullable_to_non_nullable
              as String,
      FirstName: null == FirstName
          ? _value.FirstName
          : FirstName // ignore: cast_nullable_to_non_nullable
              as String,
      Email: null == Email
          ? _value.Email
          : Email // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserModel implements _UserModel {
  const _$_UserModel(
      {@HiveField(0) @JsonKey(name: 'Id') required this.id,
      @HiveField(1) @JsonKey(name: 'Name') required this.Name,
      @HiveField(2) @JsonKey(name: 'Firstname') required this.FirstName,
      @HiveField(3) @JsonKey(name: 'Email') required this.Email});

  factory _$_UserModel.fromJson(Map<String, dynamic> json) =>
      _$$_UserModelFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: 'Id')
  final int id;
  @override
  @HiveField(1)
  @JsonKey(name: 'Name')
  final String Name;
  @override
  @HiveField(2)
  @JsonKey(name: 'Firstname')
  final String FirstName;
  @override
  @HiveField(3)
  @JsonKey(name: 'Email')
  final String Email;

  @override
  String toString() {
    return 'UserModel(id: $id, Name: $Name, FirstName: $FirstName, Email: $Email)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.Name, Name) || other.Name == Name) &&
            (identical(other.FirstName, FirstName) ||
                other.FirstName == FirstName) &&
            (identical(other.Email, Email) || other.Email == Email));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, Name, FirstName, Email);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      __$$_UserModelCopyWithImpl<_$_UserModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserModelToJson(
      this,
    );
  }
}

abstract class _UserModel implements UserModel {
  const factory _UserModel(
      {@HiveField(0)
      @JsonKey(name: 'Id')
          required final int id,
      @HiveField(1)
      @JsonKey(name: 'Name')
          required final String Name,
      @HiveField(2)
      @JsonKey(name: 'Firstname')
          required final String FirstName,
      @HiveField(3)
      @JsonKey(name: 'Email')
          required final String Email}) = _$_UserModel;

  factory _UserModel.fromJson(Map<String, dynamic> json) =
      _$_UserModel.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: 'Id')
  int get id;
  @override
  @HiveField(1)
  @JsonKey(name: 'Name')
  String get Name;
  @override
  @HiveField(2)
  @JsonKey(name: 'Firstname')
  String get FirstName;
  @override
  @HiveField(3)
  @JsonKey(name: 'Email')
  String get Email;
  @override
  @JsonKey(ignore: true)
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}
