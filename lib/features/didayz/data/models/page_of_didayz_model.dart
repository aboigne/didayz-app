import 'package:didayz/features/didayz/domain/entities/page_of_didayz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'didayz_model.dart';

part 'page_of_didayz_model.freezed.dart';
part 'page_of_didayz_model.g.dart';

@freezed
class PageOfDidayzModel with _$PageOfDidayzModel {
  const factory PageOfDidayzModel({
    @JsonKey(name: 'currentPage') required int page,
    @JsonKey(name: 'calendar') required List<DidayzModel> results,
    @JsonKey(name: 'totalPages') required int totalPages,
  }) = _PageOfDidayzModel;

  factory PageOfDidayzModel.fromJson(dynamic json) => _$PageOfDidayzModelFromJson(json);

  factory PageOfDidayzModel.fromDomain(PageOfDidayz entity) {
    return PageOfDidayzModel(
      page: entity.page,
      results: entity.results.map(DidayzModel.fromDomain).toList(),
      totalPages: entity.totalPages,
    );
  }
}

extension PageOfDidayzModelX on PageOfDidayzModel {
  PageOfDidayz toDomain() {
    return PageOfDidayz(
      page: page,
      results: results.map((e) => e.toDomain()).toList(),
      totalPages: totalPages,
    );
  }
}
