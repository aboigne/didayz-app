// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'didayz_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DidayzModelAdapter extends TypeAdapter<_$_DidayzModel> {
  @override
  final int typeId = 0;

  @override
  _$_DidayzModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    UserModel creator = UserModel(
        id: fields[5][0] as int,
        Name: fields[5][1] as String,
        FirstName: fields[5][2] as String,
        Email: fields[5][3] as String
    );

    return _$_DidayzModel(
        id: fields[0] as int,
        Name: fields[1] as String,
        Key: fields[2] as String,
        DateEnd: fields[3] as DateTime,
        NBDays: fields[4] as int,
        IsPublished: fields[6] as bool,
        Progress: fields[7] as double,
        Creator: creator);
  }

  @override
  void write(BinaryWriter writer, DidayzModel obj) {
    writer.writeByte(0);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DidayzModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DidayzModel _$$_DidayzModelFromJson(Map<String, dynamic> json) =>
    _$_DidayzModel(
      id: json['id'] as int,
      Name: json['Name'] as String,
      Key: json['Key'] as String,
      IntroMsg: json['IntroMessage'] as String?,
      Background: json['BackgroundPicture'] as String?,
      DateEnd: DateTime.parse(json['DateEnd'] as String),
      NBDays: json['NBDays'] as int,
      IsPublished: json['IsPublished'] > 0 ? true : false,
      Progress: (json['Progress'] as num?)?.toDouble(),
      Creator: UserModel.fromJson(json['user']),
    );

Map<String, dynamic> _$$_DidayzModelToJson(_$_DidayzModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'Name': instance.Name,
      'Key': instance.Key,
      'IntroMessage': instance.IntroMsg,
      'BackgroundPicture': instance.Background,
      'DateEnd': instance.DateEnd.toIso8601String(),
      'NBDays': instance.NBDays,
      'IsPublished': instance.IsPublished,
      'Progress': instance.Progress,
      'user': instance.Creator,
    };
