import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

import '../../domain/entities/didayz.dart';
import 'user_model.dart';

part 'didayz_model.freezed.dart';
part 'didayz_model.g.dart';

@freezed
@HiveType(typeId: 0)
class DidayzModel with _$DidayzModel {
  const factory DidayzModel({
    @HiveField(0) @JsonKey(name: 'id') required int id,
    @HiveField(1) @JsonKey(name: 'Name') required String Name,
    @HiveField(2) @JsonKey(name: 'Key') required String Key,
    @HiveField(3) @JsonKey(name: 'IntroMessage') String? IntroMsg,
    @HiveField(4) @JsonKey(name: 'BackgroundPicture') String? Background,
    @HiveField(5) @JsonKey(name: 'DateEnd') required DateTime DateEnd,
    @HiveField(6) @JsonKey(name: 'NBDays') required int NBDays,
    @HiveField(7) @JsonKey(name: 'IsPublished') required bool IsPublished,
    @HiveField(8) @JsonKey(name: 'Progress') double? Progress,
    @HiveField(9) @JsonKey(name: 'user') required UserModel Creator,
  }) = _DidayzModel;

  factory DidayzModel.fromJson(dynamic json) => _$DidayzModelFromJson(json);

  factory DidayzModel.fromDomain(DidayzEntity entity) {
    return DidayzModel(
      id: entity.id,
      Name: entity.Name,
      Key: entity.Key,
      IntroMsg: entity.IntroMsg,
      Background: entity.Background,
      DateEnd: entity.DateEnd,
      NBDays: entity.NBDays,
      IsPublished: entity.IsPublished,
      Progress: entity.Progress!,
      Creator: UserModel.fromDomain(entity.Creator),
    );
  }
}

extension DidayzModelX on DidayzModel {
  DidayzEntity toDomain() {
    return DidayzEntity(
      id: id,
      Name: Name,
      Key: Key,
      IntroMsg: IntroMsg,
      Background: Background,
      DateEnd: DateEnd,
      NBDays: NBDays,
      IsPublished: IsPublished,
      Progress: Progress,
      Creator: Creator.toDomain()
    );
  }
}
