part of '../../../di/injection_container.dart';

void _featureDidayz() {
  hive.registerAdapter(DidayzModelAdapter());

  sl
  // DataSources
    ..injectDataSource<DidayzRemoteDataSource>(() => DidayzRemoteDataSourceImpl(dio: dio))

  // Repositories
    ..injectRepository<DidayzRepository>(
          () => DidayzRepositoryImpl(
                  remoteDataSource: sl(),
                  networkInfo: sl(),
                ),
    )

  // UseCases
    ..injectUseCase(() => GetDidayzByCreatorByPageUseCase(sl()))
    ..injectUseCase(() => GetDidayzByPlayerByPageUseCase(sl()))

  // Blocs
    ..injectBloc(() => DidayzCubit(didayzByPlayer: sl()))
    ..injectBloc(() => DidayzCreatorCubit(didayzByCreator: sl()))
    ..injectBloc(() => DidayzCreatorPublishedCubit(didayzByCreator: sl()));

  // Interfaces
}
