class Vectors {
  Vectors._();

  static const String didayz = 'assets/vectors/didayz.svg';
  static const String google_icon = 'assets/vectors/google_icon.svg';
  static const String didayz_icon = 'assets/vectors/didayz_icon.svg';
  static const String home_icon = 'assets/vectors/home_icon.svg';
  static const String text_icon = 'assets/vectors/text_icon.svg';
  static const String photo_icon = 'assets/vectors/photo_icon.svg';
  static const String sound_icon = 'assets/vectors/sound_icon.svg';
  static const String camera_icon = 'assets/vectors/camera_icon.svg';
  static const String document_icon = 'assets/vectors/document_icon.svg';
  static const String file_icon = 'assets/vectors/file_icon.svg';
  static const String user_icon = 'assets/vectors/user_icon.svg';
  static const String pic_icon = 'assets/vectors/pic_icon.svg';
  static const String sound_rec_icon = 'assets/vectors/sound_record_icon.svg';
  static const String upload_file_icon = 'assets/vectors/upload_file_icon.svg';
}
