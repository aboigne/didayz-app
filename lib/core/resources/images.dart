class Images {
  Images._();

  static const didayz = 'assets/images/didayz.png';
  static const didayz_blue = 'assets/images/didayz_blue.png';
  static const ideas_draw = 'assets/images/ideas.png';
  static const no_didayz = 'assets/images/no_didayz.png';
  static const sound = 'assets/images/sound.png';
}
