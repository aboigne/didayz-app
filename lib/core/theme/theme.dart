import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'styles.dart';

ThemeData DidayzTheme() {
  return ThemeData(
    fontFamily: 'Raleway',
    cardColor: AppColors.primaryColor,
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.primaryColor,
      iconTheme: const IconThemeData(
        color: Colors.white,
      ),
      titleTextStyle: AppStyles.defaultTitleStyle.merge(
        TextStyle(
          color: Colors.white,
          fontSize: 14.0,
        ),
      ),
    ),
  );
}
