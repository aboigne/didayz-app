import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppStyles {
  static final inputText = TextStyle(
    color: Colors.black,
    fontFamily: 'Raleway',
  );
  static final defaultTextStyle = TextStyle(
    fontSize: 14,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w600,
    letterSpacing: 0.5,
    height: 1.5,
  );
  static final defaultTitleStyle = TextStyle(
    fontSize: 22,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w800,
    letterSpacing: 0.5,
    height: 1.5,
  );
  static final defaultDescStyle = TextStyle(
    fontSize: 14,
    fontFamily: 'SF-Pro-Text',
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5,
    height: 1.5,
  );
}

class AppGradients {
  static LinearGradient gradient = LinearGradient(
    colors: [Colors.transparent, AppColors.scarlet],
    stops: [0, 1],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}
