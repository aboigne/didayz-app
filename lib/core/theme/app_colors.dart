import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = MaterialColor(
    0xFF4437C1,
    <int, Color>{
      50: Color(0xFF6b60d2),
      100: Color(0xFF9088dd),
      200: Color(0xFFb5afe9),
      300: Color(0xFFdad7f4),
    },
  );

  // https://chir.ag/projects/name-that-color/
  static const Color malibu = Color(0xFF68d7ff);
  static const Color mauve = Color(0xFFCD9CFD);
  static const Color carnationPink = Color(0xFFff8bc7);
  static const Color blazeOrange = Color(0xFFff6d00);
  static const Color scarlet = Color(0xFFff1700);
  static const Color superNova = Color(0xFFffca00);
  static const Color chartreuse = Color(0xFF86ef00);
  static const Color serenade = Color(0xFFfff2e8);
  static const Color grey = Color(0xFF999999);
  static const Color greenWater = Color(0xFF3EB99D);
}
