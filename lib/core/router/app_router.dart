import 'package:flutter/material.dart';

import '../../features/auth/presentation/pages/login_page.dart';
import '../../features/auth/presentation/pages/newpassword_page.dart';
import '../../features/auth/presentation/pages/register_page.dart';
import '../../features/didayz_detail/presentation/pages/didayz_detail_page.dart';
import '../../features/splash/presentation/pages/splashscreen_page.dart';
import '../../features/tutorial/presentation/pages/starter_tutorial.dart';
import '../constants/route_list.dart';
import '../presentation/pages/main_page.dart';

class AppRouter {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  String get initialRoute => RouteList.splash;

  List<Route> onGenerateInitialRoutes(String initialRoute) {
    return [getPageRoute(initialRoute, null)];
  }

  Route onGenerateRoute(RouteSettings routeSettings) {
    final String routeName = routeSettings.name ?? '';
    return getPageRoute(routeName, routeSettings);
  }

  Route getPageRoute(String routeName, RouteSettings? routeSettings) {
    switch (routeName) {
      case RouteList.splash:
        return MaterialPageRoute(builder: (_) => const SplashScreenPage());
      case RouteList.login:
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case RouteList.register:
        return MaterialPageRoute(builder: (_) => const RegisterPage());
      case RouteList.forgotpwd:
        return MaterialPageRoute(builder: (_) => const newPasswordPage());
      case RouteList.creation:
        return MaterialPageRoute(builder: (_) => const MainPage());
      case RouteList.didayz:
        return MaterialPageRoute(builder: (_) => const MainPage());
      case RouteList.create:
        return MaterialPageRoute(builder: (_) => const MainPage());
      case RouteList.profile:
        return MaterialPageRoute(builder: (_) => const MainPage());
      case RouteList.tutorial:
        return MaterialPageRoute(builder: (_) => StartTutorialPage());
      case RouteList.crt_didayz:
        return MaterialPageRoute(
          settings: routeSettings,
          builder: (_) => ExtractArgumentsScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for $routeName'),
            ),
          ),
        );
    }
  }
}
