class RouteList {
  RouteList._();

  static const String splash = '/splash';
  static const String login = '/login';
  static const String register = '/register';
  static const String forgotpwd = '/newpassword';
  static const String creation = '/creation';
  static const String didayz = '/didayz';
  static const String create = '/create';
  static const String profile = '/account';
  static const String tutorial = '/tutorial';
  static const String statistics = '/statistics';
  static const String crt_didayz = '/crt_didayz';
}
