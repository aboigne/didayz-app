class ApiConstants {
  static const String signin = '/auth/signin';
  static const String signup = '/auth/signup';
  static const String rstPassword = '/auth/resetPassword';
  static const String verifytoken = '/auth/verifytoken';
  static const String verify_crt_tkn = '/test/user';
  static const String change_user = '/user/changeUser';
  static const String change_user_pwd = '/user/changePassword';
  static const String delete_user = '/auth/deleteaccount';

  static String calendarByKey(String key) => '/calendar/byKey?key=$key';
  static String calendarsByPlayer(int page, int limit) => '/calendar/byPlayer/?page=$page&limit=$limit';
  static String calendarsByUser(int page, int limit, int is_publish) => '/calendar/byUser/?page=$page&limit=$limit&is_publish=$is_publish';
  static const String calendarsPublics = '/calendar/public';
  static String calendarsInfoByUser(int page, int limit) => '/calendar/infosByUser/?page=$page&limit=$limit';
  static const String create = '/calendarUser/create';
  static const String create_didayz = '/calendar/newCalendar';
  static const String delete = '/calendarUser/delete';
  static String getDays(String id) => '/calendar/days?calId=$id';
  static String getDay(String cal_id, String id) => '/calendar/day?id=$id&calId=$cal_id';
  static const String change_order_days = '/calendar/changeOrderDays';
  static const String change_didayz_name = '/calendar/changeCalendarName';
  static const String change_didayz_key = '/calendar/changeCalendarKey';
  static const String change_didayz_intro_msg = '/calendar/changeCalendarIntroMsg';
  static const String change_didayz_pic = '/calendar/changeCalendarPicture';
  static const String change_didayz_add_days = '/calendar/addDays';
  static const String change_didayz_rmv_days = '/calendar/removeDays';
  static const String change_didayz_date_end = '/calendar/changeDtEnd';
  static const String publish_didayz = '/calendar/publish';

  static const String change_day_intro_msg = '/calendar/updateDayIntroMsg';
  static const String change_day_code = '/calendar/updateDayCode';
  static const String change_day_send_rep = '/calendar/updateDaySendRep';
  static const String change_day_type = '/calendar/updateDayType';
  static const String change_day_texte = '/calendar/updateDayTexte';
  static const String change_day_pic = '/calendar/updateDayPic';
  static const String change_day_video_type = '/calendar/updateDayVideoType';
  static const String change_day_vid = '/calendar/updateDayVid';
  static const String change_day_audio_type = '/calendar/updateDayAudioType';
  static const String change_day_aud = '/calendar/updateDayAud';
  static const String change_day_file = '/calendar/updateDayFile';

  static const String send_reponse_day = '/sendResponse';


  static String getFile(int id, String file_name) => '/file/files/$id/$file_name';
  static const String upload_file = '/file/upload';
  static const String deleteFile = '/file/delete';

  static const String send_msg = '/sendMsg';

  static const String information_msg = '/currentinformations';
}
