part of 'navigation_cubit.dart';

enum NavbarItem { home, didayz }

extension NavbarItemX on NavbarItem {
  int get itemIndex {
    switch (this) {
      case NavbarItem.home:
        return 0;
      case NavbarItem.didayz:
        return 1;
    }
  }
}

@freezed
class NavigationState with _$NavigationState {
  const NavigationState._();

  const factory NavigationState(NavbarItem navbarItem) = _NavigationState;

  int get index => navbarItem.itemIndex;
}
