import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../features/create/presentation/pages/create_page.dart';
import '../../../features/home/presentation/pages/home.dart';
import '../../../features/my_didayz/presentation/pages/my_didayz.dart';
import '../../extension/context.dart';
import '../../theme/app_colors.dart';
import '../../theme/styles.dart';
import '../blocs/navigation/navigation_cubit.dart';
import '../widgets/add_didayz_alert.dart';
import '../widgets/bottom_navigation_bar_widget.dart';
import '../widgets/route_transition.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  OverlayEntry? overlayEntry;
  AnimationController? animationController;
  Animation<double>? animation;
  bool showOverlay = false;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(vsync: this, duration: const Duration(seconds: 1));
    animation = CurveTween(curve: Curves.easeInOut).animate(animationController!);
  }

  _showOverLay() async {
    setState(() {
      showOverlay = !showOverlay;
    });

    final List icons = [Icons.edit_rounded, Icons.card_giftcard_rounded];
    final List labels = [context.translate().create_new_didayz, context.translate().add_given_didayz];

    final OverlayState? overlayState = Overlay.of(context);
    overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        left: 0,
        right: 0,
        bottom: 100,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ...[
              for (int i = 0; i < icons.length; i++)
                ScaleTransition(
                  scale: animation!,
                  child: FloatingActionButton.extended(
                    backgroundColor: AppColors.primaryColor,
                    heroTag: icons[i],
                    label: Text(
                      labels[i],
                      style: AppStyles.defaultTextStyle.merge(
                        TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    icon: Icon(
                      icons[i],
                      color: Colors.white,
                    ),
                    onPressed: () {
                      this.setState(() {
                        showOverlay = false;
                      });

                      animationController!.reverse()
                          .whenComplete(() {
                        overlayEntry!.remove();
                        if(i == 0)
                        {
                          Navigator.of(context).push(createRoute(CreatePage()));
                        }
                        else if(i == 1)
                        {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return AddDidayzAlert();
                            },
                          );
                        }
                      });
                    },
                  ),
                ),
            ].expand(
                  (widget) =>
              [
                widget,
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
    animationController!.addListener(() {
      overlayState!.setState(() {});
    });

    if(showOverlay)
    {
      animationController!.forward();
    }
    else
    {
      animationController!.reverse().whenComplete(() => overlayEntry!.remove());
    }

    overlayState!.insert(overlayEntry!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavigationBarWidget(),
      body: BlocBuilder<NavigationCubit, NavigationState> (
        builder: (context, state) {
          switch (state.navbarItem) {
            case NavbarItem.home:
              return const HomePage();
              case NavbarItem.didayz:
                return const MyDidayzPage();
              default:
                return SizedBox.shrink();
          }
        },
      ),
      resizeToAvoidBottomInset: false,
      floatingActionButton: Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 6.0),
          child: FloatingActionButton(
            onPressed: _showOverLay,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: AppColors.primaryColor,
                    spreadRadius: 1.10,
                    blurRadius: 14.35,
                    offset: Offset(0, 7.73),
                  ),
                ],
              ),
              child: showOverlay ? Icon(Icons.close_rounded, color: Colors.white,) : Icon(Icons.add_rounded, color: Colors.white,),
            ),
            backgroundColor: AppColors.primaryColor,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,
    );
  }
}
