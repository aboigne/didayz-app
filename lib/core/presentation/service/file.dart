import 'dart:convert';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

import '../../../../core/constants/api_constants.dart';

class FileService {
  final String domain = GlobalConfiguration().getValue('api_base_url');
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Future<int> upload_file(XFile entity_file, String suffix, int didayz_id, String fromCamera) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final req = http.MultipartRequest('POST', Uri.parse(domain + ApiConstants.upload_file));

      final Map<String, String> headers = {'x-access-token': crtToken!};
      req.headers.addAll(headers);

      req.files.add(
        await http.MultipartFile.fromPath(
          'file',
          entity_file.path,
        ),
      );

      req.fields['name'] = '$didayz_id' + suffix;
      req.fields['id'] = '$didayz_id';
      req.fields['fromCamera'] = fromCamera;
      req.fields['day_id'] = '0';

      final result = await req.send();

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> upload_file_day(File entity_file, String suffix, int didayz_id, int day_id, String fromCamera) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final req = http.MultipartRequest('POST', Uri.parse(domain + ApiConstants.upload_file));

      final Map<String, String> headers = {'x-access-token': crtToken!};
      req.headers.addAll(headers);

      req.files.add(
        await http.MultipartFile.fromPath(
          'file',
          entity_file.path,
        ),
      );

      req.fields['name'] = '$day_id' + suffix;
      req.fields['id'] = '$didayz_id';
      req.fields['fromCamera'] = fromCamera;
      req.fields['day_id'] = '$day_id';

      final result = await req.send();

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }

    return error_code;
  }

  Future<int> delete_file(String repository, String name, int didayz_id) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'name': repository + '/' + name, 'id': didayz_id});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.deleteFile),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }

    return error_code;
  }

}
