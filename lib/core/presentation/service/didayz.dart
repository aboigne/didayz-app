import 'dart:convert';
import 'dart:math';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

import '../../../../core/constants/api_constants.dart';

class ResponseDidayz {
  int error_code;
  List<dynamic>? didayz;

  ResponseDidayz(this.error_code, {this.didayz});
}

class DidayzService {
  final String domain = GlobalConfiguration().getValue('api_base_url');
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  Future<ResponseDidayz> getPublicDidayz() async {
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final result = await http.get(
        Uri.parse(domain + ApiConstants.calendarsPublics),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
      );

      final ResponseDidayz res_didayz = new ResponseDidayz(
        result.statusCode,
        didayz: json.decode(result.body)['calendar'],
      );

      return res_didayz;
    } on Exception catch (e) {
      final ResponseDidayz res_didayz = new ResponseDidayz(500,);
      return res_didayz;
    }
  }

  Future<List<dynamic>> add_didayz(String key, String notif_title, String notif_body, String notif_summary) async {
    List<dynamic> res_didayz = [];
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final result = await http.get(
        Uri.parse(domain + ApiConstants.calendarByKey(key)),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
      );

      if(result.statusCode == 201)
      {
        AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
          if (!isAllowed) {
            AwesomeNotifications().requestPermissionToSendNotifications();
          }
        });

        final Random random = new Random();

        for(int i = json.decode(result.body)['NBDays'] - 1; i >= 0; i--)
        {
          final DateTime dtDay = DateTime.parse(json.decode(result.body)['DateEnd']).subtract(Duration(days: i));
          AwesomeNotifications().createNotification(
            content: NotificationContent(
              id: random.nextInt(10000),
              channelKey: 'basic_channel',
              title: notif_title,
              body: notif_body,
              summary: notif_summary,
              actionType: ActionType.Default,
              wakeUpScreen: true,
              payload: {
                'didayz_id': json.decode(result.body)['id'].toString(),
              },
            ),
            schedule: NotificationCalendar.fromDate(
              date: dtDay,
            ),
          );
        }
        res_didayz = [result.statusCode, json.decode(result.body)['Name'], json.decode(result.body)['IntroMessage']];
      }
      else
      {
        res_didayz = [result.statusCode];
      }
    } on Exception catch (e) {
      res_didayz = [500];
    }
    return res_didayz;
  }

  Future<Map<String, int>> create_didayz(String name, String key, String dtEnd, int nbDays, String? msgIntro, String? picture) async {
    final response = { 'statusCode': 0, 'cal_id': 0 };
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'name': name, 'key': key, 'dateEnd': dtEnd, 'nbDays': nbDays, 'msgIntro': msgIntro, 'picture': picture});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.create_didayz),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      response['statusCode'] = result.statusCode;
      if(result.statusCode == 200)
      {
        response['cal_id'] = jsonDecode(result.body)['id'];
      }
    } on Exception catch (e) {
      response['statusCode'] = 500;
    }
    return response;
  }

  Future<int> update_didayz_name(int calId, String name) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'name': name});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_name),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> update_didayz_intro_msg(int calId, String? intro_msg) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'introMsg': intro_msg});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_intro_msg),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> update_didayz_key(int calId, String key) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'key': key});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_key),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> update_didayz_pic(int calId, String? picture) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'picture': picture});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_pic),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> update_didayz_add_days(int calId, int days_to_add) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'nbDays': days_to_add});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_add_days),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> update_didayz_rmv_day(int calId, int dayId, String dtDay) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'day_id': dayId, 'date': dtDay, });
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_rmv_days),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> update_didayz_date_end(int calId, String date_end) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'dateEnd': date_end});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.change_didayz_date_end),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }

  Future<int> publish_didayz(int calId) async {
    var error_code = 0;
    try {
      final key_encrypted = await secureStorage.read(
        key: 'encryptionKey',
      );
      final encryptionKey = base64Url.decode(
        key_encrypted!,
      );
      final boxUser = await Hive.openBox(
        'user',
        encryptionCipher: HiveAesCipher(encryptionKey),
      );
      final crtToken = boxUser.get('token', defaultValue: false);

      final body = jsonEncode({'id': calId, 'nb_players': 100});
      final result = await http.post(
        Uri.parse(domain + ApiConstants.publish_didayz),
        headers: {'Content-Type': 'application/json', 'x-access-token': crtToken!},
        body: body,
      );

      error_code = result.statusCode;
    } on Exception catch (e) {
      error_code = 500;
    }
    return error_code;
  }
}
