import 'package:didayz/core/presentation/service/didayz.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';

import '../../extension/context.dart';

class ConfirmationAlert extends StatelessWidget {
  ConfirmationAlert({required this.message, required this.calId, this.dayId, this.isAdded = true, this.dt_to_del, super.key,});

  final String message;
  final int calId;
  int? dayId;
  String? dt_to_del;
  final bool isAdded;

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              ctx.translate().confirmation_alert,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20.0,),
            Text(
              message,
              style: AppStyles.defaultTextStyle.merge(
                TextStyle(
                  color: Color(0xFF999999),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      ctx.translate().cancel,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () async {
                      int? result_days;
                      if(isAdded) {
                        result_days = await DidayzService().update_didayz_add_days(calId, 1);
                      }
                      else if(!isAdded && dayId != null && dt_to_del != null){
                        result_days = await DidayzService().update_didayz_rmv_day(calId, dayId!, dt_to_del!);
                      }

                      if(result_days != null && result_days != 200)
                      {
                        SnackBar snackBar = SnackBar(
                          content: Text(
                            isAdded ? ctx.translate().error_adding_day : ctx.translate().error_deleting_day,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.blazeOrange,
                        );

                        ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                      }
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      isAdded ? ctx.translate().add : ctx.translate().delete,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
