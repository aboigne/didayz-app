import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/blocs/navigation/navigation_cubit.dart';
import 'package:didayz/core/presentation/service/didayz.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/didayz/presentation/blocs/didayz/didayz_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../extension/context.dart';

class AddDidayzAlert extends StatefulWidget {
  AddDidayzAlert({super.key,});

  @override
  State<AddDidayzAlert> createState() => _AddDidayzAlertState();
}

class _AddDidayzAlertState extends State<AddDidayzAlert> {
  String code_cal = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              ctx.translate().enter_didayz_key,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            TextField(
              scrollPadding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom + 64,
              ),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              style: AppStyles.inputText.merge(
                TextStyle(
                  fontSize: 14,
                ),
              ),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15.0),
                filled: true,
                fillColor: Colors.white,
                hintText: ctx.translate().key_didayz,
                hintStyle: AppStyles.inputText.merge(
                  TextStyle(
                    color: Color(0xFF999999),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                  borderRadius: BorderRadius.circular(10),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              cursorColor: Colors.black,
              cursorHeight: 25.0,
              onChanged: (value) {
                code_cal = value;
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      ctx.translate().cancel,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () async {
                      final FocusScopeNode currentFocus = FocusScope.of(context);

                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }

                      final network_inf = new NetworkInfoImpl();
                      late SnackBar snackBar;

                      if(!(await network_inf.isConnected))
                      {
                        snackBar = SnackBar(
                          content: Text(
                            context.translate().no_internet_connection,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.blazeOrange,
                        );
                      }
                      else
                      {
                        final result_didayz_added = await DidayzService().add_didayz(code_cal, context.translate().didayz, context.translate().notif_body, context.translate().notif_summary);
                        switch(result_didayz_added[0]) {
                          case 201:
                            snackBar = SnackBar(
                              content: Text(
                                ctx.translate().didayz_added_success,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.primaryColor,
                            );

                            if(result_didayz_added.length == 3)
                            {
                              await showDialog(
                                context: context,
                                builder: (context) {
                                  return Dialog(
                                    insetPadding: EdgeInsets.all(20.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.white,
                                      ),
                                      padding: EdgeInsets.all(20.0),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Flexible(
                                            child: Text(
                                              result_didayz_added[1],
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: AppColors.primaryColor,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Flexible(
                                            child: Padding(
                                              padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                                              child: Text(
                                                result_didayz_added[2],
                                                style: AppStyles.defaultTextStyle.merge(
                                                  TextStyle(
                                                    color: Color(0xFF999999),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 20.0,),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: AppColors.primaryColor,
                                              foregroundColor: Colors.white,
                                              minimumSize: Size.fromHeight(60),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text(
                                              context.translate().ok,
                                              style: AppStyles.defaultTitleStyle.merge(
                                                TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              );
                            }

                            ctx.read<DidayzCubit>().getDidayzByPlayer(1, 10);
                            ctx.read<NavigationCubit>().getNavBarItemFromIndex(1);
                            break;
                          case 204:
                            snackBar = SnackBar(
                              content: Text(
                                ctx.translate().error_key_not_exist,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                            break;
                          case 403:
                            snackBar = SnackBar(
                              content: Text(
                                ctx.translate().error_didayz_not_published,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                            break;
                          case 200:
                            snackBar = SnackBar(
                              content: Text(
                                ctx.translate().error_key_added,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                            break;
                          default:
                            snackBar = SnackBar(
                              content: Text(
                                ctx.translate().error_add_didayz,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                        }
                      }

                      Navigator.of(ctx).pop();
                      ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                    },
                    child: Text(
                      ctx.translate().add,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
