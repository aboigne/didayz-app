import 'dart:ui';
import 'dart:math' as math;

import '../../constants/api_constants.dart';
import 'route_transition.dart';
import '../../resources/vectors.dart';
import '../../theme/app_colors.dart';
import '../../theme/styles.dart';
import '../../../features/didayz_day_detail/presentation/pages/didayz_day_detail_page.dart';
import '../../../features/didayz_day_detail/presentation/pages/didayz_day_detail_ro_page.dart';
import '../../../features/didayz_detail/presentation/blocs/didayz_detail_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../features/didayz/domain/entities/didayz_day.dart';
import '../../extension/context.dart';
import '../../resources/images.dart';
import '../service/didayz_states.dart';
import 'confirmation_alert.dart';
import 'loading_widget.dart';

class CardDay extends StatefulWidget {
  final DidayzDayEntity? didayz_day;
  final int didayz_id;
  final States_Didayz? state;
  final bool add_day;
  final refreshKey;
  const CardDay(this.didayz_id, {Key? key, this.didayz_day = null, this.state = null, this.add_day = false, this.refreshKey = null}) : super(key: key);

  @override
  _CardDayState createState() => _CardDayState();
}

class _CardDayState extends State<CardDay > {
  _CardDayState();  //constructor
  final String _base_url = GlobalConfiguration().getValue('api_base_url');
  final formatter = new DateFormat('dd/MM/yyyy');
  ImageProvider? videoThumbnail;

  @override
  void initState() {
    super.initState();
    if(widget.didayz_day != null) getVideoThumbnail();
  }

  bool _is_available(int number, int month, int year) {
    final DateTime date_to_check = DateTime.parse(year.toString() + month.toString().padLeft(2, '0') + number.toString().padLeft(2, '0'));
    final DateTime crt_date = DateTime.now();
    if(date_to_check.compareTo(crt_date) < 0) return true;
    return false;
  }

  String _display_date(int number, int month, int year) {
    final DateTime date_to_display = DateTime.parse(year.toString() + month.toString().padLeft(2, '0') + number.toString().padLeft(2, '0'));
    return DateFormat.MMMMd('fr').format(date_to_display);
  }

  getVideoThumbnail() async {
    if(widget.didayz_day!.Type == 'Video' && (widget.didayz_day!.VideoType == 'File' || widget.didayz_day!.VideoType == null) && widget.didayz_day!.Video != null)
    {
      final thumbnailAsUint8List = await VideoThumbnail.thumbnailData(
        video: _base_url + ApiConstants.getFile(widget.didayz_id, widget.didayz_day!.Video!),
        imageFormat: ImageFormat.JPEG,
        maxWidth: 320,
        quality: 50,
      );
      setState(() {
        videoThumbnail = MemoryImage(thumbnailAsUint8List!);
      });
    }
  }

  Widget _icon(Color color, String SvgFile) {
    return SvgPicture.asset(
      SvgFile,
      color: color,
      height: 20,
      width: 20,
      fit: BoxFit.scaleDown,
    );
  }

  @override
  Widget build(BuildContext context) {
    if(widget.add_day)
    {
      return GestureDetector(
        key: UniqueKey(),
        onTap: () async {
          await showDialog(
            context: context,
            builder: (context) {
              return ConfirmationAlert(
                message: context.translate().didayz_update_one_day_added,
                calId: widget.didayz_id,
                isAdded: true,
              );
            },
          );
          context.read<DidayzDetailCubit>().getDidayzDetail(widget.didayz_id);
        },
        child: Container(
          key: UniqueKey(),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(18.0),),
            color: AppColors.primaryColor,
          ),
          child: Align(
            alignment: Alignment.center,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: Container(
                  padding: const EdgeInsets.all(12.0),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.4),
                    borderRadius: BorderRadius.all(Radius.circular(50.0),),
                  ),
                  child: Icon(
                    Icons.add_rounded,
                    color: Colors.white,
                    size: 25,
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }

    return GestureDetector(
      key: Key(widget.didayz_day!.id.toString()),
      onTap: (){
        if(widget.state == States_Didayz.draft)
        {
          showModalBottomSheet<void>(
            context: context,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(25.0),
              ),
            ),
            builder: (context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Container(
                      width: 0.4 * MediaQuery.of(context).size.width,
                      height: 5.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFFEFEFF4),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      context.translate().import_content,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          await Navigator.of(context).push(createRoute(DidayzDayDetailPage(widget.didayz_id, widget.didayz_day!.id, content_type: 'Texte',)));
                          if(widget.refreshKey != null) {
                            widget.refreshKey!.currentState!.show();
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundColor: AppColors.primaryColor.withOpacity(0.1),
                              child: _icon(
                                AppColors.primaryColor,
                                Vectors.text_icon,
                              ),
                            ),
                            SizedBox(width: 20.0,),
                            Text(
                              context.translate().text_content(((widget.didayz_day!.Type == 'Texte') && widget.didayz_day!.Texte != null && widget.didayz_day!.Texte != '') ? 1 : 2),
                              style: AppStyles.defaultTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          await Navigator.of(context).push(createRoute(DidayzDayDetailPage(widget.didayz_id, widget.didayz_day!.id, content_type: 'Picture',)));
                          if(widget.refreshKey != null) {
                            widget.refreshKey!.currentState!.show();
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundColor: AppColors.primaryColor.withOpacity(0.1),
                              child: _icon(
                                AppColors.primaryColor,
                                Vectors.photo_icon,
                              ),
                            ),
                            SizedBox(width: 20.0,),
                            Text(
                              context.translate().picture_content(widget.didayz_day!.Type == 'Picture' ? 1 : 2),
                              style: AppStyles.defaultTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          await Navigator.of(context).push(createRoute(DidayzDayDetailPage(widget.didayz_id, widget.didayz_day!.id, content_type: 'Sound',)));
                          if(widget.refreshKey != null) {
                            widget.refreshKey!.currentState!.show();
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundColor: AppColors.primaryColor.withOpacity(0.1),
                              child: _icon(
                                AppColors.primaryColor,
                                Vectors.sound_icon,
                              ),
                            ),
                            SizedBox(width: 20.0,),
                            Text(
                              context.translate().sound_content(widget.didayz_day!.Type == 'Sound' ? 1 : 2),
                              style: AppStyles.defaultTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          await Navigator.of(context).push(createRoute(DidayzDayDetailPage(widget.didayz_id, widget.didayz_day!.id, content_type: 'Video',)));
                          if(widget.refreshKey != null) {
                            widget.refreshKey!.currentState!.show();
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundColor: AppColors.primaryColor.withOpacity(0.1),
                              child: _icon(
                                AppColors.primaryColor,
                                Vectors.camera_icon,
                              ),
                            ),
                            SizedBox(width: 20.0,),
                            Text(
                              context.translate().video_content(widget.didayz_day!.Type == 'Video' ? 1 : 2),
                              style: AppStyles.defaultTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          await Navigator.of(context).push(createRoute(DidayzDayDetailPage(widget.didayz_id, widget.didayz_day!.id, content_type: 'File',)));
                          if(widget.refreshKey != null) {
                            widget.refreshKey!.currentState!.show();
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundColor: AppColors.primaryColor.withOpacity(0.1),
                              child: _icon(
                                AppColors.primaryColor,
                                Vectors.document_icon,
                              ),
                            ),
                            SizedBox(width: 20.0,),
                            Text(
                              context.translate().file_content(widget.didayz_day!.Type == 'File' ? 1 : 2),
                              style: AppStyles.defaultTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        }
        else if(widget.state == States_Didayz.published || _is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year))
        {
          Navigator.of(context).push(createRoute(DidayzDayDetailReadOnlyPage(widget.didayz_id, widget.didayz_day!.id, isVideoYoutube: widget.didayz_day!.Type != 'Video' ? null : widget.didayz_day!.VideoType == 'Youtube' ? true : false, video: widget.didayz_day!.Type == 'Video' ? widget.didayz_day!.Video : null,)));
        }
      },
      child: Stack(
        children: [
          if(((([States_Didayz.received, States_Didayz.demo].contains(widget.state) && _is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year) && (widget.didayz_day!.Code == null || widget.didayz_day!.Code == '')) || !([States_Didayz.received, States_Didayz.demo].contains(widget.state))) && ['Picture', 'Video'].contains(widget.didayz_day!.Type))
              ||
              (([States_Didayz.received, States_Didayz.demo].contains(widget.state) && (!_is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year) || (widget.didayz_day!.Code != null && widget.didayz_day!.Code != ''))) || (widget.didayz_day!.Type == 'Texte' && widget.didayz_day!.Texte != null && widget.didayz_day!.Texte != '') || (widget.didayz_day!.Type == 'Texte' && (widget.didayz_day!.Texte == null || widget.didayz_day!.Texte == '')) || widget.didayz_day!.Type == 'Sound' || widget.didayz_day!.Type == 'File'))
          Stack(
              children: [
                const Center(child: LoadingWidget()),
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(18.0),),
                    child: !((([States_Didayz.received, States_Didayz.demo].contains(widget.state) && _is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year) && (widget.didayz_day!.Code == null || widget.didayz_day!.Code == '')) || !([States_Didayz.received, States_Didayz.demo].contains(widget.state))) && ['Picture', 'Video'].contains(widget.didayz_day!.Type)) ? Container(
                      color: ([States_Didayz.received, States_Didayz.demo].contains(widget.state) && !_is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year)) ? AppColors.primaryColor : ([States_Didayz.received, States_Didayz.demo].contains(widget.state) && widget.didayz_day!.Code != null && widget.didayz_day!.Code != '') ? AppColors.carnationPink : (widget.didayz_day!.Type == 'Texte' && widget.didayz_day!.Texte != null && widget.didayz_day!.Texte != '') ? AppColors.mauve : (widget.didayz_day!.Type == 'Texte' && (widget.didayz_day!.Texte == null || widget.didayz_day!.Texte == '')) ? AppColors.blazeOrange : widget.didayz_day!.Type == 'Sound' ? AppColors.malibu : widget.didayz_day!.Type == 'File' ? AppColors.superNova : null,
                      child: SizedBox(
                          height: double.infinity,
                          width: double.infinity,
                      ),
                    ) : ((widget.didayz_day!.Type == 'Picture' && widget.didayz_day!.Picture != null) || (widget.didayz_day!.Type == 'Video' && widget.didayz_day!.VideoType == 'Youtube' && widget.didayz_day!.Video != null && (YoutubePlayer.convertUrlToId(widget.didayz_day!.Video!,) ?? null) != null)) ? FadeInImage.memoryNetwork(
                      width: double.infinity,
                      fit: BoxFit.cover,
                      placeholder: kTransparentImage,
                      image: (widget.didayz_day!.Type == 'Picture' && widget.didayz_day!.Picture != null) ? _base_url + ApiConstants.getFile(widget.didayz_id, widget.didayz_day!.Picture!) :
                      'https://img.youtube.com/vi/' + widget.didayz_day!.Video! + '/0.jpg',
                    ) : (widget.didayz_day!.Type == 'Video' && (widget.didayz_day!.VideoType == 'File' || widget.didayz_day!.VideoType == null) && widget.didayz_day!.Video != null && videoThumbnail != null) ? Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: videoThumbnail!,
                            fit: BoxFit.cover,
                          ),
                      ),
                    ) : Container(
                      color: Colors.white,
                      child: Image.asset(
                        Images.didayz_blue,
                        key: UniqueKey(),
                        cacheWidth: 400,
                      ),
                    ),
                  ),
                ),
              ],
          ),
          [States_Didayz.received, States_Didayz.demo].contains(widget.state) && (!_is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year) || (widget.didayz_day!.Code != null && widget.didayz_day!.Code != '')) ?
          Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50.0),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                    child: Container(
                      padding: const EdgeInsets.all(7.0),
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.4),
                        borderRadius: BorderRadius.all(Radius.circular(50.0),),
                      ),
                      child: Icon(
                        !_is_available(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year) ? Icons.lock_clock_rounded : Icons.lock_rounded,
                        color: Colors.white,
                        size: 20,
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(36.0),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.4),
                          borderRadius: BorderRadius.all(Radius.circular(36.0),),
                        ),
                        child: Text(
                          _display_date(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year),
                          style: AppStyles.defaultTitleStyle.merge(
                            TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ) :
          Stack(
            children: [
              if(widget.didayz_day!.Type != 'Picture') Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if(widget.didayz_day!.Type == 'Sound') SizedBox(height: 12,),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                        child: Container(
                          padding: const EdgeInsets.all(15.0),
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.4),
                            borderRadius: BorderRadius.all(Radius.circular(50.0),),
                          ),
                          child: Icon(
                            ['Video', 'Sound'].contains(widget.didayz_day!.Type) ? Icons.play_arrow_rounded : widget.didayz_day!.Type == 'File' ? Icons.article_outlined : (widget.didayz_day!.Texte != null && widget.didayz_day!.Texte != '') ? Icons.chat_outlined : Icons.sentiment_dissatisfied_rounded,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                      ),
                    ),
                    if(widget.didayz_day!.Type == 'Sound') SizedBox(
                      height: 10,
                    ),
                    if(widget.didayz_day!.Type == 'Sound') Image.asset(
                      Images.sound,
                      height: 22,
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(36.0),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.4),
                          borderRadius: BorderRadius.all(Radius.circular(36.0),),
                        ),
                        child: Text(
                          _display_date(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year),
                          style: AppStyles.defaultTitleStyle.merge(
                            TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              if((widget.didayz_day!.Code != null && widget.didayz_day!.Code != '') || widget.didayz_day!.SendResponse || widget.state == States_Didayz.draft) Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: ((widget.didayz_day!.Code != null && widget.didayz_day!.Code != '') || widget.didayz_day!.SendResponse) ? MainAxisAlignment.spaceBetween : MainAxisAlignment.end,
                    children: [
                      if((widget.didayz_day!.Code != null && widget.didayz_day!.Code != '') || widget.didayz_day!.SendResponse) Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ...[
                            if(widget.didayz_day!.Code != null && widget.didayz_day!.Code != '') ClipRRect(
                              borderRadius: BorderRadius.circular(50.0),
                              child: BackdropFilter(
                                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                child: Container(
                                  padding: const EdgeInsets.all(7.0),
                                  decoration: BoxDecoration(
                                    color: Colors.white.withOpacity(0.4),
                                    borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                  ),
                                  child: Icon(
                                    Icons.lock_outlined,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                ),
                              ),
                            ),
                            if(widget.didayz_day!.SendResponse) ClipRRect(
                              borderRadius: BorderRadius.circular(50.0),
                              child: BackdropFilter(
                                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                child: Container(
                                  padding: const EdgeInsets.all(7.0),
                                  decoration: BoxDecoration(
                                    color: Colors.white.withOpacity(0.4),
                                    borderRadius: BorderRadius.all(Radius.circular(50.0),),
                                  ),
                                  child: Transform.rotate(
                                    angle: -35 * math.pi / 180,
                                    child: Icon(
                                      Icons.send_outlined,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ].expand(
                                (widget) => [
                              widget,
                              const SizedBox(
                                width: 6,
                              ),
                            ],
                          ),
                        ],
                      ),
                      if(widget.state == States_Didayz.draft) GestureDetector(
                        onTap: () async {
                          await showDialog(
                            context: context,
                            builder: (context) {
                              return ConfirmationAlert(
                                message: context.translate().confirm_deleting(_display_date(widget.didayz_day!.Number, widget.didayz_day!.Month, widget.didayz_day!.Year)),
                                calId: widget.didayz_id,
                                isAdded: false,
                                dayId: widget.didayz_day!.id,
                                dt_to_del: widget.didayz_day!.Year.toString() + '-' + widget.didayz_day!.Month.toString() + '-' + widget.didayz_day!.Number.toString(),
                              );
                            },
                          );
                          context.read<DidayzDetailCubit>().getDidayzDetail(widget.didayz_id);
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50.0),
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                            child: Container(
                              padding: const EdgeInsets.all(7.0),
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.4),
                                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                              ),
                              child: Icon(
                                Icons.remove_rounded,
                                color: Colors.white,
                                size: 20,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
