import 'package:didayz/core/resources/images.dart';
import 'package:flutter/material.dart';

class DefaultLogoHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 100.0, bottom: 30.0),
      child: Image.asset(
        Images.didayz,
        width: 200,
      ),
    );
  }
}
