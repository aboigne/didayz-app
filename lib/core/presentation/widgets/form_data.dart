class FormData {
  String? email;
  String? password;
  String? name;
  String? firstname;
  String? confirmpassword;

  void setEmail(String value) {
    this.email = value;
  }

  void setPassword(String value) {
    this.password = value;
  }

  void setName(String value) {
    this.name = value;
  }

  void setFirstName(String value) {
    this.firstname = value;
  }

  void setConfirmPassword(String value) {
    this.confirmpassword = value;
  }
}

class TypeFormInput {
  String? type;
  String? text;
  TypeFormInput(this.type, this.text);
}
