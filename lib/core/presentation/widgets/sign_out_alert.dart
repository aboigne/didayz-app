import 'dart:convert';

import 'package:didayz/core/presentation/blocs/navigation/navigation_cubit.dart';
import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/auth/presentation/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';

import '../../extension/context.dart';

class SignOutAlert extends StatelessWidget {
  SignOutAlert({super.key,});

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              ctx.translate().log_out,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              ctx.translate().sign_out_validation,
              style: AppStyles.defaultTextStyle.merge(
                TextStyle(
                  color: Color(0xFF999999),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      ctx.translate().cancel,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () async {
                      final key_encrypted = await secureStorage.read(
                        key: 'encryptionKey',
                      );

                      final encryptionKey = base64Url.decode(
                        key_encrypted!,
                      );
                      final boxUser = await Hive.openBox(
                        'user',
                        encryptionCipher: HiveAesCipher(encryptionKey),
                      );
                      boxUser.delete('token');

                      ctx.read<NavigationCubit>().getNavBarItemFromIndex(0);
                      Navigator.of(ctx).push(createRoute(LoginPage()));
                    },
                    child: Text(
                      ctx.translate().confirm,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
