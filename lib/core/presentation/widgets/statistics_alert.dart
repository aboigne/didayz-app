import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../extension/context.dart';

class StatisticsAlert extends StatelessWidget {
  StatisticsAlert({required this.didayz_name, required this.users, super.key,});

  final String didayz_name;
  final Map<String, DateTime> users;

  String _display_date_from_datetime(DateTime dt) {
    final formatter = new DateFormat('dd/MM/yyyy');
    return formatter.format(dt);
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              ctx.translate().users_of(didayz_name),
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: users.length,
              itemBuilder: (BuildContext context, int index) {
                String key = users.keys.elementAt(index);
                return ListTile(
                  title: Text(
                    '$key',
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  subtitle: Text(
                    ctx.translate().added_on(_display_date_from_datetime(users[key]!)),
                    style: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        fontSize: 12,
                        color: Color(0xFF999999),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Divider(
                  thickness: 0,
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.primaryColor,
                foregroundColor: Colors.white,
                minimumSize: Size.fromHeight(60),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                Navigator.pop(ctx);
              },
              child: Text(
                ctx.translate().ok,
                style: AppStyles.defaultTitleStyle.merge(
                  TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
