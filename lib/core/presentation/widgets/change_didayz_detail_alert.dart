import 'dart:io';

import 'package:didayz/core/constants/api_constants.dart';
import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/didayz.dart';
import 'package:didayz/core/presentation/service/file.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/spacing.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/didayz_detail/presentation/blocs/didayz_detail_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../features/didayz/domain/entities/didayz.dart';
import '../../extension/context.dart';
import '../../resources/images.dart';

class ChangeDidayzDetailAlert extends StatefulWidget {
  ChangeDidayzDetailAlert(this.crt_didayz, {super.key});

  final DidayzEntity crt_didayz;

  @override
  State<ChangeDidayzDetailAlert> createState() => _ChangeDidayzDetailAlertState();
}

class _ChangeDidayzDetailAlertState extends State<ChangeDidayzDetailAlert> {
  final String _base_url = GlobalConfiguration().getValue('api_base_url');
  TextEditingController didayz_date_end = TextEditingController();
  late String name = '', access_key = '', intro_msg = '';
  late DateTime dtEnd;
  bool image_updated = false;
  XFile? pic = null;
  final ImagePicker _picker = ImagePicker();
  ImageSource? media_selected = null;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    clearCache();
    didayz_date_end.text = DateFormat('dd/MM/yyyy').format(widget.crt_didayz.DateEnd);
    name = widget.crt_didayz.Name;
    access_key = widget.crt_didayz.Key;
    intro_msg = widget.crt_didayz.IntroMsg == null ? '' : widget.crt_didayz.IntroMsg!;
    super.initState();
  }

  void clearCache() {
    imageCache.clear();
    imageCache.clearLiveImages();
  }

  Future getImage(ImageSource media) async {
    this.setState(() {
      media_selected = media;
    });
    XFile? selected_file;
    bool error = false;
    if(media == ImageSource.gallery)
    {
      selected_file = await _picker.pickImage(source: ImageSource.gallery);
    }
    else if(media == ImageSource.camera)
    {
      selected_file = await _picker.pickImage(source: ImageSource.camera);
    }

    if(selected_file != null && !error)
    {
      setState(() {
        pic = selected_file;
        image_updated = true;
      });
    }
  }

  void alert_image() {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          insetPadding: EdgeInsets.all(20.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  context.translate().media_to_use,
                  style: AppStyles.defaultTitleStyle.merge(
                    TextStyle(
                      color: AppColors.primaryColor,
                    ),
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                    getImage(ImageSource.gallery);
                  },
                  child: Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.primaryColor,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.image,
                          color: Colors.white,
                        ),
                        AppGap.regular(),
                        Text(
                          context.translate().from_gallery,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                    getImage(ImageSource.camera);
                  },
                  child: Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.primaryColor,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.camera,
                          color: Colors.white,
                        ),
                        AppGap.regular(),
                        Text(
                          context.translate().from_camera,
                          style: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void remove_image() {
    setState(() {
      image_updated = true;
      pic = null;
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 100,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:BorderRadius.circular(100),
                          ),
                          child: SizedBox(
                            height: 18,
                            width: 18,
                            child: IconButton(
                              padding: new EdgeInsets.all(0.0),
                              icon: const Icon(
                                Icons.edit,
                                color: Colors.black,
                                size: 18,
                              ),
                              onPressed: alert_image,
                            ),
                          ),
                        ),
                        if(pic != null || (widget.crt_didayz.Background != null && widget.crt_didayz.Background != '')) Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:BorderRadius.circular(100),
                          ),
                          child: SizedBox(
                            height: 18,
                            width: 18,
                            child: IconButton(
                              padding: new EdgeInsets.all(0.0),
                              icon: const Icon(
                                Icons.remove_outlined,
                                color: Colors.black,
                                size: 18,
                              ),
                              onPressed: remove_image,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    image: DecorationImage(
                      image: widget.crt_didayz.Background != null && widget.crt_didayz.Background != '' && !image_updated ?
                      Image.network(
                        _base_url + ApiConstants.getFile(widget.crt_didayz.id, widget.crt_didayz.Background!),
                        key: UniqueKey(),
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        },
                        errorBuilder: (context, obj, stackTrace) {
                          return new Image.asset(
                            Images.didayz_blue,
                            key: UniqueKey(),
                            cacheWidth: 400,
                          );
                        },
                      ).image : pic != null ?
                      Image.file(
                        File(pic!.path),
                        width: 200,
                        height: 200,
                      ).image :
                      Image.asset(
                        Images.didayz_blue,
                        key: UniqueKey(),
                        cacheWidth: 400,
                      ).image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                AppGap.large(),
                TextFormField(
                  initialValue: name,
                  scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                  ),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  style: AppStyles.inputText.merge(
                    TextStyle(
                      fontSize: 14,
                    ),
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    filled: true,
                    fillColor: Colors.white,
                    alignLabelWithHint: true,
                    labelText: context.translate().form_title,
                    labelStyle: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    hintText: context.translate().form_title,
                    hintStyle: AppStyles.inputText.merge(
                      TextStyle(
                        color: Color(0xFF999999),
                        fontSize: 10,
                      ),
                    ),
                    errorStyle: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        color: Color(0xffffaea6),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  cursorColor: Colors.black,
                  cursorHeight: 20.0,
                  obscureText: false,
                  onChanged: (value) {
                    this.setState(() {
                      name = value;
                    });
                  },
                  validator: (value){
                    if(value == null || value.isEmpty || value == '')
                    {
                      return context.translate().field_required;
                    }
                    else if(value.length > 50)
                    {
                      return context.translate().form_error_50char;
                    }
                    return null;
                  },
                ),
                AppGap.regular(),
                TextFormField(
                  initialValue: intro_msg,
                  scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  minLines: 3,
                  textInputAction: TextInputAction.newline,
                  style: AppStyles.inputText.merge(
                    TextStyle(
                      fontSize: 14,
                    ),
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    filled: true,
                    fillColor: Colors.white,
                    alignLabelWithHint: true,
                    labelText: context.translate().form_introductory_message,
                    labelStyle: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    hintText: context.translate().form_introductory_message,
                    hintStyle: AppStyles.inputText.merge(
                      TextStyle(
                        color: Color(0xFF999999),
                        fontSize: 10,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  cursorColor: Colors.black,
                  cursorHeight: 20.0,
                  obscureText: false,
                  onChanged: (value) {
                    this.setState(() {
                      intro_msg = value;
                    });
                  },
                ),
                AppGap.regular(),
                TextFormField(
                  scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                  ),
                  controller: didayz_date_end,
                  readOnly: true,
                  style: AppStyles.inputText.merge(
                    TextStyle(
                      fontSize: 14,
                    ),
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    filled: true,
                    fillColor: Colors.white,
                    alignLabelWithHint: true,
                    labelText: context.translate().form_end_day,
                    labelStyle: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    hintText: context.translate().form_end_day,
                    hintStyle: AppStyles.inputText.merge(
                      TextStyle(
                        color: Color(0xFF999999),
                        fontSize: 10,
                      ),
                    ),
                    errorStyle: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        color: Color(0xffffaea6),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  validator: (value){
                    if(value == null || value.isEmpty || value == '')
                    {
                      return context.translate().field_required;
                    }
                    return null;
                  },
                  onTap: () async {
                    final date_end_format = !didayz_date_end.text.isEmpty ? didayz_date_end.text.split('/')[2] + '-' + didayz_date_end.text.split('/')[1] + '-' + didayz_date_end.text.split('/')[0] : '';
                    final DateTime? pickedDate = await showDatePicker(
                      context: context,
                      builder: (context, child) {
                        return Theme(
                          data: Theme.of(context).copyWith(
                            colorScheme: ColorScheme.light(
                              primary: AppColors.primaryColor, // header background color
                              onPrimary: Colors.black, // header text color
                              onSurface: AppColors.primaryColor, // body text color
                            ),
                            textButtonTheme: TextButtonThemeData(
                              style: TextButton.styleFrom(
                                foregroundColor: AppColors.primaryColor, // button text color
                              ),
                            ),
                          ),
                          child: child!,
                        );
                      },
                      initialDate: didayz_date_end.text.isEmpty ? DateTime.now() : DateTime.parse(date_end_format),
                      firstDate: DateTime.parse(date_end_format).isBefore(DateTime.now()) ? DateTime.parse(date_end_format) : DateTime.now(),
                      lastDate: DateTime.now().add(const Duration(days: 3650)),
                    );

                    if (pickedDate != null) {
                      final String formattedDate = DateFormat('dd/MM/yyyy').format(pickedDate);
                      setState(() {
                        didayz_date_end.text = formattedDate; //set output date to TextField value.
                      });
                    }
                  },
                ),
                AppGap.regular(),
                TextFormField(
                  initialValue: access_key,
                  scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom + 64,
                  ),
                  textInputAction: TextInputAction.next,
                  style: AppStyles.inputText.merge(
                    TextStyle(
                      fontSize: 14,
                    ),
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    filled: true,
                    fillColor: Colors.white,
                    alignLabelWithHint: true,
                    labelText: context.translate().form_key,
                    labelStyle: AppStyles.inputText.merge(
                      TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    hintText: context.translate().form_key_bis,
                    hintStyle: AppStyles.inputText.merge(
                      TextStyle(
                        color: Color(0xFF999999),
                        fontSize: 10,
                      ),
                    ),
                    errorStyle: AppStyles.defaultTextStyle.merge(
                      TextStyle(
                        color: Color(0xffffaea6),
                      ),
                    ),
                    errorMaxLines: 3,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  cursorColor: Colors.black,
                  cursorHeight: 20.0,
                  obscureText: false,
                  onChanged: (value) {
                    this.setState(() {
                      access_key = value;
                    });
                  },
                  validator: (value){
                    final validCharacters = RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$');
                    if(value == null || value.isEmpty || value == '')
                    {
                      return context.translate().field_required;
                    }
                    else if(!validCharacters.hasMatch(value))
                    {
                      return context.translate().form_key_error_format;
                    }
                    else if(value.length > 20)
                    {
                      return context.translate().form_error_20char;
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.primaryColor,
                          foregroundColor: Colors.white,
                          minimumSize: Size.fromHeight(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(ctx);
                        },
                        child: Text(
                          ctx.translate().cancel,
                          style: AppStyles.defaultTitleStyle.merge(
                            TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.primaryColor,
                          foregroundColor: Colors.white,
                          minimumSize: Size.fromHeight(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        onPressed: () async {
                          final FocusScopeNode currentFocus = FocusScope.of(context);

                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          }

                          if(_formKey.currentState!.validate())
                          {
                            final network_inf = new NetworkInfoImpl();

                            if(!(await network_inf.isConnected))
                            {
                              final snackBar = SnackBar(
                                content: Text(
                                  context.translate().no_internet_connection,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                backgroundColor: AppColors.blazeOrange,
                              );
                              ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                              Navigator.pop(ctx);
                              return;
                            }

                            String? errors = null;
                            if(widget.crt_didayz.Name != name)
                            {
                              final result_didayz_name = await DidayzService().update_didayz_name(widget.crt_didayz.id, name);
                              if(result_didayz_name != 200)
                              {
                                errors = context.translate().didayz_update_error;
                              }
                            }

                            if(widget.crt_didayz.IntroMsg != intro_msg)
                            {
                              final result_didayz_intro_msg = await DidayzService().update_didayz_intro_msg(widget.crt_didayz.id, intro_msg);

                              if(result_didayz_intro_msg != 200)
                              {
                                errors = errors != null ? errors : context.translate().didayz_update_error;
                              }
                            }

                            if(widget.crt_didayz.Key != access_key)
                            {
                              final result_didayz_key = await DidayzService().update_didayz_key(widget.crt_didayz.id, access_key);
                              late SnackBar snackBar;

                              if(result_didayz_key == 400) {
                                snackBar = SnackBar(
                                  content: Text(
                                    ctx.translate().didayz_creation_error_key,
                                    style: AppStyles.defaultTextStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  backgroundColor: AppColors.blazeOrange,
                                );
                                ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                              }
                              else if(result_didayz_key != 200) {
                                errors = errors != null ? errors : context.translate().didayz_update_error;
                              }
                            }

                            if(image_updated)
                            {
                              final result_didayz_pic = await DidayzService().update_didayz_pic(widget.crt_didayz.id, pic != null ? media_selected == ImageSource.gallery ? pic!.name : 'from_camera.jpeg' : null);

                              if(result_didayz_pic == 200)
                              {
                                if(pic != null)
                                {
                                  final result_upload_file = await FileService().upload_file(pic!, '_didayz_pic', widget.crt_didayz.id, media_selected == ImageSource.gallery ? '0' : '1');

                                  if(result_upload_file != 200)
                                  {
                                    errors = errors != null ? errors : context.translate().didayz_update_error;
                                  }
                                }
                                else if(widget.crt_didayz.Background != null)
                                {
                                  await FileService().delete_file('didayz_' + widget.crt_didayz.id.toString(), widget.crt_didayz.Background!, widget.crt_didayz.id);
                                }
                              }
                              else
                              {
                                errors = errors != null ? errors : context.translate().didayz_update_error;
                              }
                            }

                            if(DateFormat('dd/MM/yyyy').format(widget.crt_didayz.DateEnd) != didayz_date_end.text)
                            {
                              final String date_end = didayz_date_end.text.split('/')[2] + '-' + didayz_date_end.text.split('/')[1] + '-' + didayz_date_end.text.split('/')[0];
                              final result_didayz_dt_end = await DidayzService().update_didayz_date_end(widget.crt_didayz.id, date_end);
                              if(result_didayz_dt_end != 200)
                              {
                                errors = context.translate().didayz_update_error;
                              }
                            }

                            clearCache();
                            context.read<DidayzDetailCubit>().getDidayzDetail(widget.crt_didayz.id);
                            Navigator.pop(ctx);

                            if(errors != null)
                            {
                              final SnackBar snackBar = SnackBar(
                                content: Text(
                                  errors,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                backgroundColor: AppColors.blazeOrange,
                              );
                              ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                            }
                          }
                        },
                        child: Text(
                          ctx.translate().edit,
                          style: AppStyles.defaultTitleStyle.merge(
                            TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
