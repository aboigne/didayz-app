import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../extension/context.dart';

class ParamDayAlert extends StatefulWidget {
  ParamDayAlert(this.is_access_code, {super.key});

  final bool is_access_code;

  @override
  State<ParamDayAlert> createState() => _ParamDayAlertState();
}

class _ParamDayAlertState extends State<ParamDayAlert> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.is_access_code ? context.translate().access_code : context.translate().answer,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
              child: Text(
                widget.is_access_code ? context.translate().day_access_code_txt(DateFormat.MMMMd('fr').format(DateTime.now().subtract(Duration(days: 1))), DateFormat.MMMMd('fr').format(DateTime.now())) : context.translate().day_send_mail_txt,
                style: AppStyles.defaultTextStyle.merge(
                  TextStyle(
                    color: Color(0xFF999999),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20.0,),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.primaryColor,
                foregroundColor: Colors.white,
                minimumSize: Size.fromHeight(60),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                Navigator.pop(ctx);
              },
              child: Text(
                context.translate().got_it,
                style: AppStyles.defaultTitleStyle.merge(
                  TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
