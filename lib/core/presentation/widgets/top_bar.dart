import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';

import '../../extension/context.dart';

class TopBar extends StatelessWidget implements PreferredSizeWidget {
  const TopBar({
    this.leading,
    this.title,
    super.key,
  });

  final Widget? leading;
  final Widget? title;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        leadingWidth: 130,
        title: title,
        leading: ElevatedButton.icon(
          onPressed: () => Navigator.of(context).pop(),
          icon: const Icon(Icons.chevron_left_rounded, color: Colors.white,),
          label: Text(
            context.translate().back,
            style: AppStyles.defaultTitleStyle.merge(
              TextStyle(
                color: Colors.white,
                fontSize: 14.0,
              ),
            ),
          ),
          style: ElevatedButton.styleFrom(
            elevation: 0,
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
