import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../extension/context.dart';
import '../../resources/vectors.dart';
import '../../theme/app_colors.dart';

import '../../theme/styles.dart';
import '../blocs/navigation/navigation_cubit.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  const BottomNavigationBarWidget({Key? key}) : super(key: key);

  @override
  _BottomNavigationBarWidgetState createState() =>
      _BottomNavigationBarWidgetState();
}

class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationCubit, NavigationState>(
      builder: (context, state) {
        return SizedBox(
          height: 70,
          child: BottomNavigationBar(
            currentIndex: state.index,
            selectedItemColor: AppColors.primaryColor,
            showUnselectedLabels: true,
            unselectedItemColor: const Color(0xFF7B7B7B),
            selectedLabelStyle: AppStyles.defaultTextStyle.merge(
              TextStyle(
                fontSize: 12,
                color: AppColors.primaryColor,
                overflow: TextOverflow.visible,
              ),
            ),
            unselectedLabelStyle: AppStyles.defaultTextStyle.merge(
              TextStyle(
                fontSize: 12,
                color: const Color(0xFF7B7B7B),
                overflow: TextOverflow.visible,
              ),
            ),
            type: BottomNavigationBarType.fixed,
            elevation: 1,
            items: [
              BottomNavigationBarItem(
                activeIcon: _icon(
                  isSvg: true,
                  SvgFile: Vectors.home_icon,
                  AppColors.primaryColor,
                ),
                icon: _icon(
                  isSvg: true,
                  SvgFile: Vectors.home_icon,
                  const Color(0xFF7B7B7B),
                ),
                label: context.translate().home,
              ),
              BottomNavigationBarItem(
                activeIcon: _icon(
                  isSvg: true,
                  SvgFile: Vectors.didayz_icon,
                  AppColors.primaryColor,
                ),
                icon: _icon(
                  isSvg: true,
                  SvgFile: Vectors.didayz_icon,
                  const Color(0xFF7B7B7B),
                ),
                label: context.translate().didayz,
              ),
            ],
            onTap: context.read<NavigationCubit>().getNavBarItemFromIndex,
          ),
        );
      },
    );
  }

  Widget _icon(Color color, {bool isSvg = false, IconData icon = Icons.account_circle, String SvgFile = ''})
  {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: isSvg
          ? SvgPicture.asset(
              SvgFile,
              colorFilter: ColorFilter.mode(
                color,
                BlendMode.srcIn,
              ),
              height: 20,
              width: 20,
              fit: BoxFit.scaleDown,
            ) : Icon(
              icon,
              color: color,
              size: 20,
            ),
    );
  }
}
