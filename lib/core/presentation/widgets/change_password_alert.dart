import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../features/account/service/profil.dart';
import '../../extension/context.dart';

class ChangePasswordAlert extends StatefulWidget {
  ChangePasswordAlert({super.key});

  @override
  State<ChangePasswordAlert> createState() => _ChangePasswordAlertState();
}

class _ChangePasswordAlertState extends State<ChangePasswordAlert> {
  String? old_password = null, new_password = null, new_password_confirmation = null;
  String? errorTextPassword = null, errorTextNewPassword = null, errorTextNewPasswordConfirm = null;
  final _formKeyAlert = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    final RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,60}$');
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SingleChildScrollView(
                physics: ScrollPhysics(),
                child: Form(
                  key: _formKeyAlert,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        autofocus: true,
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                        style: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          filled: true,
                          fillColor: Colors.white,
                          alignLabelWithHint: true,
                          hintText: context.translate().current_pwd,
                          hintStyle: AppStyles.inputText.merge(
                            TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 10,
                            ),
                          ),
                          errorText: errorTextPassword,
                          errorStyle: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Color(0xffffaea6),
                            ),
                          ),
                          errorMaxLines: 10,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        cursorColor: Colors.black,
                        cursorHeight: 20.0,
                        obscureText: true,
                        onChanged: (value) {
                          this.setState(() {
                            old_password = value;
                          });
                        },
                        validator: (val){
                          if(val == null || val.isEmpty)
                          {
                            errorTextPassword = ctx.translate().field_required;
                          }
                          else if(!regex.hasMatch(val))
                          {
                            errorTextPassword = ctx.translate().format_pwd;
                          }
                          else
                          {
                            errorTextPassword = null;
                          }
                          return errorTextPassword;
                        },
                      ),
                      SizedBox(height: 10.0,),
                      TextFormField(
                        autofocus: false,
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                        style: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          filled: true,
                          fillColor: Colors.white,
                          alignLabelWithHint: true,
                          hintText: context.translate().new_pwd,
                          hintStyle: AppStyles.inputText.merge(
                            TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 10,
                            ),
                          ),
                          errorText: errorTextNewPassword,
                          errorStyle: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Color(0xffffaea6),
                            ),
                          ),
                          errorMaxLines: 10,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        cursorColor: Colors.black,
                        cursorHeight: 20.0,
                        obscureText: true,
                        onChanged: (value) {
                          this.setState(() {
                            new_password = value;
                          });
                        },
                        validator: (val){
                          if(val == null || val.isEmpty)
                          {
                            errorTextNewPassword = ctx.translate().field_required;
                          }
                          else if(!regex.hasMatch(val))
                          {
                            errorTextNewPassword = ctx.translate().format_pwd;
                          }
                          else
                          {
                            errorTextNewPassword = null;
                          }
                          return errorTextNewPassword;
                        },
                      ),
                      SizedBox(height: 10.0,),
                      TextFormField(
                        autofocus: false,
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                        style: AppStyles.inputText.merge(
                          TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          filled: true,
                          fillColor: Colors.white,
                          alignLabelWithHint: true,
                          hintText: ctx.translate().confirm_pwd,
                          hintStyle: AppStyles.inputText.merge(
                            TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 10,
                            ),
                          ),
                          errorText: errorTextNewPasswordConfirm,
                          errorStyle: AppStyles.defaultTextStyle.merge(
                            TextStyle(
                              color: Color(0xffffaea6),
                            ),
                          ),
                          errorMaxLines: 10,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        cursorColor: Colors.black,
                        cursorHeight: 20.0,
                        obscureText: true,
                        onChanged: (value) {
                          this.setState(() {
                            new_password_confirmation = value;
                          });
                        },
                        validator: (val){
                          if(val == null || val.isEmpty)
                          {
                            errorTextNewPasswordConfirm = ctx.translate().field_required;
                          }
                          else if(val != this.new_password)
                          {
                            errorTextNewPasswordConfirm = ctx.translate().confirm_pwd_invalid;
                          }
                          else
                          {
                            errorTextNewPasswordConfirm = null;
                          }

                          return errorTextNewPasswordConfirm;
                        },
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.primaryColor,
                        foregroundColor: Colors.white,
                        minimumSize: Size.fromHeight(40),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(ctx);
                      },
                      child: Text(
                        ctx.translate().cancel,
                        style: AppStyles.defaultTitleStyle.merge(
                          TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.primaryColor,
                        foregroundColor: Colors.white,
                        minimumSize: Size.fromHeight(40),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      onPressed: () async {
                        FocusScopeNode currentFocus = FocusScope.of(ctx);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }

                        if(_formKeyAlert.currentState!.validate() && this.old_password != null && this.new_password != null && this.new_password_confirmation != null)
                        {
                          SnackBar? snackBar = null;
                          final network_inf = new NetworkInfoImpl();
                          if(!(await network_inf.isConnected))
                          {
                            snackBar = SnackBar(
                              content: Text(
                                context.translate().no_internet_connection,
                                style: AppStyles.defaultTextStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              backgroundColor: AppColors.blazeOrange,
                            );
                            ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                            Navigator.pop(ctx);
                          }
                          else
                          {
                            final result_change_pwd = await ProfilService().change_pwd_user(this.old_password!, this.new_password!);
                            if(result_change_pwd == 200)
                            {
                              snackBar = SnackBar(
                                content: Text(
                                  context.translate().pwd_update_success,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                backgroundColor: AppColors.primaryColor,
                              );
                              ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                              Navigator.pop(ctx);
                            }
                            else if(result_change_pwd == 401)
                            {
                              snackBar = SnackBar(
                                content: Text(
                                  context.translate().old_pwd_not_correct,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                backgroundColor: AppColors.blazeOrange,
                              );
                              ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                            }
                            else
                            {
                              snackBar = SnackBar(
                                content: Text(
                                  context.translate().pwd_update_error,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                backgroundColor: AppColors.blazeOrange,
                              );
                              ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                            }
                          }
                        }
                      },
                      child: Text(
                        ctx.translate().edit,
                        style: AppStyles.defaultTitleStyle.merge(
                          TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
