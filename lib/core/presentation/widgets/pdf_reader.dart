import 'package:didayz/core/presentation/widgets/top_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class PDFReaderFile extends StatefulWidget {
  final String pdfFile;
  final bool is_picked;
  const PDFReaderFile({Key? key, required this.pdfFile, required this.is_picked}) : super(key: key);

  @override
  _PDFReaderFileState createState() => _PDFReaderFileState(pdfFile);
}

class _PDFReaderFileState extends State<PDFReaderFile > {
  String pdfFile;

  _PDFReaderFileState(this.pdfFile);  //constructor

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(),
      body: widget.is_picked ?
        PDF(
          swipeHorizontal: true,
        ).fromPath(this.pdfFile) :
        PDF(
          swipeHorizontal: true,
        ).fromUrl(this.pdfFile),
    );
  }
}
