import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:video_player/video_player.dart';
import 'package:audioplayers/audioplayers.dart' as AudioPlay;
import 'package:didayz/core/constants/api_constants.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../extension/context.dart';
import '../../resources/images.dart';

class SeeContentAlert extends StatefulWidget {
  SeeContentAlert(this.calId, this.type, {super.key, this.video_file_from_url, this.video_file_from_local, this.video_file_from_youtube, this.picture_file_from_url, this.picture_file_from_local, this.audio_file_from_url, this.audio_file_from_local});

  final int calId;
  final String type;
  final String? video_file_from_url;
  final File? video_file_from_local;
  final String? video_file_from_youtube;
  final String? picture_file_from_url;
  final XFile? picture_file_from_local;
  final String? audio_file_from_url;
  final File? audio_file_from_local;

  @override
  State<SeeContentAlert> createState() => _SeeContentAlertState();
}

class _SeeContentAlertState extends State<SeeContentAlert> {
  final String _base_url = GlobalConfiguration().getValue('api_base_url');

  /* Vidéo */
  Duration _videoposition = new Duration();
  /* From Local or URL */
  VideoPlayerController? _videoPlayerController = null;
  //CustomVideoPlayerController? _customVideoPlayerController = null;
  /* From Youtube */
  late YoutubePlayerController _controller;
  bool _youtubeismute = false;
  String? youtube_title, youtube_channel;

  /* Audio */
  AudioPlay.AudioPlayer player = AudioPlay.AudioPlayer();
  int maxduration = 100, currentpos = 0;

  @override
  void initState() {
    super.initState();
    if(widget.video_file_from_youtube != null)
    {
      _controller = YoutubePlayerController(
        initialVideoId: widget.video_file_from_youtube!,
        flags: const YoutubePlayerFlags(
          hideControls: true,
          hideThumbnail: true,
          mute: false,
          autoPlay: false,
          disableDragSeek: false,
          loop: false,
          isLive: false,
          forceHD: false,
          enableCaption: true,
        ),
      )..addListener(() {
        setState(() {
          _videoposition = _controller.value.position;
        });
      });

      getYoutubeMetadata(widget.video_file_from_youtube!);
    }
    if(widget.video_file_from_url != null)
    {
      _videoPlayerController = VideoPlayerController.network(
        _base_url + ApiConstants.getFile(widget.calId, widget.video_file_from_url!),
      )..addListener(() {
        setState(() {
          _videoposition = _videoPlayerController!.value.position;
        });
      })..initialize().then((_) {
          setState(() {});
      });
    }
    if(widget.video_file_from_local != null)
    {
      _videoPlayerController = VideoPlayerController.asset(widget.video_file_from_local!.path)
        ..addListener(() {
          setState(() {
            _videoposition = _videoPlayerController!.value.position;
          });
        })
        ..initialize().then((_) {
          setState(() {});
        });
    }
    if(widget.audio_file_from_local != null || widget.audio_file_from_url != null)
    {
      Future.delayed(Duration.zero, () async {
        player.onDurationChanged.listen((d) {
          setState(() {
            maxduration = d.inMilliseconds;
          });
        });

        player.onPositionChanged.listen((p){
          setState(() {
            currentpos = p.inMilliseconds;
          });
        });
      });
    }

    /*if(_videoPlayerController != null){
      _customVideoPlayerController = CustomVideoPlayerController(
        context: context,
        videoPlayerController: _videoPlayerController!,
      );
    }*/
  }

  void getYoutubeMetadata(String id_yt) async {
    final String yt_url = 'https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=$id_yt&format=json';
    final res = await http.get(Uri.parse(yt_url));
    try {
      if (res.statusCode == 200) {
        setState(() {
          youtube_title = json.decode(res.body)['title'];
          youtube_channel = json.decode(res.body)['author_name'];
        });
      }
    } on FormatException catch (e) {
      print('invalid JSON' + e.toString());
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    if(_videoPlayerController != null) _videoPlayerController!.dispose();
    //if(_customVideoPlayerController != null) _customVideoPlayerController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 7.0, right: 7.0,),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: ListView(
                shrinkWrap: true,
                children: [
                  if(widget.type == 'Picture' && widget.picture_file_from_url != null) Image.network(
                    _base_url + ApiConstants.getFile(widget.calId, widget.picture_file_from_url!),
                    key: UniqueKey(),
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) {
                        return child;
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                    errorBuilder: (context, obj, stackTrace) {
                      return new Image.asset(
                        Images.didayz_blue,
                        key: UniqueKey(),
                        cacheWidth: 400,
                      );
                    },
                  ),
                  if(widget.type == 'Picture' && widget.picture_file_from_local != null) Image.file(
                    File(widget.picture_file_from_local!.path),
                  ),
                  if(widget.type == 'Video' && (widget.video_file_from_url != null || widget.video_file_from_local != null) && _videoPlayerController != null /*&& _customVideoPlayerController != null*/) Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AspectRatio(
                        aspectRatio: _videoPlayerController!.value.aspectRatio,
                        child: VideoPlayer(
                          _videoPlayerController!,
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(20.0),
                        color: AppColors.primaryColor,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  _videoposition.inMinutes.toString() + ':' + (_videoposition.inSeconds % 60).toString().padLeft(2, '0'),
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Slider(
                                  inactiveColor: Colors.white.withOpacity(0.5),
                                  activeColor: Colors.white,
                                  thumbColor: Colors.white,
                                  value: _videoPlayerController!.value.duration.inMilliseconds > 0 ? (_videoposition.inMilliseconds / _videoPlayerController!.value.duration.inMilliseconds) * 100 : 0,
                                  min: 0.0,
                                  max: 100.0,
                                  onChanged: (value) {
                                    final double new_position = (value * _videoPlayerController!.value.duration.inMilliseconds) / 100;
                                    _videoPlayerController!.seekTo(Duration(milliseconds: new_position.round()));
                                    setState(() {
                                      _videoposition = Duration(milliseconds: new_position.round());
                                    });
                                  },
                                ),
                                Text(
                                  _videoPlayerController!.value.duration.inMinutes.toString() + ':' + (_videoPlayerController!.value.duration.inSeconds % 60).toString().padLeft(2, '0'),
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    final double newpos = _videoposition.inSeconds - 5.0;
                                    _videoPlayerController!.seekTo(Duration(seconds: newpos.round()));
                                    setState(() {
                                      _videoposition = newpos < 0 ? Duration(seconds: 0) : Duration(seconds: newpos.round());
                                    });
                                  },
                                  icon: Icon(
                                    Icons.fast_rewind_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if(_videoPlayerController!.value.isPlaying)
                                    {
                                      _videoPlayerController!.pause();
                                      setState(() {});
                                    }
                                    else
                                    {
                                      _videoPlayerController!.play();
                                      setState(() {});
                                    }
                                  },
                                  icon: Icon(
                                    _videoPlayerController!.value.isPlaying ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    final double newpos = _videoposition.inSeconds + 5.0;
                                    _videoPlayerController!.seekTo(Duration(seconds: newpos.round()));
                                    setState(() {
                                      _videoposition = Duration(seconds: newpos.round()) > _videoPlayerController!.value.duration ? _videoPlayerController!.value.duration : Duration(seconds: newpos.round());
                                    });
                                  },
                                  icon: Icon(
                                    Icons.fast_forward_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if(_videoPlayerController!.value.volume > 0)
                                    {
                                      _videoPlayerController!.setVolume(0.0);
                                    }
                                    else
                                    {
                                      _videoPlayerController!.setVolume(1.0);
                                    }
                                  },
                                  icon: Icon(
                                    _videoPlayerController!.value.volume > 0 ? Icons.volume_off_outlined : Icons.volume_up_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  child: SliderTheme(
                                    child: Slider(
                                      inactiveColor: Colors.white.withOpacity(0.5),
                                      activeColor: Colors.white,
                                      value: _videoPlayerController!.value.volume,
                                      min: 0.0,
                                      max: 1.0,
                                      onChanged: (value) {
                                        _videoPlayerController!.setVolume(value);
                                      },
                                    ),
                                    data: SliderTheme.of(context).copyWith(
                                      thumbShape: SliderComponentShape.noThumb,
                                      trackShape: RectangularSliderTrackShape(),
                                      overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  if(widget.type == 'Video' && widget.video_file_from_youtube != null) Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      YoutubePlayer(
                        controller: _controller,
                      ),
                      Container(
                        width: double.infinity,
                        color: Colors.white,
                        padding: EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0, bottom: 60.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              context.translate().form_title + ' : ' + (youtube_title != null ? youtube_title! : context.translate().unknown_title),
                              textAlign: TextAlign.left,
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            SizedBox(height: 8,),
                            Text(
                              context.translate().channel + ' : ' + (youtube_channel != null ? youtube_channel! : context.translate().unknown_channel),
                              textAlign: TextAlign.left,
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Color(0xFF999999),
                                  fontSize: 12.0,
                                ),
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(20.0),
                        color: AppColors.primaryColor,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  _videoposition.inMinutes.toString() + ':' + (_videoposition.inSeconds % 60).toString().padLeft(2, '0'),
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Slider(
                                  inactiveColor: Colors.white.withOpacity(0.5),
                                  activeColor: Colors.white,
                                  thumbColor: Colors.white,
                                  value: _controller.value.metaData.duration.inMilliseconds > 0 ? (_videoposition.inMilliseconds / _controller.value.metaData.duration.inMilliseconds) * 100 : 0,
                                  min: 0.0,
                                  max: 100.0,
                                  onChanged: (value) {
                                    double new_position = (value * _controller.value.metaData.duration.inMilliseconds) / 100;
                                    _controller.seekTo(Duration(milliseconds: new_position.round()));
                                    setState(() {
                                      _videoposition = Duration(milliseconds: new_position.round());
                                    });
                                  },
                                ),
                                Text(
                                  _controller.value.metaData.duration.inMinutes.toString() + ':' + (_controller.value.metaData.duration.inSeconds % 60).toString().padLeft(2, '0'),
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    double newpos = _videoposition.inSeconds - 5.0;
                                    _controller.seekTo(Duration(seconds: newpos.round()));
                                    setState(() {
                                      _videoposition = newpos < 0 ? Duration(seconds: 0) : Duration(seconds: newpos.round());
                                    });
                                  },
                                  icon: Icon(
                                    Icons.fast_rewind_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if(_controller.value.isPlaying)
                                    {
                                      _controller.pause();
                                      setState(() {});
                                    }
                                    else
                                    {
                                      _controller.play();
                                      setState(() {});
                                    }
                                  },
                                  icon: Icon(
                                    _controller.value.isPlaying ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    double newpos = _videoposition.inSeconds + 5.0;
                                    _controller.seekTo(Duration(seconds: newpos.round()));
                                    setState(() {
                                      _videoposition = Duration(seconds: newpos.round()) > _controller.value.metaData.duration ? _controller.value.metaData.duration : Duration(seconds: newpos.round());
                                    });
                                  },
                                  icon: Icon(
                                    Icons.fast_forward_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    IconButton(
                                      iconSize: 30,
                                      onPressed: () {
                                        if(!_youtubeismute)
                                        {
                                          _controller.mute();
                                          setState(() {
                                            _youtubeismute = true;
                                          });
                                        }
                                        else
                                        {
                                          _controller.unMute();
                                          setState(() {
                                            _youtubeismute = false;
                                          });
                                        }
                                      },
                                      icon: Icon(
                                        !_youtubeismute ? Icons.volume_off_outlined : Icons.volume_up_outlined,
                                        color: Colors.white,
                                      ),
                                    ),
                                    ClipRRect(
                                      borderRadius: BorderRadius.all(Radius.circular(12)),
                                      child: SliderTheme(
                                        child: Slider(
                                          inactiveColor: Colors.white.withOpacity(0.5),
                                          activeColor: Colors.white,
                                          value: _controller.value.volume.toDouble(),
                                          min: 0.0,
                                          max: 100,
                                          onChanged: (value) {
                                            _controller.setVolume(value.round());
                                          },
                                        ),
                                        data: SliderTheme.of(context).copyWith(
                                          thumbShape: SliderComponentShape.noThumb,
                                          trackShape: RectangularSliderTrackShape(),
                                          overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    _controller.toggleFullScreenMode();
                                  },
                                  icon: Icon(
                                    Icons.crop_free_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  if(widget.type == 'Sound' && (widget.audio_file_from_url != null || widget.audio_file_from_local != null)) Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(20.0),
                    color: AppColors.primaryColor,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              Duration(milliseconds:currentpos).inMinutes.toString() + ':' + (Duration(milliseconds:currentpos).inSeconds % 60).toString().padLeft(2, '0'),
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Slider(
                              inactiveColor: Colors.white.withOpacity(0.5),
                              activeColor: Colors.white,
                              thumbColor: Colors.white,
                              value: currentpos.toDouble(),
                              min: 0.0,
                              max: maxduration.toDouble(),
                              onChanged: (value) {
                                player.seek(Duration(milliseconds: value.round()));
                                setState(() {
                                  currentpos = value.round();
                                });
                              },
                            ),
                            Text(
                              Duration(milliseconds:maxduration).inMinutes.toString() + ':' + (Duration(milliseconds:maxduration).inSeconds % 60).toString().padLeft(2, '0'),
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                double newpos = Duration(milliseconds:currentpos).inSeconds - 5.0;
                                player.seek(Duration(seconds: newpos.round()));
                                setState(() {
                                  currentpos = newpos < 0 ? 0 : newpos.round();
                                });
                              },
                              icon: Icon(
                                Icons.fast_rewind_outlined,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                if(player.state == AudioPlay.PlayerState.paused)
                                {
                                  player.resume();
                                  setState(() {});
                                }
                                else if(player.state == AudioPlay.PlayerState.playing)
                                {
                                  player.pause();
                                  setState(() {});
                                }
                                else
                                {
                                  player.play(widget.audio_file_from_local != null ? new AudioPlay.DeviceFileSource(widget.audio_file_from_local!.path) : new AudioPlay.UrlSource(_base_url + ApiConstants.getFile(widget.calId, widget.audio_file_from_url!)));
                                  setState(() {});
                                }
                              },
                              icon: Icon(
                                player.state == AudioPlay.PlayerState.playing ?  Icons.pause_outlined : Icons.play_arrow_outlined,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              iconSize: 30,
                              onPressed: () {
                                double newpos = Duration(milliseconds:currentpos).inSeconds + 5.0;
                                player.seek(Duration(seconds: newpos.round()));
                                setState(() {
                                  currentpos = newpos.round() > Duration(milliseconds:maxduration).inSeconds ? maxduration : newpos.round();
                                });
                              },
                              icon: Icon(
                                Icons.fast_forward_outlined,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 0.0,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Align(
                alignment: Alignment.topRight,
                child: CircleAvatar(
                  radius: 14.0,
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.close,
                    color: AppColors.primaryColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
