import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/service/didayz.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../extension/context.dart';

class PublishAlert extends StatefulWidget {
  PublishAlert({required this.message, required this.calId, super.key,});

  final String message;
  final int calId;

  @override
  State<PublishAlert> createState() => _PublishAlertState();
}

class _PublishAlertState extends State<PublishAlert> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              ctx.translate().publish,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              widget.message,
              style: AppStyles.defaultTextStyle.merge(
                TextStyle(
                  color: Color(0xFF999999),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      ctx.translate().cancel,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () async {
                      final network_inf = new NetworkInfoImpl();
                      if(!(await network_inf.isConnected))
                      {
                        final snackBar = SnackBar(
                          content: Text(
                            context.translate().no_internet_connection,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.blazeOrange,
                        );
                        ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                        Navigator.pop(ctx);
                        return;
                      }

                      final result_publish = await DidayzService().publish_didayz(widget.calId);
                      if(result_publish == 402)
                      {
                        final snackBar = SnackBar(
                          content: Text(
                            context.translate().publish_didayz_update_app_error,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.blazeOrange,
                        );
                        ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                      }
                      else if(result_publish != 200)
                      {
                        final snackBar = SnackBar(
                          content: Text(
                            ctx.translate().error_publishing_didayz,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.blazeOrange,
                        );
                        ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                      }
                      else
                      {
                        final snackBar = SnackBar(
                          content: Text(
                            context.translate().publish_didayz_success,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.primaryColor,
                        );
                        ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
                      }
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      ctx.translate().publish,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
