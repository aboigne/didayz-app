import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:share_plus/share_plus.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../features/didayz/domain/entities/didayz.dart';
import '../../../features/didayz_detail/presentation/pages/didayz_detail_page.dart';
import '../../constants/api_constants.dart';
import '../../extension/context.dart';
import '../../resources/images.dart';
import '../../theme/app_colors.dart';
import '../../theme/styles.dart';
import '../service/didayz_states.dart';
import 'change_didayz_detail_alert.dart';
import 'loading_widget.dart';
import 'publish_alert.dart';
import 'route_transition.dart';

class CardDidayz extends StatefulWidget {
  final DidayzEntity didayz;
  final States_Didayz state;
  final bool didayz_desc;
  final refreshKey;
  const CardDidayz(this.didayz, this.state, {Key? key, this.didayz_desc = false, this.refreshKey = null}) : super(key: key);

  @override
  _CardDidayzState createState() => _CardDidayzState();
}

class _CardDidayzState extends State<CardDidayz > {
  _CardDidayzState();  //constructor
  final String _base_url = GlobalConfiguration().getValue('api_base_url');
  final formatter = new DateFormat('dd/MM/yyyy');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: GestureDetector(
        onTap: () async {
          if(widget.didayz_desc)
          {
            if(widget.state == States_Didayz.draft)
            {
              showDialog(
                context: context,
                builder: (context) {
                  return ChangeDidayzDetailAlert(widget.didayz);
                },
              );
            }
            else if(widget.didayz.IntroMsg != null && widget.didayz.IntroMsg != '')
            {
              showDialog(
                context: context,
                builder: (context) {
                  return Dialog(
                    insetPadding: EdgeInsets.all(10.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            widget.didayz.Name,
                            style: AppStyles.defaultTitleStyle.merge(
                              TextStyle(
                                color: AppColors.primaryColor,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0,),
                          Text(
                            widget.didayz.IntroMsg!,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Color(0xFF999999),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 40.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: AppColors.primaryColor,
                                foregroundColor: Colors.white,
                                minimumSize: Size.fromHeight(60),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                context.translate().ok,
                                style: AppStyles.defaultTitleStyle.merge(
                                  TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            }
          }
          else
          {
            await Navigator.of(context).push(createRoute(DidayzDetailPage(widget.didayz.id, widget.state,)));
            if(widget.refreshKey != null) {
              widget.refreshKey!.currentState!.show();
            }
          }
        },
        child: Container(
          padding: EdgeInsets.only(bottom: 15.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(24),),
            color: Colors.white,
            boxShadow: [
              new BoxShadow(
                offset: Offset(0.0, 4.0),
                color: AppColors.primaryColor.withOpacity(0.1),
                blurRadius: 20.0,
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 215,
                  width: double.infinity,
                  child: Stack(
                    children: [
                      Stack(
                        children: [
                          const Center(child: LoadingWidget()),
                          Center(
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(Radius.circular(18.0),),
                              child: widget.didayz.Background == '' || widget.didayz.Background == null ? Container(
                                color: Colors.white,
                                child: Image.asset(
                                  Images.didayz_blue,
                                  key: UniqueKey(),
                                  cacheWidth: 400,
                                ),
                              ) : FadeInImage.memoryNetwork(
                                width: double.infinity,
                                fit: BoxFit.cover,
                                placeholder: kTransparentImage,
                                image: _base_url + ApiConstants.getFile(widget.didayz.id, widget.didayz.Background!),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(36.0),
                              child: BackdropFilter(
                                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                child: Container(
                                  padding: const EdgeInsets.all(12.0),
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.2),
                                    borderRadius: BorderRadius.all(Radius.circular(36.0),),
                                  ),
                                  child: Text(
                                    context.translate().display_date(formatter.format(widget.didayz.DateEnd), formatter.format(widget.didayz.DateEnd.subtract(Duration(days: widget.didayz.NBDays-1)))),
                                    style: AppStyles.defaultTitleStyle.merge(
                                      TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              !([States_Didayz.received, States_Didayz.demo].contains(widget.state)) ?
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 25.0,),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.didayz.Name,
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: AppStyles.defaultTextStyle.merge(
                                    TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            if (widget.didayz.IntroMsg != null) Padding(
                              padding: const EdgeInsets.only(
                                left: 25.0,
                                right: 25.0,
                                top: 15.0,
                              ),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.didayz.IntroMsg!,
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: AppStyles.defaultDescStyle.merge(
                                    TextStyle(
                                      color: AppColors.grey,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 25.0),
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(
                              color: (widget.state == States_Didayz.published || (widget.didayz.Progress != null && widget.didayz.Progress == 100)) ? AppColors.greenWater : AppColors.blazeOrange,
                            ),
                            color: (widget.state == States_Didayz.published || (widget.didayz.Progress != null && widget.didayz.Progress == 100)) ? AppColors.greenWater.withOpacity(0.1) : AppColors.blazeOrange.withOpacity(0.1),
                          ),
                          child: Center(
                            child: Text(
                              widget.state == States_Didayz.published ? context.translate().published(1) : context.translate().completion_ratio((widget.didayz.Progress == null) ? '0' : widget.didayz.Progress!.toStringAsFixed(0)),
                              style: AppStyles.defaultTitleStyle.merge(
                                (widget.state == States_Didayz.published || (widget.didayz.Progress != null && widget.didayz.Progress == 100)) ?
                                TextStyle(
                                  color: AppColors.greenWater,
                                  fontSize: 12,
                                ) :
                                TextStyle(
                                  color: AppColors.blazeOrange,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  if(widget.state != States_Didayz.published && widget.didayz.Progress != null && widget.didayz.Progress == 100) Padding(
                    padding: const EdgeInsets.only(
                      top: 25.0,
                      left: 25.0,
                      right: 25.0,
                    ),
                    child: GestureDetector(
                      onTap: () async {
                        await showDialog(
                          context: context,
                          builder: (context) {
                            return PublishAlert(
                              message: context.translate().publish_txt,
                              calId: widget.didayz.id,
                            );
                          },
                        );
                      },
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: AppColors.primaryColor,
                        ),
                        child: Center(
                          child: Text(
                            context.translate().publish,
                            style: AppStyles.defaultTitleStyle.merge(
                              TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  if(widget.state == States_Didayz.published) Padding(
                    padding: const EdgeInsets.only(
                      top: 25.0,
                      left: 25.0,
                      right: 25.0,
                    ),
                    child: GestureDetector(
                      onTap: () {
                        Share.share(
                          context.translate().share_txt(formatter.format(widget.didayz.DateEnd), widget.didayz.Key),
                          subject: context.translate().share_txt_title,
                        );
                      },
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: AppColors.primaryColor,
                        ),
                        child: Center(
                          child: Text(
                            context.translate().share,
                            style: AppStyles.defaultTitleStyle.merge(
                              TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ) :
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0,),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.didayz.Name,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: AppStyles.defaultTextStyle.merge(
                          TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                  if (widget.didayz.IntroMsg != null) Padding(
                    padding: const EdgeInsets.only(
                      left: 25.0,
                      right: 25.0,
                      top: 15.0,
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.didayz.IntroMsg!,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: AppStyles.defaultDescStyle.merge(
                          TextStyle(
                            color: AppColors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
