import 'dart:convert';

import 'package:didayz/core/network/network_info.dart';
import 'package:didayz/core/presentation/blocs/navigation/navigation_cubit.dart';
import 'package:didayz/core/presentation/widgets/route_transition.dart';
import 'package:didayz/core/theme/app_colors.dart';
import 'package:didayz/core/theme/styles.dart';
import 'package:didayz/features/auth/presentation/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';

import '../../../features/account/service/profil.dart';
import '../../extension/context.dart';

class DeleteAccountAlert extends StatelessWidget {
  DeleteAccountAlert({super.key,});

  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
      insetPadding: EdgeInsets.all(20.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              ctx.translate().delete_account,
              style: AppStyles.defaultTitleStyle.merge(
                TextStyle(
                  color: AppColors.primaryColor,
                ),
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              ctx.translate().delete_account_alert,
              style: AppStyles.defaultTextStyle.merge(
                TextStyle(
                  color: Color(0xFF999999),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(ctx);
                    },
                    child: Text(
                      ctx.translate().cancel,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.primaryColor,
                      foregroundColor: Colors.white,
                      minimumSize: Size.fromHeight(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () async {
                      SnackBar? snackBar = null;
                      bool isDeleted = false;
                      final network_inf = new NetworkInfoImpl();

                      if(!(await network_inf.isConnected))
                      {
                        snackBar = SnackBar(
                          content: Text(
                            ctx.translate().no_internet_connection,
                            style: AppStyles.defaultTextStyle.merge(
                              TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          backgroundColor: AppColors.blazeOrange,
                        );
                      }
                      else
                      {
                        final result_change_user = await ProfilService().delete_account_user();
                        if(result_change_user == 200)
                        {
                          isDeleted = true;
                          snackBar = SnackBar(
                            content: Text(
                              ctx.translate().success_deleting_account,
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            backgroundColor: AppColors.primaryColor,
                          );
                        }
                        else
                        {
                          snackBar = SnackBar(
                            content: Text(
                              ctx.translate().error_deleting_account,
                              style: AppStyles.defaultTextStyle.merge(
                                TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            backgroundColor: AppColors.blazeOrange,
                          );
                        }
                      }
                      ScaffoldMessenger.of(ctx).showSnackBar(snackBar);

                      if(isDeleted) {
                        final key_encrypted = await secureStorage.read(
                          key: 'encryptionKey',
                        );

                        final encryptionKey = base64Url.decode(
                          key_encrypted!,
                        );
                        final boxUser = await Hive.openBox(
                          'user',
                          encryptionCipher: HiveAesCipher(encryptionKey),
                        );
                        boxUser.delete('token');

                        ctx.read<NavigationCubit>().getNavBarItemFromIndex(0);
                        Navigator.of(ctx).push(createRoute(LoginPage()));
                      }
                    },
                    child: Text(
                      ctx.translate().confirm,
                      style: AppStyles.defaultTitleStyle.merge(
                        TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
