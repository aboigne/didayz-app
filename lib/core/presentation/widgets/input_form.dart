import 'package:didayz/core/presentation/widgets/form_data.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../extension/context.dart';
import '../../theme/app_colors.dart';
import '../../theme/styles.dart';

class DefaultInputForm extends StatelessWidget {
  DefaultInputForm({required this.inputForm, this.formData, this.fromLogin});

  TypeFormInput? inputForm;
  FormData? formData = FormData();
  String? errorText = null;
  bool? fromLogin = false;

  final formatter = new DateFormat('dd/MM/yyyy');

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      keyboardType: this.inputForm!.type == 'email' ? TextInputType.emailAddress : this.inputForm!.type == 'password' || this.inputForm!.type == 'confirmpassword' ? TextInputType.visiblePassword : TextInputType.text,
      textInputAction: TextInputAction.next,
      style: AppStyles.inputText.merge(
        TextStyle(
          fontSize: 14,
        ),
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(15.0),
        filled: true,
        fillColor: Colors.white,
        hintText: this.inputForm!.text!,
        hintStyle: AppStyles.inputText.merge(
          TextStyle(
            color: Color(0xFF999999),
          ),
        ),
        errorText: errorText,
        errorStyle: AppStyles.defaultTextStyle.merge(
          TextStyle(
            color: Color(0xffffaea6),
          ),
        ),
        errorMaxLines: 10,
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.primaryColor[300]!, width: 0.0),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xFFF0F0F0), width: 1.0),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      cursorColor: Colors.black,
      cursorHeight: 25.0,
      obscureText: this.inputForm!.type == 'password' || this.inputForm!.type == 'confirmpassword' ? true : false,
      onChanged: (value) {
        switch(this.inputForm!.type){
          case 'email': {
            this.formData?.setEmail(value);
          }
          break;
          case 'password': {
            this.formData?.setPassword(value);
          }
          break;
          case 'name': {
            this.formData?.setName(value);
          }
          break;
          case 'firstname': {
            this.formData?.setFirstName(value);
          }
          break;
          case 'confirmpassword': {
            this.formData?.setConfirmPassword(value);
          }
          break;
          default: {
            this.formData?.setEmail(value);
          }
          break;
        }
      },
      validator: (val){
        switch(this.inputForm!.type){
          case 'email': {
            if(val == null || val.isEmpty)
            {
              errorText = context.translate().field_required;
            }
            else if(!EmailValidator.validate(val))
            {
              errorText = context.translate().valid_email;
            }
            else
            {
              errorText = null;
            }
          }
          break;
          case 'password': {
            if(this.fromLogin == null || !this.fromLogin!) {
              RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,60}$');
              if(val == null || val.isEmpty)
              {
                errorText = context.translate().field_required;
              }
              else if(!regex.hasMatch(val))
              {
                errorText = context.translate().format_pwd;
              }
              else
              {
                errorText = null;
              }
            }
          }
          break;
          case 'name': {
            if(val == null || val.isEmpty)
            {
              errorText = context.translate().field_required;
            }
            else if(val.length > 50)
            {
              errorText = context.translate().name_format;
            }
            else
            {
              errorText = null;
            }
          }
          break;
          case 'firstname': {
            if(val == null || val.isEmpty)
            {
              errorText = context.translate().field_required;
            }
            else if(val.length > 50)
            {
              errorText = context.translate().firstname_format;
            }
            else
            {
              errorText = null;
            }
          }
          break;
          case 'confirmpassword': {
            if(val == null || val.isEmpty)
            {
              errorText = context.translate().field_required;
            }
            else if(val != this.formData!.password)
            {
              errorText = context.translate().confirm_pwd_invalid;
            }
            else
            {
              errorText = null;
            }
          }
          break;
          default: {
            errorText = null;
          }
          break;
        }
        return errorText;
      },
    );
  }
}
