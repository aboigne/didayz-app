import 'package:didayz/features/didayz_day_detail/data/datasources/didayz_detail_remote_data_source.dart';
import 'package:didayz/features/didayz_day_detail/data/repositories/didayz_day_detail_repository_impl.dart';
import 'package:didayz/features/didayz_day_detail/domain/repositories/didayz_day_detail_repository.dart';
import 'package:didayz/features/didayz_day_detail/domain/usecases/get_didayz_detail_usecase.dart';
import 'package:didayz/features/didayz_day_detail/presentation/blocs/didayz_day_detail_cubit.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:didayz/features/didayz/data/models/didayz_model.dart';

import '../features/didayz/data/datasources/didayz_remote_data_source.dart';
import '../features/didayz_detail/data/datasources/didayz_detail_remote_data_source.dart';

import '../core/network/network_info.dart';
import '../core/presentation/blocs/navigation/navigation_cubit.dart';
import '../features/didayz/data/repositories/didayz_repository_impl.dart';
import '../features/didayz/domain/repositories/didayz_repository.dart';
import '../features/didayz/domain/usecases/get_didayz_by_page_usecase.dart';
import '../features/didayz/presentation/blocs/didayz/didayz_cubit.dart';

import '../features/didayz_detail/data/repositories/didayz_detail_repository_impl.dart';
import '../features/didayz_detail/domain/repositories/didayz_detail_repository.dart';
import '../features/didayz_detail/domain/usecases/get_didayz_detail_usecase.dart';
import '../features/didayz_detail/presentation/blocs/didayz_detail_cubit.dart';
import 'di_ext.dart';


part '../features/didayz/common/didayz_injection.dart';
part '../features/didayz_detail/common/didayz_detail_injection.dart';
part '../features/didayz_day_detail/common/didayz_day_detail_injection.dart';

final GetIt sl = GetIt.instance;
final Dio dio = Dio();
final hive = Hive;

Future<void> init() async {
  final docDir = await getApplicationDocumentsDirectory();
  hive.init(docDir.path);

  _core();
  _featureDidayz();
  _featureDidayzDetail();
  _featureDidayzDayDetail();
}

void _core() {
  sl
    // DataSources

    // Repositories

    // UseCases

    // Blocs
    ..injectBloc(NavigationCubit.new)

    // Interfaces
    ..injectInterface(GlobalConfiguration.new)
    ..injectInterface<NetworkInfo>(NetworkInfoImpl());
}
